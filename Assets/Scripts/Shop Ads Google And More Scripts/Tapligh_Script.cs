﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tapligh_Script : MonoBehaviour
{
    #region singleton Boilerplate
    private static Tapligh_Script _instance = null;
    public static Tapligh_Script Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    #endregion

    string TOKEN = "JKOD82L3GEJIHG9TB2YLOMSAMS3JXE";
    string UNIT_CODE_REWARD_TYPE = "CC5ECC60E0C655C59A47469C3764C1";
    string UNIT_CODE_FORCED_TYPE = "DCA070154FE2D9D323835CAD5AA934";


    bool testMode = false;
    string _rewardTypeToken = "";
    string _forceTypeToken = "";

    //public bool TaplighRewardTypeAvailable;
    //public bool TaplighForcedTypeAvailable;

    public Action OnTaplighRewardEligible;
    public Action OnTaplighVideoRewardTypeAvailable;
    public Action OnTaplighVideoForcedTypeAvailable;
    public Action OnTaplighVerifiedSuccessful;
    public Action OnTaplighVerifiedUnSuccessful;

    void Start()
    {
        SetTestMode(testMode);

        TaplighInterface.Instance.InitializeTapligh(TOKEN);


        StartCoroutine(IsInitialized(() =>
        {
            if (TaplighInterface.Instance.IsInitializeDone())
            {
                //LoadAd();
                StartCoroutine(LoadAdInterval());
            }
        }));

        
    }
    private void OnEnable()
    {
        TaplighInterface.Instance.OnAdListener += OnAdResult;
        TaplighInterface.Instance.OnRewardReadyListener += OnRewardReady;
        TaplighInterface.Instance.OnLoadReadyListener += OnAdReady;
        TaplighInterface.Instance.OnLoadErrorListener += OnLoadError;
        TaplighInterface.Instance.OnTokenVerifyFinishedListener += OnVerifyListener;
    }
    private void OnDisable()
    {
        TaplighInterface.Instance.OnAdListener -= OnAdResult;
        TaplighInterface.Instance.OnRewardReadyListener -= OnRewardReady;
        TaplighInterface.Instance.OnLoadReadyListener -= OnAdReady;
        TaplighInterface.Instance.OnLoadErrorListener -= OnLoadError;
        TaplighInterface.Instance.OnTokenVerifyFinishedListener -= OnVerifyListener;
    }
    /************************************************ Main methods ************************************************/

    public void LoadAd()
    {
        //TaplighRewardTypeAvailable = false;
        //TaplighForcedTypeAvailable = false;

        //if (_forceTypeToken == "")
        //{
            TaplighInterface.Instance.LoadAd(UNIT_CODE_FORCED_TYPE);
       // }
        //if (_rewardTypeToken == "")
        //{
            TaplighInterface.Instance.LoadAd(UNIT_CODE_REWARD_TYPE);
        //}
    }
    IEnumerator LoadAdInterval()
    {
        yield return new WaitForSeconds(5f);
        while (true)
        {
            yield return new WaitForSeconds(10f);
            LoadAd();
        }
    }

    public void ShowAdRewardType()
    {
        TaplighInterface.Instance.ShowAd(UNIT_CODE_REWARD_TYPE);
		GameController.Instance.isAdVideoAvailable_Reward = false;
		PlayerData.Instance.isAdVideoAvailable_Reward = false;
    }
    public void ShowAdForcedType()
    {
		if (!PlayerData.Instance.isNoAds) {
			TaplighInterface.Instance.ShowAd (UNIT_CODE_FORCED_TYPE);
			GameController.Instance.isAdVideoAvailable_Forced = false;
			PlayerData.Instance.isAdVideoAvailable_Forced = false;
		}
    }


    public void SetTestMode(bool mode)
    {
        string str = "";

        if (mode)
        {
            str = "ON";
        }
        else
        {
            str = "OFF";
        }

        TaplighInterface.Instance.SetTestEnable(mode);
        Debug.Log(str);
    }

    public void VerifyToken()
    {
        Debug.Log("_rewardTypeToken" + _rewardTypeToken);
        TaplighInterface.Instance.VerifyToken(_rewardTypeToken);
    }

    /************************************************ Private methods ************************************************/

    private void OnAdResult(AdResult result, string token)
    {
        string message = "Ad Result : ";

        switch (result)
        {
            case AdResult.BAD_TOKEN_USED: message += "Bad Token Used"; break;
            case AdResult.INTERNAL_ERROR: message += "Internal Error"; break;
            case AdResult.NO_AD_READY: message += "No Ad Ready"; break;
            case AdResult.NO_INTERNET_ACSSES: message += "No Internet Access"; break;
            case AdResult.AD_VIEWED_COMPLETELY: message += "Ad View Completelty"; break;
            case AdResult.AD_CLICKED: message += "Ad Clicked"; break;
            case AdResult.AD_IMAGE_CLOSED: message += "Ad Image Closed"; break;
            case AdResult.AD_VIDEO_CLOSED_AFTER_FULL_VIEW: message += "Ad Closed After Full View"; break;
            case AdResult.AD_VIDEO_CLOSED_ON_VIEW: message += "Ad Video Closed On View"; break;
        }

//        Debug.Log(message + "  " + token);
//        Debug.Log("_forceTypeToken" + _forceTypeToken);
//        Debug.Log("_rewardTypeToken" + _rewardTypeToken);

        //az in result hich estefade i nakon
        if (result == AdResult.AD_VIEWED_COMPLETELY)
        {
            return;            
        }
        if (result == AdResult.AD_VIDEO_CLOSED_AFTER_FULL_VIEW
            || result == AdResult.AD_CLICKED)
        {
            if (OnTaplighVerifiedSuccessful != null)
            {
                OnTaplighVerifiedSuccessful();
            }
        }
        else
        {
            if (OnTaplighVerifiedUnSuccessful != null)
            {
                OnTaplighVerifiedUnSuccessful();
            }
        }

        if (result == AdResult.AD_VIDEO_CLOSED_AFTER_FULL_VIEW
            || result == AdResult.AD_CLICKED
            || result == AdResult.AD_IMAGE_CLOSED
            || result == AdResult.AD_VIDEO_CLOSED_ON_VIEW)
        {

        }
        else
        {
 
            Debug.Log("eshkali dar namayesh tabligh be vojod amad.");
            
            _forceTypeToken = "";
            _rewardTypeToken = "";
        }
        if (token == _rewardTypeToken)
        {
            _rewardTypeToken = "";
        }
        if (token == _forceTypeToken)
        {
            _forceTypeToken = "";
        }
    }

    private void OnRewardReady(string reward)
    {
        if (OnTaplighRewardEligible != null)
        {
            OnTaplighRewardEligible();
        }
    }

    private void OnAdReady(string unit, string token)
    {
//        Debug.Log("LOAD SUCCESSFULL. THE TOKEN IS : " + token);
//        Debug.Log("LOAD SUCCESSFULL. THE UNIT IS : " + unit);

        if (unit == UNIT_CODE_REWARD_TYPE)
        {
            if (OnTaplighVideoRewardTypeAvailable != null)
            {
                OnTaplighVideoRewardTypeAvailable();
            }
			GameController.Instance.isAdVideoAvailable_Reward = true;
			PlayerData.Instance.isAdVideoAvailable_Reward = true;
            _rewardTypeToken = token;
            Debug.Log("yoReward");
        }
        if (unit==UNIT_CODE_FORCED_TYPE)
        {
            if (OnTaplighVideoForcedTypeAvailable!= null)
            {
                OnTaplighVideoForcedTypeAvailable();
            }
			GameController.Instance.isAdVideoAvailable_Forced = true;
			PlayerData.Instance.isAdVideoAvailable_Forced = true;
            _forceTypeToken = token;
            Debug.Log("yoForced");
        }
    }

    private void OnLoadError(string unit, LoadErrorStatus error)
    {
        string message = "On Load Error : ";

        switch (error)
        {
            case LoadErrorStatus.NO_INTERNET_ACCSSES: message += "No Internet Access"; break;
            case LoadErrorStatus.BAD_TOKEN_USED: message += "Bad Token Used"; break;
            case LoadErrorStatus.AD_UNIT_DISABLED: message += "Ad Unit Disabled"; break;
            case LoadErrorStatus.AD_UNIT_NOT_FOUND: message += "Ad Unit Not Found"; break;
            case LoadErrorStatus.INTERNAL_ERROR: message += "Internal Error"; break;
            case LoadErrorStatus.NO_AD_READY: message += "No Ad Ready"; break;
        }

        if (unit == UNIT_CODE_FORCED_TYPE)
        {
            _forceTypeToken = "";
            //TaplighForcedTypeAvailable = false;
        }
        if (unit == UNIT_CODE_REWARD_TYPE)
        {
            _rewardTypeToken = "";
            //TaplighRewardTypeAvailable = false;
        }

        
        //LoadAd();
        Debug.Log("ON LOAD ERROR : THE UNIT IS : " + unit);
        Debug.Log(message);
    }

    private List<string> GetResultArguments(string result)
    {
        List<string> arguments = new List<string>();

        int deviderIndex = result.IndexOf(';');
        arguments.Add(result.Substring(0, deviderIndex));
        Debug.Log("First Message : " + arguments[0]);

        for (; deviderIndex < result.Length; deviderIndex++)
        {
            if (result[deviderIndex] != ';')
                break;
        }

        arguments.Add(result.Substring(deviderIndex));
        Debug.Log("First Message : " + arguments[1]);

        return arguments;
    }

    private void OnVerifyListener(TokenResult tokenResult)
    {

        var verifyText = "Token Verify : ";
        switch (tokenResult)
        {
            case TokenResult.INTERNAL_ERROR: verifyText += "ENTERNAL ERRROR"; break;
            case TokenResult.NOT_USED: verifyText += "NOT USED"; break;
            case TokenResult.SUCCESS: verifyText += "SUCCESS"; break;
            case TokenResult.TOKEN_EXPIRED: verifyText += "TOKEN EXPIRED"; break;
            case TokenResult.TOKEN_NOT_FOUND: verifyText += "TOKEN NOT FOUND"; break;

        }
        if (tokenResult == TokenResult.SUCCESS)
        {

            if (OnTaplighVerifiedSuccessful!= null)
            {
                OnTaplighVerifiedSuccessful();
            }

        }
        else
        {
            if (OnTaplighVerifiedUnSuccessful != null)
            {
                OnTaplighVerifiedUnSuccessful();
            }
        }
        Debug.Log("Token Verify : " + verifyText );
    }

    private IEnumerator IsInitialized(System.Action onComplete)
    {
        int c = 0;
        while (c < 20)
        {
            if (TaplighInterface.Instance.IsInitializeDone())
                break;
            yield return new WaitForSeconds(0.5f);
            c++;
        }
        if (onComplete != null)
        {
            onComplete();
        }
    }

}
