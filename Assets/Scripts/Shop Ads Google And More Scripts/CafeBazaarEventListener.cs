﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BazaarPlugin;


public class CafeBazaarEventListener : MonoBehaviour
{
#if UNITY_ANDROID

    void OnEnable()
    {
        // Listen to all events for illustration purposes
        IABEventManager.billingSupportedEvent += billingSupportedEvent;
        IABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
        IABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
        IABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
        IABEventManager.querySkuDetailsSucceededEvent += querySkuDetailsSucceededEvent;
        IABEventManager.querySkuDetailsFailedEvent += querySkuDetailsFailedEvent;
        IABEventManager.queryPurchasesSucceededEvent += queryPurchasesSucceededEvent;
        IABEventManager.queryPurchasesFailedEvent += queryPurchasesFailedEvent;
        IABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
        IABEventManager.purchaseFailedEvent += purchaseFailedEvent;
        IABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
        IABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
    }

    void OnDisable()
    {
        // Remove all event handlers
        IABEventManager.billingSupportedEvent -= billingSupportedEvent;
        IABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
        IABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
        IABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
        IABEventManager.querySkuDetailsSucceededEvent -= querySkuDetailsSucceededEvent;
        IABEventManager.querySkuDetailsFailedEvent -= querySkuDetailsFailedEvent;
        IABEventManager.queryPurchasesSucceededEvent -= queryPurchasesSucceededEvent;
        IABEventManager.queryPurchasesFailedEvent -= queryPurchasesFailedEvent;
        IABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
        IABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
        IABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
        IABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
    }


    void billingSupportedEvent()
    {
        Debug.Log("billingSupportedEvent");
        // check for purchase
        BazaarIAB.queryPurchases();
    }

    void billingNotSupportedEvent(string error)
    {
        Debug.Log("billingNotSupportedEvent: " + error);
    }

    void queryInventorySucceededEvent(List<BazaarPurchase> purchases, List<BazaarSkuInfo> skus)
    {
        Debug.Log(string.Format("queryInventorySucceededEvent. total purchases: {0}, total skus: {1}", purchases.Count, skus.Count));

        for (int i = 0; i < purchases.Count; ++i)
        {
            Debug.Log(purchases[i].ToString());
        }

        Debug.Log("-----------------------------");

        for (int i = 0; i < skus.Count; ++i)
        {
            Debug.Log(skus[i].ToString());
        }
    }

    void queryInventoryFailedEvent(string error)
    {
        Debug.Log("queryInventoryFailedEvent: " + error);
    }

    private void querySkuDetailsSucceededEvent(List<BazaarSkuInfo> skus)
    {
        Debug.Log(string.Format("querySkuDetailsSucceededEvent. total skus: {0}", skus.Count));

        for (int i = 0; i < skus.Count; ++i)
        {
            Debug.Log(skus[i].ToString());
        }
    }

    private void querySkuDetailsFailedEvent(string error)
    {
        Debug.Log("querySkuDetailsFailedEvent: " + error);
    }

    private void queryPurchasesSucceededEvent(List<BazaarPurchase> purchases)
    {
        Debug.Log(string.Format("queryPurchasesSucceededEvent. total purchases: {0}", purchases.Count));

        for (int i = 0; i < purchases.Count; ++i)
        {
            Debug.Log(purchases[i].ToString());
            if (purchases[i].ProductId == IAPManager.NO_ADS_ITEM_NAME)
            {
                PlayerData.Instance.isNoAds = true;
				if (GameController.Instance != null) {
					GameController.Instance.menuScript.NoAdsSetup ();
				}
            }
        }
    }

    private void queryPurchasesFailedEvent(string error)
    {
        Debug.Log("queryPurchasesFailedEvent: " + error);
    }

    void purchaseSucceededEvent(BazaarPurchase purchase)
    {
        Debug.Log("purchaseSucceededEvent: " + purchase);
        if (purchase.DeveloperPayload == "hezarmoshtan")
        {
            switch (purchase.ProductId)
            {
			case IAPManager.NO_ADS_ITEM_NAME:
				PlayerData.Instance.isNoAds = true;
				GameController.Instance.menuScript.NoAds_Success ();
                break;
            case IAPManager.GOLDEN_ITEM_NAME:
                // add to game database
				PlayerData.Instance.hasKnifeArr[1] = true;
                break;
            case IAPManager.ELEGANT_DAGGER_G_ITEM_NAME:
				PlayerData.Instance.hasKnifeArr[17] = true;
                break;
            case IAPManager.DRAGON_G_ITEM_NAME:
				PlayerData.Instance.hasKnifeArr[19] = true;
                break;
			case IAPManager.CURVED_G_ITEM_NAME:
				PlayerData.Instance.hasKnifeArr[21] = true;
				break;
            case IAPManager.DART_G_ITEM_NAME:
				PlayerData.Instance.hasKnifeArr[24] = true;
                break;
            case IAPManager.ARABIC_SWORD_G_ITEM_NAME:
				PlayerData.Instance.hasKnifeArr[26] = true;
                break;
            default:
				Debug.LogError ("Default Error");
                break;
            }

			GameController.Instance.menuScript.BuyKnife_RealMoneySuccess ();
        }
    }

    void purchaseFailedEvent(string error)
    {
		GameController.Instance.menuScript.NoAds_Failed ();
        Debug.Log("purchaseFailedEvent: " + error);
    }

    void consumePurchaseSucceededEvent(BazaarPurchase purchase)
    {
        Debug.Log("consumePurchaseSucceededEvent: " + purchase);
        if (purchase.ProductId == "noads")
        {
            testSceneManager.PlayerHavePurchasedNoAds = false;
            PlayerPrefs.SetInt("NoAdsPurchased", 0);
        }
    }

    void consumePurchaseFailedEvent(string error)
    {
        Debug.Log("consumePurchaseFailedEvent: " + error);
    }

#endif

}


