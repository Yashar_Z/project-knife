﻿using BazaarPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPManager : MonoBehaviour {

	void OnEnable () {
		var key = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCZStpIlJutJd8zxuBJLmdxxp4BXDAgnSCJ0Nbkf8SUvf1KxD7v6hdaiiy+1swf6VThjWSxBOxr4O8U9RQaiXWLf5nWr/fxfkOmnCooTd+IDG2yZyQclE7CDAW4aMhDIAfXKbxOJ3nMKUwVKvwym7Fo4tFiBye/IPZ8PX476nw0vMpX9wys/9HnG1rWq7Pz/95twTLuOhob19O9yQqXVRgF+WxVwyIZxaR3CyxtFW8CAwEAAQ==";
		BazaarIAB.init(key);
		BazaarIAB.enableLogging(true);
	}

    public const string NO_ADS_ITEM_NAME = "noads";
    public const string GOLDEN_ITEM_NAME = "golden";
    public const string ELEGANT_DAGGER_G_ITEM_NAME = "elegant_dagger_g";
    public const string DRAGON_G_ITEM_NAME = "dragon_g";
    public const string DART_G_ITEM_NAME = "dart_g";
    public const string CURVED_G_ITEM_NAME = "curved_g";
    public const string ARABIC_SWORD_G_ITEM_NAME = "arabic_sword_g";

    string[] skus = { NO_ADS_ITEM_NAME, GOLDEN_ITEM_NAME,
                        ELEGANT_DAGGER_G_ITEM_NAME, DRAGON_G_ITEM_NAME,
                        DART_G_ITEM_NAME,CURVED_G_ITEM_NAME,ARABIC_SWORD_G_ITEM_NAME };

    public static void PurchaseNoAds()
    {
        BazaarIAB.purchaseProduct(NO_ADS_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenDagger()
    {
        BazaarIAB.purchaseProduct(GOLDEN_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenElegantDagger()
    {
        BazaarIAB.purchaseProduct(ELEGANT_DAGGER_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenDragonDagger()
    {
        BazaarIAB.purchaseProduct(DRAGON_G_ITEM_NAME, "hezarmoshtan");
    }
	public static void PurchaseGoldenCurvedDagger()
	{
		BazaarIAB.purchaseProduct(CURVED_G_ITEM_NAME, "hezarmoshtan");
	}
    public static void PurchaseGoldenDartDagger()
    {
        BazaarIAB.purchaseProduct(DART_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenArabicSword()
    {
        BazaarIAB.purchaseProduct(ARABIC_SWORD_G_ITEM_NAME, "hezarmoshtan");
    }

}
