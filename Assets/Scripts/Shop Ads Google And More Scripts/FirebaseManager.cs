﻿using Firebase;
using Firebase.Analytics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// event defined in this link:
/// https://paper.dropbox.com/doc/Mobile-Game-Tracks-OyTvaHdvGhT2zs5Yv8cqA
/// </summary>
public class FirebaseManager : MonoBehaviour {

    //fire base vars
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    public bool firebaseInitialized = false;

    private void Awake ()
    {
        //initiate firebase
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    void InitializeFirebase()
    {
        Debug.Log("Enabling data collection.");
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

        Debug.Log("Set user properties.");
        // Set the user's sign up method.
        FirebaseAnalytics.SetUserProperty(FirebaseAnalytics.UserPropertySignUpMethod, "Google");
        // Set the user ID.
        FirebaseAnalytics.SetUserId(Social.localUser.userName);
        Debug.Log(Social.localUser.userName);
        // Set default session duration values.
        FirebaseAnalytics.SetMinimumSessionDuration(new TimeSpan(0, 0, 10));
        FirebaseAnalytics.SetSessionTimeoutDuration(new TimeSpan(0, 30, 0));
        firebaseInitialized = true;
    }

    // 1 - event start/restart ye level
    public static void FireBaseEventStartRestartGame()
    {
        Debug.Log("Logging start/Restart game event.");

        FirebaseAnalytics.LogEvent("GameStart");
    }
    // 4 - event tedad kharid har chaghoo
    public static void FireBaseEventKnifeObtained(int knifeIndex)
    {
        Debug.Log("Logging knife obtained event.");

        FirebaseAnalytics.LogEvent("KnifeObtained" , "KnifeIndex" , knifeIndex);
    }
    // 5 - event estefade az continue
    public static void FireBaseEventContinueUsed()
    {
        Debug.Log("Logging continue clicked event.");

        FirebaseAnalytics.LogEvent("ContinueUsed");
    }
    // 6 - event didan video ekhtiari ( az koja click shode)
    public static void FireBaseEventVideoAd(string whereInGame)
    {
        Debug.Log("Logging video ad clicked event.");

        FirebaseAnalytics.LogEvent("VideoAd", "WhereInGame", whereInGame);
    }
    // 7 - event estefade az daily gift
    public static void FireBaseEventDailyGift()
    {
        Debug.Log("Logging daily gift clicked event.");

        FirebaseAnalytics.LogEvent("DailyGift");
    }
    // 8 - miangin score roozane ( post score event)
    public static void FireBaseEventPostScore(int score)
    {
        Debug.Log("Logging post score event.");

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPostScore, FirebaseAnalytics.ParameterScore, score);
    }
    // 9 - startLevel baraye funnel checkpoint estefade mishavad
    public static void FireBaseEventStartLevel(string levelName)
    {
        Debug.Log("Logging a end level event.");

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, FirebaseAnalytics.ParameterLevelName, levelName);
    }
    // 9 - EndLevel baraye funnel checkpoint estefade mishavad shomare 9
    public static void FireBaseEventEndLevel(string levelName)
    {
        Debug.Log("Logging a end level event.");

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, FirebaseAnalytics.ParameterLevelName, levelName);
    }
    // 10 - event click bar roye rate us
    /// <summary>
    /// Rate Us Fire Event , 0 For No , 1 For Yes
    /// </summary>
    /// <param name="clickValue"> 0 For No , 1 For Yes</param>
    public static void FireBaseEventRateUs(int clickValue)
    {
        Debug.Log("Logging rate us event.");

        FirebaseAnalytics.LogEvent("RateUs","ClickedOnYes", clickValue);
    }
    // 11 - event login
    public static void FireBaseEventLogin()
    {
        Debug.Log("Login event.");

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
    }
    // 12 event unlock achievement
    public static void FireBaseEventUnlockAchievement(string achivementId)
    {
        Debug.Log("unlock achievement event.");

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventUnlockAchievement,FirebaseAnalytics.ParameterAchievementId, achivementId);
    }

	// 13 video verification (success)

	// 14 video verification (fail)
}
