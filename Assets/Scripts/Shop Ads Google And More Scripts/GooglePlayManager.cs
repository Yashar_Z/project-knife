﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

public class GooglePlayManager : MonoBehaviour
{
	public static System.Action OnGooglePlaySignIn;
	public static System.Action OnGooglePlaySignOut;

	public static GooglePlayManager Instance { get; private set; }

	FirebaseManager firebaseManagerRef;

	void Start()
	{
		Instance = this;

		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			.Build();
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.Activate();

		firebaseManagerRef = GetComponent<FirebaseManager> ();
		StartCoroutine (SignIn_Delay ());
	}

	System.Collections.IEnumerator SignIn_Delay () {
		yield return new WaitForSeconds (3.65F);
		while (true) {
			yield return new WaitForSeconds (1);
			if (firebaseManagerRef.firebaseInitialized) {
				SignIn ();
				break;
			} 
		}
	}

	public void SignOut()
	{
		PlayGamesPlatform.Instance.SignOut();

		PlayerData.Instance.isYesGooglePlay = false;
		if (OnGooglePlaySignOut != null) {
			OnGooglePlaySignOut ();
		}
	}

	public void SignIn()
	{
		Social.localUser.Authenticate((success) => {
			if (success)
			{
				Debug.Log ("sign in successful");
                // Moshtan_Utilties_Script.ShowToast("sign in successful");

                // Fire Base Login Event
                FirebaseManager.FireBaseEventLogin();

				PlayerData.Instance.isYesGooglePlay = true;
				if (OnGooglePlaySignIn != null) {
					OnGooglePlaySignIn ();
				}
			}
			else
			{
				Debug.Log ("sign in fail");
				// Moshtan_Utilties_Script.ShowToast("sign in fail");

				PlayerData.Instance.isYesGooglePlay = false;
				if (OnGooglePlaySignOut != null) {
					OnGooglePlaySignOut ();
				}
			}
		});
	}


	#region Achievements
	public void UnlockAchievement(string id)
	{
		Social.ReportProgress(id, 100, success => {
			// Moshtan_Utilties_Script.ShowToast ("achievement unlocked");
		});
	}

	public void IncrementAchievement(string id, int stepsToIncrement)
	{
		PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => {
			// Moshtan_Utilties_Script.ShowToast ("achievement incremented");
		});
	}

	public void ShowAchievementsUI()
	{
		Social.ShowAchievementsUI();
	}


	#endregion /Achievements

	#region Leaderboards
	public void AddScoreToLeaderboard(string leaderboardId, long score)
	{
		Social.ReportScore(score, leaderboardId, (bool success) =>
			{
				if (success)
				{
					Debug.Log("score added" + leaderboardId);
				}
			});
	}

	public void ShowLeaderboardsUI()
	{
		Social.ShowLeaderboardUI();
		//PlayGamesPlatform.Instance.ShowLeaderboardUI();
	}
	#endregion /Leaderboards

}