﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Spawner : MonoBehaviour {

	public GameObject objPresetBlockHolder;

	public GameObject objMoonSource;
	public GameObject objPlanetSource;

	public GameObject objMoonSourceParnet;
	public GameObject objPlanetSourceParnet;

	public GameObject objTopCheckerPlanetSource;

	public Transform transMoonPathGroupParent;

	public Transform transPlanetsParentHolder;

	public PlanetController planetControllerRef;
	public PlanetController planetControllerRef_Prev;

	public int intPlanetCounter;
	public int intMoonPathCounter;

	[Header ("Moon Path & Distance Display")]
	public Transform transDistanceTextCanvas;
	public float floatDistanceBuffer;
	public float floatOrbitsSumsBuffer;

	public Vector2 v2_MoonPathPositBuffer;
	public float floatMoonPathRotation;
	public float floatMoonHeightIncrease;
	public Transform transNewMoonBuffer;

	public float floatSingleMoonStandardSize;

	public enum LastPlanetWasTo
	{
		Right,
		Left
	}
	[HideInInspector]
	public LastPlanetWasTo lastPlanetWasTo;

	public bool isMoonPathGroup;

	public enum MoonPath_PositType
	{
		Midpoint_Core,
		Midpoint_Orbits,
		CloserToStart,
		CloserToEnd,
	}

	public MoonPath_PositType moonPath_PositType;
	public bool isPlanetToRight;

	[Header ("Start Height")]
	public float floatPlanets_SpawnHeight;
	public float floatPlanets_SpawnHoriz;

	[HideInInspector]
	public float floatPrevPlanetHolder;

	[Header ("Designer Blocks")]
//	public int intDiffCounter;
//	public int intBlockCounter;

	public Transform transDesignerBlockRef;
	public Transform transDesignerDiffSetRef;

	public int intDifficultyDesigner_DiffIndex;
	public int intDifficultyDesigner_BlocksCounter;

	private float floatDistance_IncreaseAmount;

	// Moon chances for moon path spawns
	private int intMoonChance_Hazard;
	private int intMoonChance_Currency; 
	private int intMoonChance_Superscore;
	private int intMoonChance_Autoplay;
	private int intMoonChance_Total;

	// Difficulty Increase Counter Steps
	private int intPlanetsIncrease_SpeedReach;
	private int intPlanetsDecrease_Size;
	private int intPlanetsIncrease_HazardChance;
	private int intHazardItselfIncrease;
	private int intPlanetsIncrease_MoonMin;
	private int intPlanetsIncrease_MoonMax;
	//
	private int intPlanetsCounter_Step;

	// Difficulty Forced Triggers
	private int intForcedTrigger_Hazard;
	private int intForcedTrigger_SpecialPlanet;
	private int intForcedTrigger_MoonPath;
	private int intForcedTrigger_MoonPathGroup;
	private int intForcedTrigger_Event;
	// This is for the new method of events based on pools
	private int intEventPlanetTarget;
	//
	private int intForcedTriggerHazard_Step;
	private int intForcedTriggerSpecPlanet_Step;
	private int intForcedTriggerMoonPath_Step;
	private int intForcedTriggerMoonPathGroup_Step;
	private int intForcedTriggerEvent_Step;
	//
	private bool isForced_Hazard;
	private int intForced_HazardCount;
	// Buffer is for las value and ONLY for forced hazard
	private int intForced_HazardBuffer;
	//
	private bool isForced_PlanetSpecial;
	private int intForced_PlanetSpecialCount;
	//
	private int intForced_PlanetSpecialRandomNumber;
	//
	private bool isForced_MoonPath;
	private int intForced_MoonPathCount;
	//
	private bool isForced_MoonPathGroup;
	private int intForced_MoonPathGroupCount;
	//
	private bool isEventMixed;
	private PlanetType planetTypeEventBuffer;

	private int intPlanetSpecialChance_All;
	private int intPlanetSpecialChance_BigSmall;
	private int intPlanetSpecialChance_IrregularRotate;
	private int intPlanetSpecialChance_ShrinkingBomb;
	private int intPlanetSpecialChance_TOTAL;
	//
	// These are for internal calculations
	private int intPlanetSpecial_Count_All;
	private int intPlanetSpecial_Count_BigSmall;
	private int intPlanetSpecial_Count_IrregularRot;
	private int intPlanetSpecial_Count_ShrinkingBomb;

	// For Event Planets
	private bool isEventPlanetsSet;
	private int intEventPlanetsCounter;

	private Vector2 v2_PlanetPositBuffer;

	private PlanetType planetType_New;
	private PlanetType planetType_Prev;

	// For spawner end bug
	private bool canSpawnAnyMoreBlocks;

	void Awake () {
		objMoonSourceParnet.SetActive (false);
		objPlanetSourceParnet.SetActive (false);
		transMoonPathGroupParent.gameObject.SetActive (false);
	}

	public void Spawner_Start () {
		Prepare_PlanetCounterValues ();
		Prepare_OrbitSpeeds ();
		Prepare_OrbitSizes ();

		if (!GameController.Instance.isDesignerMode) {
			GameController.Instance.Spawn_MorePlanets ();
			objPresetBlockHolder.SetActive (false);
		} 
		else {
			objPresetBlockHolder.SetActive (true);
		}
	}

	public void Prepare_PlanetCounterValues () {
		//		intBlockCounter = 0;
		intDifficultyDesigner_BlocksCounter = -1;

		// Get steps
		intPlanetsCounter_Step = GameController.Instance.intPlanetsCounter_Step;
		intForcedTriggerHazard_Step = GameController.Instance.intForcedTrigger_Hazardstep;
		intForcedTriggerSpecPlanet_Step = GameController.Instance.intForcedTriggerSpecPlanet_Step;
		intForcedTriggerMoonPath_Step = GameController.Instance.intForcedTriggerMoonPath_Step;
		intForcedTriggerMoonPathGroup_Step = GameController.Instance.intForcedTriggerMoonPathGroup_Step;
		intForcedTriggerEvent_Step = GameController.Instance.intForcedTriggerEvent_Step;
		
		// Difficulty Planets
		intPlanetsDecrease_Size = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsDecrease_Size, intPlanetsCounter_Step);
		intPlanetsIncrease_HazardChance = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_HazardChance, intPlanetsCounter_Step);
		intHazardItselfIncrease = PlanetsCounter_StepSetup (GameController.Instance.intHazardItselfIncrease, intPlanetsCounter_Step);
		intPlanetsIncrease_MoonMin = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_MoonMin, intPlanetsCounter_Step);
		intPlanetsIncrease_MoonMax = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_MoonMax, intPlanetsCounter_Step);
		// Now use this for how many planets before reaching max speed;
//		intPlanetsIncrease_SpeedReach = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_SpeedReach, intPlanetsCounter_Step);

		// Forced Triggers
		intForcedTrigger_Hazard = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_Hazard, intForcedTriggerHazard_Step);
		intForcedTrigger_SpecialPlanet = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_SpecialPlanet, intForcedTriggerHazard_Step);
		intForcedTrigger_MoonPath = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPath, intForcedTriggerMoonPath_Step);
		intForcedTrigger_MoonPathGroup = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPathGroup, intForcedTriggerMoonPathGroup_Step);
		intForcedTrigger_Event = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_Event, intForcedTriggerEvent_Step);

		// Planet Type Chances (There are not step based)
		//		intPlanetSpecialChance_All = PlanetsCounter_StepSetup (GameController.Instance.intPlanetSpecialChance_All, intForcedTriggerHazard_Step);
		//		intPlanetSpecialChance_BigSmall = PlanetsCounter_StepSetup (GameController.Instance.intPlanetSpecialChance_BigSmall, intForcedTriggerHazard_Step);
		//		intPlanetSpecialChance_IrregularRotate = PlanetsCounter_StepSetup (GameController.Instance.intPlanetSpecialChance_IrregularRotate, intForcedTriggerHazard_Step);
		//		intPlanetSpecialChance_ShrinkingBomb = PlanetsCounter_StepSetup (GameController.Instance.intPlanetSpecialChance_ShrinkingBomb, intForcedTriggerHazard_Step);

		// Planet type chances (No progress / change)
		intPlanetSpecialChance_All = GameController.Instance.intPlanetSpecialChance_All;
		intPlanetSpecialChance_BigSmall = GameController.Instance.intPlanetSpecialChance_BigSmall;
		intPlanetSpecialChance_IrregularRotate = GameController.Instance.intPlanetSpecialChance_IrregularRotate + intPlanetSpecialChance_BigSmall;
		intPlanetSpecialChance_ShrinkingBomb = GameController.Instance.intPlanetSpecialChance_ShrinkingBomb + intPlanetSpecialChance_IrregularRotate;
		intPlanetSpecialChance_TOTAL = intPlanetSpecialChance_ShrinkingBomb;
	}

	public void Prepare_OrbitSpeeds () {
		GameController.Instance.floatOrbitSpeed_Min += GameController.Instance.intCheckpoint_TotalTarget * GameController.Instance.floatOrbitSpeed_MinIncrease;
		GameController.Instance.floatOrbitSpeed_Max += GameController.Instance.intCheckpoint_TotalTarget * GameController.Instance.floatOrbitSpeed_MaxIncrease;

		GameController.Instance.floatOrbitSpeed_MinIncrease = (GameController.Instance.floatOrbitSpeed_AfterAbsolute_Min
			- GameController.Instance.floatOrbitSpeed_Min) / GameController.Instance.intPlanetsIncrease_SpeedReach;
		GameController.Instance.floatOrbitSpeed_MaxIncrease = (GameController.Instance.floatOrbitSpeed_AfterAbsolute_Max
			- GameController.Instance.floatOrbitSpeed_Max) / GameController.Instance.intPlanetsIncrease_SpeedReach;

		if (GameController.Instance.floatOrbitSpeed_MinIncrease <= 0) {
			GameController.Instance.floatOrbitSpeed_MinIncrease = 0;
		}

		if (GameController.Instance.floatOrbitSpeed_MaxIncrease <= 0) {
			GameController.Instance.floatOrbitSpeed_MaxIncrease = 0;
		}
	}

	public void Prepare_OrbitSizes () {
		GameController.Instance.floatOrbitSize_IntroMin -= GameController.Instance.intCheckpoint_TotalTarget * GameController.Instance.floatOrbitSize_IntroMinDecrease;
		GameController.Instance.floatOrbitSize_IntroMax -= GameController.Instance.intCheckpoint_TotalTarget * GameController.Instance.floatOrbitSize_IntroMaxDecrease;

		GameController.Instance.intPlanetsIncrease_SizeReach -= GameController.Instance.intCheckpoint_TotalTarget;
		if (GameController.Instance.intPlanetsIncrease_SizeReach < 1) {
			GameController.Instance.isIntroSectionSizeDone = true;
		}

		GameController.Instance.floatOrbitSize_IntroMinDecrease = (GameController.Instance.floatOrbitSize_IntroMin 
			- GameController.Instance.floatOrbitSize_Min) / GameController.Instance.intPlanetsIncrease_SizeReach;
		GameController.Instance.floatOrbitSize_IntroMaxDecrease = (GameController.Instance.floatOrbitSize_IntroMax
			- GameController.Instance.floatOrbitSize_Max) / GameController.Instance.intPlanetsIncrease_SizeReach;

		if (GameController.Instance.floatOrbitSize_IntroMinDecrease <= 0) {
			GameController.Instance.floatOrbitSize_IntroMinDecrease = 0;
		}

		if (GameController.Instance.floatOrbitSize_IntroMaxDecrease <= 0) {
			GameController.Instance.floatOrbitSize_IntroMaxDecrease = 0;
		}
	}

	/* Old Diff Block Method
	public void EmptyBlock_CheckOLD () {
		if (transDesignerDiffSetRef.childCount == 0) {
			intDifficultyDesigner_DiffCounter--;

			if (intDifficultyDesigner_DiffCounter >= 0) {
				transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (intDifficultyDesigner_DiffCounter).transform;	
				EmptyBlock_Check ();
			} 

			// In case all diffs are empty
			else {
				canSpawnAnyMoreBlocks = false;
			}

		// This diff block is not empty
		} else {
			canSpawnAnyMoreBlocks = true;
		}
	}
	*/

	void EmptyBlock_Check () {

		if (transDesignerDiffSetRef.childCount != 0) {
			canSpawnAnyMoreBlocks = true;
//			Block_UsePoolsChances (intDifficultyDesigner_BlocksCounter);
		} else {
			canSpawnAnyMoreBlocks = false;
		}
	}

	public void Block_UsePoolsChances (int i) {

		// Is not introduction block
		if (i >= 0) {
			int intPoolChance_1 = GameController.Instance.v3Int_BlockPoolChancesArr [i].x;
			int intPoolChance_2 = intPoolChance_1 + GameController.Instance.v3Int_BlockPoolChancesArr [i].y;
			int intPoolChance_3 = intPoolChance_2 + GameController.Instance.v3Int_BlockPoolChancesArr [i].z;
			int intPoolChance_E = intPoolChance_3 + GameController.Instance.int_BlockEventChancesArr [i];

			int intRandomPoolInt = Random.Range (0, intPoolChance_E);

//		Debug.LogError ("Block intRandomPoolInt = " + intRandomPoolInt +
//			"\n" + "intPoolChance_1 = " + intPoolChance_1 + 
//			"\n" + "intPoolChance_2 = " + intPoolChance_2 + 
//			"\n" + "intPoolChance_3 = " + intPoolChance_3 + 
//			"\n" + "intPoolChance_E = " + intPoolChance_E 
//		);

			if (intRandomPoolInt < intPoolChance_1) {
				intDifficultyDesigner_DiffIndex = 1;
				transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (intDifficultyDesigner_DiffIndex).transform;	
				EmptyBlock_Check ();
			} else {
				if (intRandomPoolInt < intPoolChance_2) {
					intDifficultyDesigner_DiffIndex = 2;
					transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (intDifficultyDesigner_DiffIndex).transform;	
					EmptyBlock_Check ();
				} else {
					if (intRandomPoolInt < intPoolChance_3) {
						intDifficultyDesigner_DiffIndex = 3;
						transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (intDifficultyDesigner_DiffIndex).transform;	
						EmptyBlock_Check ();
					} else {
						if (intRandomPoolInt < intPoolChance_E) {
							canSpawnAnyMoreBlocks = false;
						} else {
							canSpawnAnyMoreBlocks = false;
						}
					}
				}
			}

		} 

		// Introduction block
		else {
			intDifficultyDesigner_DiffIndex = 0;
			transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (intDifficultyDesigner_DiffIndex).transform;	
			canSpawnAnyMoreBlocks = true;
		}

		intDifficultyDesigner_BlocksCounter++;

		// Repeat the lst value of array
		if (intDifficultyDesigner_BlocksCounter >= GameController.Instance.v3Int_BlockPoolChancesArr.Length) {
			intDifficultyDesigner_BlocksCounter--;
		}
	}

	public void SpawnBlock_Move () {
		// Pool chances
		Block_UsePoolsChances (intDifficultyDesigner_BlocksCounter);

		// Based on bool from above method AND the pool chances. If can't, then it should be event rather than block
		if (canSpawnAnyMoreBlocks) {
			int intSelectedDiff;
			int intSelectedBlock;

			intSelectedDiff = intDifficultyDesigner_DiffIndex;
			intSelectedBlock = Random.Range (0, transDesignerDiffSetRef.childCount - 1);

			transDesignerBlockRef = objPresetBlockHolder.transform.GetChild (intSelectedDiff).GetChild (intSelectedBlock).transform;

			objPresetBlockHolder.transform.GetChild (intSelectedDiff).gameObject.SetActive (true);
			objPresetBlockHolder.transform.GetChild (intSelectedDiff).GetChild (intSelectedBlock).gameObject.SetActive (true);

			transDesignerBlockRef.SetParent (transform);

			// For randomly switching the level from left to right
			if (Random.Range (0, 2) == 0) {
				transDesignerBlockRef.localScale = new Vector3 (transDesignerBlockRef.localScale.x, transDesignerBlockRef.localScale.y, transDesignerBlockRef.localScale.z);
			} else {
				transDesignerBlockRef.localScale = new Vector3 (-transDesignerBlockRef.localScale.x, transDesignerBlockRef.localScale.y, transDesignerBlockRef.localScale.z);
			}

			float floatNewHeight = 
				transDesignerBlockRef.GetChild (0).GetChild (transDesignerBlockRef.GetChild (0).childCount - 1).localPosition.y
				- transDesignerBlockRef.GetChild (0).GetChild (0).localPosition.y;

			// Don't activate the designer blocks parent
//			objPresetBlockHolder.SetActive (true);

			// This is NOT for designer mode
			if (!GameController.Instance.isDesignerMode) {
				// CAN'T Get last planet of this (BECAUSE THE PLANETCONTROLLER has not been instantiated)
//				planetControllerRef = transDesignerBlockRef.GetChild (0).GetChild (transDesignerBlockRef.GetChild (0).childCount - 1).GetComponentInChildren<PlanetController>();
//				floatPrevPlanetHolder = planetControllerRef.floatOrbitSize;

				Planet_HeightIncrease ();

				transDesignerBlockRef.localPosition = new Vector2 (0, floatPlanets_SpawnHeight);

				floatPlanets_SpawnHeight += (floatNewHeight + Random.Range (2.4F, 3.3F));
			}
			// End of canSpawnBlocks check
		} 

		// Event time OR Can't spawn block so must go for event
		else {
			StartCoroutine (Planet_Spawn_EventOnly (Random.Range (GameController.Instance.intEventPlanets_CountMin, GameController.Instance.intEventPlanets_CountMax)));
		}
	}

	/* Spawn Block Instantiate
	public void SpawnBlock_Instantiate (int diffBlock, int setBlock) {
		transDesignerDiffSetRef = objPresetBlockHolder.transform.GetChild (diffBlock).transform;	
		int intSelectedBlock;

		if (!GameController.Instance.isDesignerMode) {
			intSelectedBlock = Random.Range (0, transDesignerDiffSetRef.childCount - 1);
		} else {
			intSelectedBlock = setBlock;
		}

		transDesignerBlockRef = objPresetBlockHolder.transform.GetChild (diffBlock).GetChild (intSelectedBlock).transform;

		GameObject objNewBlock = Instantiate (transDesignerBlockRef.gameObject, transform);
		transDesignerBlockRef = objNewBlock.transform;

		transDesignerBlockRef.gameObject.SetActive (true);

//		transDesignerBlockRef.SetParent (transform);

		// For randomly switching the level from left to right
		if (Random.Range (0, 2) == 0) {
			transDesignerBlockRef.localScale = new Vector3 (transDesignerBlockRef.localScale.x, transDesignerBlockRef.localScale.y, transDesignerBlockRef.localScale.z);
		} else {
			transDesignerBlockRef.localScale = new Vector3 (-transDesignerBlockRef.localScale.x, transDesignerBlockRef.localScale.y, transDesignerBlockRef.localScale.z);
		}

		float floatNewHeight = 
			transDesignerBlockRef.GetChild (0).GetChild (transDesignerBlockRef.GetChild (0).childCount - 1).localPosition.y
			- transDesignerBlockRef.GetChild (0).GetChild (0).localPosition.y;

		// Don't activate the designer blocks parent
		//		objPresetBlockHolder.SetActive (true);

		// TODO: If used, needs to go up and be updated like the Spawn_Move
		// This is NOT for designer mode
		if (!GameController.Instance.isDesignerMode) {
			// CAN'T Get last planet of this (BECAUSE THE PLANETCONTROLLER has not been instantiated)
			//			planetControllerRef = transDesignerBlockRef.GetChild (0).GetChild (transDesignerBlockRef.GetChild (0).childCount - 1).GetComponentInChildren<PlanetController>();
			//			floatPrevPlanetHolder = planetControllerRef.floatOrbitSize;

			Planet_HeightIncrease ();

			transDesignerBlockRef.localPosition = new Vector2 (0, floatPlanets_SpawnHeight);

			floatPlanets_SpawnHeight += (floatNewHeight - Random.Range (3F, 4F));

			if ((intDifficultyDesigner_BlocksCounter != 0) && (intDifficultyDesigner_BlocksCounter % GameController.Instance.intDifficultyDesigner_Step == 0)) {
				intDifficultyDesigner_DiffCounter++;
			}
			intDifficultyDesigner_BlocksCounter++;

		}
	}
	*/

	public IEnumerator Planet_Spawn_Many (int howMany) {
		for (int i = 0; i < howMany; i++) {
			yield return null;

			// This is to make sure force moon path doesn't happen when Designer Block is about to appear
			if ((i == (howMany - 1)) || i == 0) {
				isForced_MoonPath = false;
			}

			Planet_Spawn ();
		}


		// Using this instead of the difficulty spawn trigger
		TopCheckerSpawn ();
			
		SpawnBlock_Move ();
//		SpawnBlock_Instantiate (intDifficultyDesigner_DiffCounter, intBlockCounter++);
	}

	public IEnumerator Planet_Spawn_EventOnly (int howMany) {
		
		intEventPlanetTarget = howMany;
		intForcedTrigger_Event = 0;
		intEventPlanetsCounter = 0;

		for (int i = 0; i < howMany; i++) {
			Difficulty_EventPlanets (true);

			yield return null;

			// This is to make sure force moon path doesn't happen when Designer Block is about to appear
			if ((i == (howMany - 1)) || i == 0) {
				isForced_MoonPath = false;
			}

			Planet_Spawn ();
		}
	}

	int PlanetsCounter_StepSetup (int intBase, int intStep) {
		return Random.Range (
			intBase, 
			intBase + intStep
		);
	}

	bool Planet_IsLeft () {
		return (planetControllerRef_Prev.transform.localPosition.x > planetControllerRef.transform.localPosition.x);
	}

	bool Planet_IsRight () {
		return (planetControllerRef_Prev.transform.localPosition.x < planetControllerRef.transform.localPosition.x);
	}

	float Planet_HorizDiff () {
		return Mathf.Abs (planetControllerRef.transform.localPosition.x - planetControllerRef_Prev.transform.localPosition.x);
	}

	void Planet_HeightIncrease () {
		if (GameController.Instance.isIntroSectionSizeDone) {
			switch (GameController.Instance.intDifficultyCounter) {
			case 1:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.7F + Random.Range (0.25F, 0.7F);

				break;
			case 2:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.72F + Random.Range (0.32F, 1F);

				break;
			case 3:
			case 4:
//			floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.75F + Random.Range (0.25F, 1);
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.8F + Random.Range (0.4F, 1.8F);

				break;
			case 5:
			case 6:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.82F + Random.Range (0.44F, 2F);
				break;

			case 7:
			case 8:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.88F + Random.Range (0.5F, 2.5F);
				break;

			case 9:
			case 10:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.9F + Random.Range (0.55F, 3F);
				break;

			case 13:
			case 14:
			case 15:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 1F + Random.Range (0.64F, 3.6F);
				break;

			case 16:
			case 17:
			case 18:
//			floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.76F + Random.Range (0.26F, 1);
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 1.1F + Random.Range (0.7F, 3.8F);
				break;
			case 19:
			case 20:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 1.2F + Random.Range (0.78F, 4F);
				break;
			default:
//			floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.76F + Random.Range (0.26F, 1);
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 1.3F + Random.Range (0.82F, 4.2F);
				Debug.LogError ("Default Error");

				break;
			}
		} 

		// increase heights for intro
		else {
			switch (GameController.Instance.intDifficultyCounter) {
			case 1:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.64F + Random.Range (0.26F, 0.5F);
				break;
			case 2:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.66F + Random.Range (0.3F, 0.55F);
				break;
			default:
				floatDistance_IncreaseAmount = (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder) * 0.69F + Random.Range (0.35F, 0.62F);
				break;
			}
		}

		floatPlanets_SpawnHeight += floatDistance_IncreaseAmount;
	}

	public void Planet_SpawnContinue (Vector3 v3_Location) {
		GameObject objNewPlanet = Instantiate (objPlanetSource);
		objNewPlanet.name = "Continue Planet";
		objNewPlanet.transform.SetParent (transPlanetsParentHolder);
		objNewPlanet.transform.position = v3_Location;

		planetControllerRef = objNewPlanet.GetComponent <PlanetController> ();
		planetControllerRef.planetType = PlanetType.Normal;
		planetControllerRef.Planet_PreSetup_Random (4);
	}

	void Test_SingleMoonSpawn (float x, float y) {
		GameObject objNewMoon = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		objNewMoon.transform.SetParent (transform);
		objNewMoon.transform.localScale = Vector3.one;
		objNewMoon.transform.localPosition = new Vector2 (x, y);
		objNewMoon.transform.eulerAngles = new Vector3 (0, 0, 90);

		objNewMoon.GetComponent <MoonController> ().Moon_Setup (MoonType.Currency, floatSingleMoonStandardSize, false);
//		objNewMoon.GetComponent <MoonController> ().Moon_Setup ((MoonType)Random.Range (1, 4), floatSingleMoonStandardSize, false);
	}

	void Test_SingleSuperCurrSpawn (float x, float y) {
		GameObject objNewMoon = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		objNewMoon.transform.SetParent (transform);
		objNewMoon.transform.localScale = Vector3.one;
		objNewMoon.transform.localPosition = new Vector2 (x, y);
		objNewMoon.transform.eulerAngles = new Vector3 (0, 0, 90);

		objNewMoon.GetComponent <MoonController> ().Moon_Setup (MoonType.SuperCurrency, floatSingleMoonStandardSize, false);
		//		objNewMoon.GetComponent <MoonController> ().Moon_Setup ((MoonType)Random.Range (1, 4), floatSingleMoonStandardSize, false);
	}

	void Test_SingleHazardSpawn () {
		GameObject objNewMoon2 = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		objNewMoon2.transform.SetParent (transform);
		objNewMoon2.transform.localScale = Vector3.one;
		objNewMoon2.transform.localPosition = new Vector2 (3, 4);;
		objNewMoon2.transform.eulerAngles = new Vector3 (0, 0, 90);

		objNewMoon2.GetComponent <MoonController> ().Moon_Setup (MoonType.Hazard, floatSingleMoonStandardSize, false);

		GameObject objNewMoon3 = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		objNewMoon3.transform.SetParent (transform);
		objNewMoon3.transform.localScale = Vector3.one;
		objNewMoon3.transform.localPosition = new Vector2 (3, 5);;
		objNewMoon3.transform.eulerAngles = new Vector3 (0, 0, 90);

		objNewMoon3.GetComponent <MoonController> ().Moon_Setup (MoonType.Hazard, floatSingleMoonStandardSize, false);
	}

	void Planet_Spawn () {
		GameObject objNewPlanet = Instantiate (objPlanetSource);
		objNewPlanet.name = objPlanetSource.name + " " + (intPlanetCounter).ToString ();
		objNewPlanet.transform.SetParent (transPlanetsParentHolder);

		if (intPlanetCounter != 0) {
			floatPlanets_SpawnHoriz = Random.Range (-GameController.Instance.floatPlanetHorizLimit, GameController.Instance.floatPlanetHorizLimit);
			planetControllerRef_Prev = planetControllerRef; 
		} else {
			floatPlanets_SpawnHoriz = 0;
			floatPlanets_SpawnHeight = 0.5F;

			Test_SingleMoonSpawn (0, -2);

			// TODO: Only for TEST
//			Test_SingleMoonSpawn (-0.5F, 5);
//			Test_SingleMoonSpawn (-2, 5);
//			Test_SingleMoonSpawn (-3.5F, 5);
//			Test_SingleMoonSpawn (-5, 5);
//			Test_SingleMoonSpawn (-6.5F, 5);
//			Test_SingleMoonSpawn (-8, 5);
//			Test_SingleMoonSpawn (-9.5F, 5);
//
//			Test_SingleSuperCurrSpawn (0.5F, 5);
//			Test_SingleSuperCurrSpawn (2, 5);
//			Test_SingleSuperCurrSpawn (3.5F, 5);
//			Test_SingleSuperCurrSpawn (5, 5);
//			Test_SingleSuperCurrSpawn (6.5F, 5);
//			Test_SingleSuperCurrSpawn (8, 5);
//			Test_SingleSuperCurrSpawn (9.5F, 5);
		}

		planetControllerRef = objNewPlanet.GetComponent <PlanetController> ();
		planetControllerRef.planetType = planetType_New;

		planetControllerRef.Planet_PreSetup_Random (intPlanetCounter);

		Planet_HeightIncrease ();
		floatPrevPlanetHolder = planetControllerRef.floatOrbitSize;

		objNewPlanet.transform.localPosition = new Vector2 (floatPlanets_SpawnHoriz, floatPlanets_SpawnHeight);

		intPlanetCounter++;

		if (planetControllerRef_Prev != null) {
			// Check if the new one IS to the LEFT
			if (Planet_IsLeft ()) {

				// Check if previous was indeed to the right
				if (lastPlanetWasTo == LastPlanetWasTo.Right) {
					lastPlanetWasTo = LastPlanetWasTo.Left;

					planetControllerRef_Prev.Planet_OrbitReverse ();
				} 

				// Change direction to make sure zigzag
				else {
					lastPlanetWasTo = LastPlanetWasTo.Right;
					v2_PlanetPositBuffer = planetControllerRef.transform.localPosition;

					floatPlanets_SpawnHoriz = planetControllerRef_Prev.transform.localPosition.x + Random.Range (GameController.Instance.floatPlanetHorizDistance_Forced, GameController.Instance.floatPlanetHorizDistance_Forced + 2F);
					v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
					planetControllerRef.transform.localPosition = v2_PlanetPositBuffer;
				}

			} else {

				if (lastPlanetWasTo == LastPlanetWasTo.Left) {
					lastPlanetWasTo = LastPlanetWasTo.Right;
				} 

				// Change direction to make sure zigzag
				else {
					lastPlanetWasTo = LastPlanetWasTo.Left;
					v2_PlanetPositBuffer = planetControllerRef.transform.localPosition;

					floatPlanets_SpawnHoriz = planetControllerRef_Prev.transform.localPosition.x - Random.Range (GameController.Instance.floatPlanetHorizDistance_Forced, GameController.Instance.floatPlanetHorizDistance_Forced + 2F);
					v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
					planetControllerRef.transform.localPosition = v2_PlanetPositBuffer;

					planetControllerRef_Prev.Planet_OrbitReverse ();
				}
			}

			// To make sure two new planets are NOT in straight line
			if (Planet_HorizDiff () < GameController.Instance.floatPlanetHorizDistance_Forced) {
				v2_PlanetPositBuffer = planetControllerRef.transform.localPosition;
				if (lastPlanetWasTo == LastPlanetWasTo.Right) {
					floatPlanets_SpawnHoriz += GameController.Instance.floatPlanetHorizDistance_Forced;
					v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
				} else {
					floatPlanets_SpawnHoriz -= GameController.Instance.floatPlanetHorizDistance_Forced;
					v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
				}
				planetControllerRef.transform.localPosition = v2_PlanetPositBuffer;
			}
				
			// To mke sure new planet is not too far to the right or left
			if (floatPlanets_SpawnHoriz < -GameController.Instance.floatPlanetHorizLimit) {
				floatPlanets_SpawnHoriz = -GameController.Instance.floatPlanetHorizLimit;
				v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
				planetControllerRef.transform.localPosition = v2_PlanetPositBuffer;
			} 

			if (floatPlanets_SpawnHoriz > GameController.Instance.floatPlanetHorizLimit) {
				floatPlanets_SpawnHoriz = GameController.Instance.floatPlanetHorizLimit;
				v2_PlanetPositBuffer.x = floatPlanets_SpawnHoriz;
				planetControllerRef.transform.localPosition = v2_PlanetPositBuffer;
			}
		}

		// Increase Moon Count
		if (intPlanetCounter != 0) {
			// Now using the pools and also for empty blocks
//			Difficulty_EventPlanets (false);

			// This is to replace the above chance correction
			if (intEventPlanetsCounter == 0) {
				planetType_New = PlanetType.Normal;
			}

			// Now using the start of blocks / events for this
//			Difficulty_TriggerSpawnMover ();

			Difficulty_Increase ();
			Difficulty_ForcedTriggers ();
			Difficulty_GeneralIncrease ();
		}
	}

	/*
	public void Difficulty_PlanetSpecial () {
		if (intPlanetCounter % intPlanetSpecial_All == 0) {
//			planetType_New = (PlanetType)3;
//			planetType_New = (PlanetType)Random.Range (1, 3);
			planetType_New = (PlanetType)Random.Range (1, 4);
			intPlanetSpecial_All = PlanetsCounter_StepSetup (GameController.Instance.intPlanetSpecial_All, intPlanetSpecial_Step);
		} else {
			planetType_New = PlanetType.Normal;
		}
	}
	*/

	public void Difficulty_TriggerSpawnMover () {
		// Spawn top checker
		if ((intPlanetCounter % (GameController.Instance.intSpawnHowMany - 1)) == 0) {
			TopCheckerSpawn ();
		}
	}

	void TopCheckerSpawn () {
		GameObject objNewTopChecker = Instantiate (objTopCheckerPlanetSource);
		objNewTopChecker.name = objTopCheckerPlanetSource.name + " " + (intPlanetCounter).ToString ();
		objNewTopChecker.transform.SetParent (transPlanetsParentHolder);
		objNewTopChecker.transform.localPosition = new Vector2 (0, floatPlanets_SpawnHeight);
	}

	public void Difficulty_Increase () {
		// Increase orbit and planet rotate speeds
		if (GameController.Instance.intTimesMaxOrbitSpeedReached < 5) {
			GameController.Instance.floatPlanetSpeed_Min += GameController.Instance.floatPlanetSpeed_MinIncrease;
			GameController.Instance.floatPlanetSpeed_Max += GameController.Instance.floatPlanetSpeed_MaxIncrease;
			GameController.Instance.floatOrbitSpeed_Min += GameController.Instance.floatOrbitSpeed_MinIncrease;
			GameController.Instance.floatOrbitSpeed_Max += GameController.Instance.floatOrbitSpeed_MaxIncrease;
		}
			
		/* Old Method for orbit speed
		if (intPlanetCounter % intPlanetsIncrease_SpeedReach == 0) {
			GameController.Instance.floatPlanetSpeed_Min += GameController.Instance.floatPlanetSpeed_MinIncrease;
			GameController.Instance.floatPlanetSpeed_Max += GameController.Instance.floatPlanetSpeed_MaxIncrease;
//			GameController.Instance.floatPlanetSpeed_Min += 0.04F;
//			GameController.Instance.floatPlanetSpeed_Max += 0.1F;

			GameController.Instance.floatOrbitSpeed_Min += GameController.Instance.floatOrbitSpeed_MinIncrease;
			GameController.Instance.floatOrbitSpeed_Max += GameController.Instance.floatOrbitSpeed_MaxIncrease;
//			GameController.Instance.floatOrbitSpeed_Min += 0.1F;
//			GameController.Instance.floatOrbitSpeed_Max += 0.4F;

//			intPlanetsIncrease_Speed = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_Speed, intPlanetsCounter_Step);
		}
		*/

		if (GameController.Instance.intIntroSizeSpawnCounter <= GameController.Instance.intPlanetsIncrease_SizeReach) {
			GameController.Instance.intIntroSizeSpawnCounter++;

			GameController.Instance.floatOrbitSize_IntroMin -= GameController.Instance.floatOrbitSize_IntroMinDecrease;
			GameController.Instance.floatOrbitSize_IntroMax -= GameController.Instance.floatOrbitSize_IntroMaxDecrease;
		} 

		// After intro size decreases
		else {
			GameController.Instance.isIntroSectionSizeDone = true;

			// Decrease orbit sizes (Only for orbits)
			if (intPlanetCounter % intPlanetsDecrease_Size == 0) {
				// Planet size decreases used to be here

				GameController.Instance.floatOrbitSize_Min -= GameController.Instance.floatOrbitSize_MinDecrease;
				GameController.Instance.floatOrbitSize_Max -= GameController.Instance.floatOrbitSize_MaxDecrease;

				if (GameController.Instance.floatOrbitSize_Min < GameController.Instance.floatOrbitSize_AbsoluteMin) {
					GameController.Instance.floatOrbitSize_Min = GameController.Instance.floatOrbitSize_AfterAbsolute_Min;
					GameController.Instance.floatOrbitSize_Max = GameController.Instance.floatOrbitSize_AfterAbsolute_Max;
				}
			}
		}

		// Increase orbit and planet rotate speeds
		if (intPlanetCounter % intPlanetsIncrease_HazardChance == 0) {
			GameController.Instance.intMoonChance_Hazard += GameController.Instance.intHazardItselfIncrease;

			if (GameController.Instance.intMoonChance_Hazard > GameController.Instance.intMoonChance_Hazard_AbsoluteMax) {
				GameController.Instance.intMoonChance_Hazard = GameController.Instance.intMoonChance_Hazard_AfterMax;
			}

//			intPlanetsIncrease_HazardChance = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_HazardChance, intPlanetsCounter_Step);
		}

		// Increase max moons chance
		if (intPlanetCounter % intPlanetsIncrease_MoonMax == 0) {

			GameController.Instance.intMoonCountMax += Random.Range (1, 3);

			if (GameController.Instance.intMoonCountMax > 6) {
				GameController.Instance.intMoonCountMax -= 2;
			}
//			intPlanetsIncrease_MoonMax = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_MoonMax, intPlanetsCounter_Step);
		}

		// Increase min moons chance
		if (intPlanetCounter % intPlanetsIncrease_MoonMin == 0) {

			GameController.Instance.intMoonCountMin += 1;

			//			Debug.LogError ("intPlanetCounter = " + intPlanetCounter);

			if (GameController.Instance.intMoonCountMin > 2) {
				GameController.Instance.intMoonCountMin -= 2;
			}

//			intPlanetsIncrease_MoonMin = PlanetsCounter_StepSetup (GameController.Instance.intPlanetsIncrease_MoonMin, intPlanetsCounter_Step);
		}

		// Increase chance of special planets spawned without forced trigger
		if (!isEventPlanetsSet) {
			if (intPlanetSpecialChance_All > Random.Range (0, 100)) {
				planetType_New = Planet_TypeSetup ();

				// Some spawns like this require diff check
				Planet_TypeDiffCheck ();

				intPlanetSpecialChance_All -= Random.Range (1, 4);
				if (intPlanetSpecialChance_All < 0) {
					intPlanetSpecialChance_All = 0;
				}
			} else {
				if (intPlanetCounter % GameController.Instance.intPlanetsIncrease_SpecialChanceAll == 0) {
					intPlanetSpecialChance_All += Random.Range (4, 8);
					if (intPlanetSpecialChance_All > 30) {
						intPlanetSpecialChance_All -= Random.Range (5, 20);
					}
				}
			}
		}
	}

	public void Difficulty_EventPlanets (bool isPoolMethod) {
		if (intEventPlanetsCounter == 0) {

			// This normal only works because it is at the start of diffs / triggers pipeline
			planetType_New = PlanetType.Normal;

			if (intForcedTrigger_Event == 0) {
				if (isPoolMethod) {
					intForcedTrigger_Event = intEventPlanetTarget;
				} else {
					intForcedTrigger_Event = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_Event, intForcedTriggerEvent_Step);
				}

				//
				isEventPlanetsSet = true;

				if (GameController.Instance.intEventPlanets_MixedChancePercent > Random.Range (0, 101)) {
					isEventMixed = true;
				} else {
					isEventMixed = false;
					planetTypeEventBuffer = Planet_TypeSetup (); 
				}

				intEventPlanetsCounter = Random.Range (GameController.Instance.intEventPlanets_CountMin, GameController.Instance.intEventPlanets_CountMax);
				//

			} else {
				intForcedTrigger_Event--;
				isEventPlanetsSet = false;
				intEventPlanetsCounter = 0;
			}

			// Old method
//			if (intPlanetCounter % GameController.Instance.intEventPlanets_Set == 0) {
//				isEventPlanetsSet = true;
//
//				if (GameController.Instance.intEventPlanets_MixedChancePercent > Random.Range (0,101)) {
//					isEventMixed = true;
//				} else {
//					isEventMixed = false;
//					planetTypeEventBuffer = Planet_TypeSetup (); 
//				}
//
//				intEventPlanetsCounter = Random.Range (GameController.Instance.intEventPlanets_CountMin, GameController.Instance.intEventPlanets_CountMax);
//			} else {
//				isEventPlanetsSet = false;
//				intEventPlanetsCounter = 0;
//			}

		} 

		else {
			if (isEventMixed) {
				planetType_New = Planet_TypeSetup (); 
			} else {
				planetType_New = planetTypeEventBuffer; 
			}
//			Debug.LogWarning (intPlanetCounter + " next is " + planetType_New);

			intEventPlanetsCounter--;
		}
	}

	public void Difficulty_ForcedTriggers () {
		// Forced Hazard
		if (planetControllerRef.intMoonCount_Hazard == 0) {
			intForced_HazardCount++;
		} else {
			if (isForced_Hazard) {
				isForced_Hazard = false;
				GameController.Instance.intMoonChance_Hazard = intForced_HazardBuffer;
				intForcedTrigger_Hazard = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_Hazard, intForcedTriggerHazard_Step);
			}
		}
		if (intForced_HazardCount > intForcedTrigger_Hazard) {
			if (!isForced_Hazard) {
				intForced_HazardBuffer = GameController.Instance.intMoonChance_Hazard;
				isForced_Hazard = true;
				intForced_HazardCount = 0;
			}
			GameController.Instance.intMoonChance_Hazard += 500;
		}

		// Forced Special Planet
		if (!isEventPlanetsSet) {
			if (planetControllerRef.planetType == PlanetType.Normal) {
				intForced_PlanetSpecialCount++;
			} else {
				if (isForced_PlanetSpecial) {
					isForced_PlanetSpecial = false;
					planetType_New = PlanetType.Normal;
					intForcedTrigger_SpecialPlanet = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_SpecialPlanet, intForcedTriggerSpecPlanet_Step);
				}
			}

			if (intForced_PlanetSpecialCount > intForcedTrigger_SpecialPlanet) {
				if (!isForced_PlanetSpecial) {
					isForced_PlanetSpecial = true;

					planetType_New = Planet_TypeSetup ();

					Planet_TypeDiffCheck ();

					intForced_PlanetSpecialCount = 0;
				}
			}
		}

		// Forced Moon on Path
		intForced_MoonPathCount++;
		if (isForced_MoonPath) {
			isForced_MoonPath = false;
			Force_Planet_MidwayMoons ();
			intForcedTrigger_MoonPath = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPath, intForcedTriggerMoonPath_Step);

			// To increase odds of short distance between forced moon paths
			if (intForcedTrigger_MoonPath > 2) {
				intForcedTrigger_MoonPath = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPath, intForcedTriggerMoonPath_Step);
			}

			if (intForcedTrigger_MoonPath > 3) {
				intForcedTrigger_MoonPath = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPath, intForcedTriggerMoonPath_Step);
			}

//			Debug.LogWarning ("Moon Forced = " + intForcedTrigger_MoonPath);
		}

		if (intForced_MoonPathCount > intForcedTrigger_MoonPath) {
			if (!isForced_MoonPath) {
				isForced_MoonPath = true;
				intForced_MoonPathCount = 0;
			}
		}
	}

	public void MoonHeightIncrease_Calculate (bool isGroup) {
		if (GameController.Instance.isIntroSectionSizeDone) {
			switch (GameController.Instance.intDifficultyCounter) {
			case 1:
				floatMoonHeightIncrease = 1.1F;
				break;
			case 2:
				floatMoonHeightIncrease = 1.3F;
				break;
			case 3:
			case 4:
			case 5:
				floatMoonHeightIncrease = 1.45F;
				break;
			case 6:
			case 7:
				floatMoonHeightIncrease = 1.5F;
				break;
			case 8:
			case 9:
				floatMoonHeightIncrease = 1.55F;
				break;
			default:
				Debug.LogError ("Default Error");
				break;
			}
		} else {
			floatMoonHeightIncrease = 0.25F;
		}

		if (isGroup) {
			floatMoonHeightIncrease += 1.2F;
		}
	}

	public void Force_Planet_MidwayMoons () {
		intForced_MoonPathGroupCount++;
		if (intForced_MoonPathGroupCount > intForcedTrigger_MoonPathGroup) {
			isMoonPathGroup = true;

//			floatMoonHeightIncrease = 2.7F;
			MoonHeightIncrease_Calculate (true);

			intForcedTrigger_MoonPathGroup = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPathGroup, intForcedTriggerMoonPathGroup_Step);
			if (intForcedTrigger_MoonPathGroup > 2) {
				intForcedTrigger_MoonPathGroup = PlanetsCounter_StepSetup (GameController.Instance.intForcedTrigger_MoonPathGroup, intForcedTriggerMoonPathGroup_Step);
			}

			intForced_MoonPathGroupCount = 0;
		} else {
			isMoonPathGroup = false;

//			floatMoonHeightIncrease = 1.75F;
			MoonHeightIncrease_Calculate (false);
		}

		if (GameController.Instance.isIntroSectionSizeDone) {
			while ((planetControllerRef.floatOrbitSize + floatPrevPlanetHolder + floatMoonHeightIncrease) > PlanetsDistance_Calculate ()) {
				// Increase height when we need moons
				floatPlanets_SpawnHeight += 0.5F;
				planetControllerRef.transform.localPosition = new Vector2 (planetControllerRef.transform.localPosition.x, floatPlanets_SpawnHeight);
			}
		} 
		else {
			while ((planetControllerRef.floatOrbitSize + floatPrevPlanetHolder + floatMoonHeightIncrease) > (PlanetsDistance_Calculate () + 0.05F)) {
				// Increase height when we need moons
				floatPlanets_SpawnHeight += 0.35F;
				planetControllerRef.transform.localPosition = new Vector2 (planetControllerRef.transform.localPosition.x, floatPlanets_SpawnHeight);
			}
		}
			
		Spawn_MoonOnPath ();
//		SpawnText ();
	}

	public void Spawn_MoonOnPath () {
		// Moon Position
		switch (moonPath_PositType) {
		case MoonPath_PositType.Midpoint_Core:
			v2_MoonPathPositBuffer = BetweenPlanets_MidOrbits (0.5F);
			break;
		case MoonPath_PositType.Midpoint_Orbits:
			v2_MoonPathPositBuffer = BetweenPlanets_MidOrbits ((planetControllerRef_Prev.floatOrbitSize 
				+ (PlanetsDistance_Calculate () * 1.5F - PlanetsOrbitsSums_Calculate () ) / 2) 
				/ (PlanetsDistance_Calculate () * 1.5F));
			break;
		case MoonPath_PositType.CloserToStart:
			v2_MoonPathPositBuffer = BetweenPlanets_MidOrbits (planetControllerRef_Prev.floatOrbitSize / PlanetsOrbitsSums_Calculate ());
			break;
		case MoonPath_PositType.CloserToEnd:
			v2_MoonPathPositBuffer = BetweenPlanets_MidOrbits (planetControllerRef.floatOrbitSize / PlanetsOrbitsSums_Calculate ());
			break;
		default:
			break;
		}

		// Moon Rotation
		floatMoonPathRotation = Mathf.Atan2 (planetControllerRef.transform.localPosition.y - planetControllerRef_Prev.transform.localPosition.y, 
			planetControllerRef.transform.localPosition.x - planetControllerRef_Prev.transform.localPosition.x) * Mathf.Rad2Deg + 90;

		// Spawn Single MoonPath
		if (!isMoonPathGroup) {
			GameObject objNewMoon = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
			objNewMoon.transform.SetParent (transform);
			objNewMoon.transform.localScale = Vector3.one;
			objNewMoon.transform.localPosition = v2_MoonPathPositBuffer;
			objNewMoon.transform.eulerAngles = new Vector3 (0, 0, floatMoonPathRotation);

			objNewMoon.GetComponent <MoonController> ().Moon_Setup (GetMoon_Single (false), floatSingleMoonStandardSize, false);

			transNewMoonBuffer = objNewMoon.transform;
		} 

		// Spawn Group MoonPath
		else {
			GameObject objNewMoon = Instantiate (transMoonPathGroupParent.GetChild(Random.Range (0,2)).gameObject);
			objNewMoon.transform.SetParent (transform);
			objNewMoon.transform.localScale = Vector3.one;
			objNewMoon.transform.localPosition = v2_MoonPathPositBuffer;
			objNewMoon.transform.eulerAngles = new Vector3 (0, 0, floatMoonPathRotation);

//			Debug.LogWarning ("PlanetsOrbitsDistance_Calculate = " + PlanetsOrbitsDistance_Calculate ());
			objNewMoon.GetComponent<MoonPath_GroupSpawner> ().MoonPathGroup_Setup (PlanetsOrbitsDistance_Calculate () * 0.16F, true);

			transNewMoonBuffer = objNewMoon.transform;
		}
		intMoonPathCounter++;
	}

	public MoonType GetMoon_Single (bool canHazard) {

		intMoonChance_Hazard = GameController.Instance.intMoonChance_Hazard;
		intMoonChance_Currency = GameController.Instance.intMoonChance_Currency + intMoonChance_Hazard;
		intMoonChance_Superscore = GameController.Instance.intMoonChance_SuperScore + intMoonChance_Currency;
		intMoonChance_Autoplay = GameController.Instance.intMoonChance_Autoplay + intMoonChance_Superscore;
		intMoonChance_Total = intMoonChance_Autoplay;

		int intRandomChanceMoon = Random.Range (0, intMoonChance_Total);

		if (intRandomChanceMoon < intMoonChance_Hazard) {
			if (canHazard) {
				return MoonType.Hazard;
			} else {
				return MoonType.Currency;
			}
		} else {
			if (intRandomChanceMoon < intMoonChance_Currency) {
				return MoonType.Currency;
			} else {
				if (intRandomChanceMoon < intMoonChance_Superscore) {
					return MoonType.SuperCurrency;
				} else {
					if (intRandomChanceMoon < intMoonChance_Autoplay) {
//					return MoonType.AutoPlay;
						return MoonType.Currency;
					} else {
//					return MoonType.AutoPlay;
						return MoonType.Currency;
					}

				}
			}
		}
	}

	public void SpawnText () {
		GameObject objNewText = Instantiate (transDistanceTextCanvas.gameObject);
		objNewText.transform.SetParent (transNewMoonBuffer);
		objNewText.transform.localScale = new Vector3 (0.05F, 0.05F, 0.05F);
		objNewText.transform.localPosition = Vector2.zero;
		objNewText.transform.localEulerAngles = Vector3.zero;

		objNewText.GetComponentInChildren<UnityEngine.UI.Text> ().text = planetControllerRef.name + " <color=green>Yes</color>";

		/*
		//		objNewText.transform.SetParent (transform);
		//		objNewText.transform.localScale = new Vector3 (0.05F, 0.05F, 0.05F);
		//		objNewText.transform.localPosition = v2_MoonPathPositBuffer;
		//		objNewText.transform.eulerAngles = new Vector3 (0, 0, floatMoonPathRotation);

		//		objNewText.transform.localPosition = ((planetControllerRef.transform.localPosition * planetControllerRef.floatOrbitSize)
		//			+ (planetControllerRef_Prev.transform.localPosition * planetControllerRef_Prev.floatOrbitSize)) 
		//			/ (planetControllerRef_Prev.floatOrbitSize + planetControllerRef_Prev.floatOrbitSize);


		//		+
		//			"\n" + (PlanetsDistance_Calculate).ToString () +
		//			"\n" + (planetControllerRef.floatOrbitSize + floatPrevPlanetHolder).ToString ();
		*/
	}

	public Vector2 BetweenPlanets_MidOrbits (float orbitsRatio) {
		//		Debug.LogWarning (planetControllerRef.name + "  to BetweenPlanets_MidOrbits  -->  ratio = " + orbitsRatio);

		return (planetControllerRef_Prev.transform.localPosition 
			+ (planetControllerRef.transform.localPosition - planetControllerRef_Prev.transform.localPosition) * orbitsRatio);
	}

	void Difficulty_GeneralIncrease () {
		if (intPlanetCounter % GameController.Instance.intDifficultyIncreasePerPlanet == 0) {
			if (GameController.Instance.intDifficultyCounter < GameController.Instance.intDifficultyMax) {
				GameController.Instance.intDifficultyCounter++;
			}
		}
	}

	public float PlanetsDistance_Calculate () {
		return Vector2.Distance (planetControllerRef.transform.localPosition, planetControllerRef_Prev.transform.localPosition);
	}

	public float PlanetsOrbitsSums_Calculate () {
		return (planetControllerRef.floatOrbitSize + planetControllerRef_Prev.floatOrbitSize);
	}

	public float PlanetsOrbitsDistance_Calculate () {
		return (PlanetsDistance_Calculate () - PlanetsOrbitsSums_Calculate ());
	}

	public void Planet_TypeDiffCheck () {
//		Debug.LogError ("intPlanetSpecial_Count_All = " + intPlanetSpecial_Count_All
//		+ "\n" + "Planet Name = " + planetControllerRef.name);
//		Debug.LogError ("intForcedTrigger_SpecialPlanet = " + intForcedTrigger_SpecialPlanet);
		
		if (intPlanetSpecial_Count_All < 3) {
			if (planetType_New == PlanetType.ShrinkingBomb) {
				planetType_New = (PlanetType)Random.Range (1, 3);
			}
		} else {

			// To make sure this diff correction doesn't happen always
			if (Random.Range (0, 100) > 98) {

				if (intPlanetSpecial_Count_All == 3) {
					planetType_New = PlanetType.ShrinkingBomb;
				} else {

					if (intPlanetSpecial_Count_All > 4) {
						if (Random.Range (0, 2) == 0) {
							if ((intPlanetSpecial_Count_IrregularRot + 2) < intPlanetSpecial_Count_BigSmall) {
								planetType_New = PlanetType.IrregularRotate;
							}
						} else {
							if ((intPlanetSpecial_Count_ShrinkingBomb + 2) < intPlanetSpecial_Count_BigSmall) {
								planetType_New = PlanetType.ShrinkingBomb;
							}
						}
					}
				}

			}
		}
	}

	public PlanetType Planet_TypeSetup () {
		if (intPlanetCounter > 1) {
			intForced_PlanetSpecialRandomNumber = Random.Range (0, intPlanetSpecialChance_TOTAL);
			intPlanetSpecial_Count_All++;

			if (intForced_PlanetSpecialRandomNumber < intPlanetSpecialChance_BigSmall) {
				intPlanetSpecial_Count_BigSmall++;
				return PlanetType.BigSmall;
			} else {
				if (intForced_PlanetSpecialRandomNumber < intPlanetSpecialChance_IrregularRotate) {
					intPlanetSpecial_Count_IrregularRot++;
					return PlanetType.IrregularRotate;
				} else {
					if (intForced_PlanetSpecialRandomNumber < intPlanetSpecialChance_ShrinkingBomb) {
						intPlanetSpecial_Count_ShrinkingBomb++;
						return PlanetType.ShrinkingBomb;
					} else {
						return (PlanetType)Random.Range (1, 4);
//					return PlanetType.Other;
					}
				}
			}
		} 

		else {
			isForced_PlanetSpecial = false;
			if (intForcedTrigger_SpecialPlanet == 0) {
				intForcedTrigger_SpecialPlanet += 2;
			}
			return PlanetType.Normal;
		}
	}
}
