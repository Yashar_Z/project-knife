﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_AnimsScript : MonoBehaviour {

	public void AnimEvent_KnifeMenuToGameEnd () {
		GameController.Instance.playerController.Start_GamePlayer ();
	}

	// Not Used?
	public void AnimEvent_HalfOfScale () {
		PlanetController plantContRef = FindObjectOfType<PlayerController> ().planetControllerRef;
		Animation animRef = FindObjectOfType<PlayerController> ().animPlayerHolder;

		Debug.Log (plantContRef.transform.GetChild (2).name + " is at = " + plantContRef.transform.GetChild (2).GetChild (0).localScale.x);
		Debug.Log (this.name + " is at = " + transform.localScale.x);
		Debug.LogWarning ("Multiply at = " + transform.localScale.x * plantContRef.transform.GetChild (2).GetChild (0).localScale.x);
		Debug.LogError ("PLAYER TIME is at = " + animRef [animRef.clip.name].time);
		Debug.Break ();

		// 
	}
}
