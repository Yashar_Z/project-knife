﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesignerDisabler : MonoBehaviour {

	public Transform transDesignerBlock;

	// Use this for initialization
	void Start () {
		if (!GameController.Instance.isDesignerMode) {
			for (int i = 0; i < transDesignerBlock.childCount; i++) {
				for (int j = 0; j < transDesignerBlock.GetChild (i).childCount; j++) {
					transDesignerBlock.GetChild (i).GetChild (j).gameObject.SetActive (false);
				}
			}
		}
	}
}
