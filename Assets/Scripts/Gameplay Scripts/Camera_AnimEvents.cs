﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_AnimEvents : MonoBehaviour {

	public Camera cameraHolder;

	public Transform transAllSpritesParent;

	public Transform transCheckpointParent;
	public Transform transFlareLineHolder;
	public Transform transCheckpointText;

	public Transform transBonusContinParent;
	public Transform transBonusContinText;

	private Vector3 v3_CheckpointTextPositBuffer;

	public void AnimeEvent_CheckpointAtStart () {
		transCheckpointText.SetParent (transCheckpointParent);
		transCheckpointText.localPosition = Vector3.zero;

		transFlareLineHolder.SetParent (transAllSpritesParent);
		transFlareLineHolder.position = GameController.Instance.planetController_Selected.transform.position;
		transFlareLineHolder.position = new Vector3 (transFlareLineHolder.position.x, transFlareLineHolder.position.y, 0);

//		v3_CheckpointTextPositBuffer = transCheckpointText.position;
//		Debug.LogError ("v3_CheckpointTextPositBuffer = " + v3_CheckpointTextPositBuffer);
	}

	public void AnimeEvent_CheckpointAfterEnd () {
		transCheckpointText.SetParent (transAllSpritesParent);
//		transCheckpointText.position = v3_CheckpointTextPositBuffer;
//		Debug.LogError ("v3_CheckpointTextPositBuffer = " + v3_CheckpointTextPositBuffer);
	}

	public void AnimeEvent_BonusContinueStart () {
		transBonusContinText.SetParent (transBonusContinParent);
		transBonusContinText.localPosition = Vector3.zero;
	}

	public void AnimeEvent_BonusContinueAfterEnd () {
		transBonusContinText.SetParent (transAllSpritesParent);
		GameController.Instance.menuScript.BonusContinue_ParticlesDisable ();
	}

	public void AnimeEvent_ShineByText () {
//		GameController.Instance.particleWhiteFlash_Quick.Play ();
		GameController.Instance.particleCheckpointFlare_Full.Play ();
	}
}
