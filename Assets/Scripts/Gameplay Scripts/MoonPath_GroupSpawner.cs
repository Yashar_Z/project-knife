﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonPath_GroupSpawner : MonoBehaviour {

	public AnimationClip[] animClipArr;
	public Animation animMoonPathGroupHolder;

	public MoonPath_GroupType moonPath_GroupType;

	public MoonController[] moonControllerArr;
	public MoonType[] MoonTypeArr;

	public float floatAnimMoonPath;
	public float floatMoonVertDistance;

	public bool isMovelessGroup;
	public bool isMoonMoveSpeedPreset;

	public bool isDesignerMoon;

	public void MoonPathGroup_DistanceAndAngle () {
	}

	public void MoonPathGroup_Setup (float vertDistance, bool isRandomType) {
		floatMoonVertDistance = vertDistance;

		if (MoonTypeArr.Length < moonControllerArr.Length) {
			MoonTypeArr = new MoonType[moonControllerArr.Length];
			for (int i = 0; i < moonControllerArr.Length; i++) {
//				MoonTypeArr [i] = (MoonType)Random.Range (1, 3);	
				MoonTypeArr [i] = GameController.Instance.gameSpawner.GetMoon_Single (false);
			}
		}

		MoonGroup_Distances (MoonTypeArr.Length);

		if (isRandomType) {
			moonPath_GroupType = (MoonPath_GroupType)Random.Range (0, 12);

			switch (moonPath_GroupType) {
			case MoonPath_GroupType.Double_1:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
				break;
			case MoonPath_GroupType.Double_2:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
				break;
			case MoonPath_GroupType.Double_3:
				animMoonPathGroupHolder.clip = animClipArr [2];
				break;
			case MoonPath_GroupType.Triple_1:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
				break;
			case MoonPath_GroupType.Triple_2:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
				break;
			case MoonPath_GroupType.Triple_3:
				animMoonPathGroupHolder.clip = animClipArr [2];

				break;
			case MoonPath_GroupType.Double_4_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (3, 6)];
				break;
			case MoonPath_GroupType.Double_5_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (3, 6)];
				break;
			case MoonPath_GroupType.Double_6_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [5];
				break;
			case MoonPath_GroupType.Triple_4_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (3, 6)];
				break;
			case MoonPath_GroupType.Triple_5_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [Random.Range (3, 6)];
				break;
			case MoonPath_GroupType.Triple_6_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [5];
				break;

//			case MoonPath_GroupType.Double_7_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
//				break;
//			case MoonPath_GroupType.Double_8_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
//				break;
//			case MoonPath_GroupType.Double_9_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [2];
//				break;
//			case MoonPath_GroupType.Triple_7_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
//				break;
//			case MoonPath_GroupType.Triple_8_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [Random.Range (0, 3)];
//				break;
//			case MoonPath_GroupType.Triple_9_ShortHoriz:
//				animMoonPathGroupHolder.clip = animClipArr [2];
//				break;

			default:
				break;
			}
		} 

		else {

			switch (moonPath_GroupType) {
			case MoonPath_GroupType.Double_1:
				animMoonPathGroupHolder.clip = animClipArr [0];
				break;
			case MoonPath_GroupType.Double_2:
				animMoonPathGroupHolder.clip = animClipArr [1];
				break;
			case MoonPath_GroupType.Double_3:
				animMoonPathGroupHolder.clip = animClipArr [2];
				break;
			case MoonPath_GroupType.Double_4_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [3];
				break;
			case MoonPath_GroupType.Double_5_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [4];
				break;
			case MoonPath_GroupType.Double_6_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [5];
				break;
			case MoonPath_GroupType.Double_7_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [6];
				break;
			case MoonPath_GroupType.Double_8_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [7];
				break;
			case MoonPath_GroupType.Double_9_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [8];
				break;

			case MoonPath_GroupType.Triple_1:
				animMoonPathGroupHolder.clip = animClipArr [0];
				break;
			case MoonPath_GroupType.Triple_2:
				animMoonPathGroupHolder.clip = animClipArr [1];
				break;
			case MoonPath_GroupType.Triple_3:
				animMoonPathGroupHolder.clip = animClipArr [2];
				break;
			case MoonPath_GroupType.Triple_4_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [3];
				break;
			case MoonPath_GroupType.Triple_5_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [4];
				break;
			case MoonPath_GroupType.Triple_6_MidHoriz:
				animMoonPathGroupHolder.clip = animClipArr [5];
				break;
			case MoonPath_GroupType.Triple_7_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [6];
				break;
			case MoonPath_GroupType.Triple_8_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [7];
				break;
			case MoonPath_GroupType.Triple_9_ShortHoriz:
				animMoonPathGroupHolder.clip = animClipArr [8];
				break;
			default:
				break;
			}
		}

		if (!isMoonMoveSpeedPreset) {
			floatAnimMoonPath = Random.Range (0.4F, 0.9F);
		} else {
			if (floatAnimMoonPath < 0.1F) {
				floatAnimMoonPath = 0.1F;
			}
		}

		for (int i = 0; i < moonControllerArr.Length; i++) {
			moonControllerArr [i].isDesignerMoon = isDesignerMoon;
			moonControllerArr [i].Moon_Setup (MoonTypeArr [i], 1.8F, false);
//			moonControllerArr [i].Moon_Setup (MoonTypeArr [i], 1.5F, false);
		}
			
		if (!isMovelessGroup) {
			animMoonPathGroupHolder [animMoonPathGroupHolder.clip.name].speed = floatAnimMoonPath;
			animMoonPathGroupHolder.Play ();
		}
	}

	void MoonGroup_Distances (int moonCount) {
		if (moonCount == 2) {
			transform.GetChild (0).localPosition = new Vector2 (0, floatMoonVertDistance);
			transform.GetChild (1).localPosition = new Vector2 (0, -floatMoonVertDistance);
		} else {
			transform.GetChild (0).localPosition = new Vector2 (0, floatMoonVertDistance / 2);
			transform.GetChild (1).localPosition = Vector2.zero;
			transform.GetChild (2).localPosition = new Vector2 (0, -floatMoonVertDistance / 2);
		}
	}
}
