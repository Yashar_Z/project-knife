﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_Script : MonoBehaviour {

	public int intCurrHUDBuffer;

	[Header ("Animations")]
	public AnimationClip[] animClipHUD_CurrencyArr;
	public Animation animHUD_CurrencyHolder;

	public AnimationClip[] animClipHUD_ScoreArr;
	public Animation animHUD_ScoreHolder;

	public Animation animCheckpointHolder;
//	public AnimationState animStateCheckpoint;
	public AnimationClip[] animClipCheckpointArr;

	public Animation animRemainHolder;

	[Header ("Texts")]
	public Text textHUD_CurrencyHolder;
	public Text textHUD_ScoreHolder;
	public Text textCheckpointRemain;

	public Text textCheckP_RemainHolder1;
	public Text textCheckP_RemainHolder2;

	public Text textCheckpointFullHolder1;
	public Text textCheckpointFullHolder2;

	public int intCheckpointNumber;

	public ParticleSystem particleCurrencyManyGatherVid;

	void Start () {
		CheckpointRemainEnabler (false);
//		animStateCheckpoint = animCheckpointHolder [animCheckpointHolder.clip.name];
	}

	public void Currency_Update_YesInstant (int currency) {
		intCurrHUDBuffer = currency;
		textHUD_CurrencyHolder.text = intCurrHUDBuffer.ToString ();

		if (intCurrHUDBuffer != 0) {
			CurrencyAnimate_Gain ();
		}
	}

	public void Currency_Update_NoInstant () {
		intCurrHUDBuffer++;
		textHUD_CurrencyHolder.text = intCurrHUDBuffer.ToString ();

		CurrencyAnimate_Gain ();
	}

	public void CurrencyAnimate_Gain () {
		animHUD_CurrencyHolder.Stop ();
		animHUD_CurrencyHolder.clip = animClipHUD_CurrencyArr [0];
		animHUD_CurrencyHolder.Play ();
	}

	public void CurrencyAnimate_NotEnough () {
		animHUD_CurrencyHolder.Stop ();
		animHUD_CurrencyHolder.clip = animClipHUD_CurrencyArr [1];
		animHUD_CurrencyHolder.Play ();
	}

	public void Score_Update (int score) {
		textHUD_ScoreHolder.text = score.ToString ();

		if (score != 0) {
			Score_UpdateNormal ();
		}
	}

	public void CheckpointRemainEnabler (bool isActive) {
		textCheckpointRemain.enabled = isActive;
	}

	public void Checkpoint_RemainUpdate (int checkpoint) {
		intCheckpointNumber = checkpoint;

//		animCheckpointHolder.Rewind ();
		CheckpointRemainEnabler (true);

		if (!GameController.Instance.isCheckpointTimeActive) {
//			Checkpoint_CheckpointNormal ();
		}

		else {
			intCheckpointNumber = 0;
//			Checkpoint_CheckpointReachYes ();
		}

		// Only show this for final count down OR every 10 times
		if ((intCheckpointNumber < 10) 
			|| (intCheckpointNumber % 5 == 0) 

			// This one is for showing checkpoint for the first time after clearing
			|| (GameController.Instance.intCheckpoint_Target - 1 == intCheckpointNumber)) {

//			textCheckpointRemain.text = (intCheckpointNumber).ToString ();
			if (intCheckpointNumber < 10) {
				Checkpoint_RemainNumber (GameController.Instance.planetController_Selected.transform.position, false);
			} else {
				Checkpoint_RemainNumber (GameController.Instance.planetController_Selected.transform.position, true);
			}

			switch (intCheckpointNumber) {
			case 9:
				// SOUND EFFECTS: Checkpoint Remain Countdown
				break;
			case 8:
				break;
			case 7:
				break;
			case 6:
				break;
			case 5:
				break;
			case 4:
				break;
			case 3:
				break;
			case 2:
				break;
			case 1:
				break;
			case 0:
				// SOUND EFFECTS: Checkpoint Remain ANY TIME NOW
				break;
			default:
				break;
			}

		}
	}

	public void Checkpoint_RemainNumber (Vector3 v3Position, bool hasLeftText) {

		animRemainHolder.Stop ();

//		Debug.Log ("GameController.Instance.planetController_Selected.floatOrbitSize = " + GameController.Instance.planetController_Selected.floatOrbitSize);
		v3Position.y += (GameController.Instance.planetController_Selected.floatOrbitSize * 0.75F);
		animRemainHolder.transform.position = v3Position;
		animRemainHolder.Play ();

		if (hasLeftText) {
			textCheckP_RemainHolder1.text = intCheckpointNumber.ToString () + " Left";
			textCheckP_RemainHolder2.text = intCheckpointNumber.ToString () + " Left";
		} else {
			textCheckP_RemainHolder1.text = intCheckpointNumber.ToString ();
			textCheckP_RemainHolder2.text = intCheckpointNumber.ToString ();
		}
	}

	public void Checkpoint_CheckpointNormal () {
		animCheckpointHolder.clip = animClipCheckpointArr [0];
		animCheckpointHolder.Play ();
	}

	public void Checkpoint_CheckpointReachYes () {
		animCheckpointHolder.clip = animClipCheckpointArr [1];
		animCheckpointHolder.Play ();
	}

	public void Checkpoint_FullNameUpdate () {
		textCheckpointFullHolder1.text = "Level " + GameController.Instance.intCheckpoint_TotalReached.ToString ();
		textCheckpointFullHolder2.text = textCheckpointFullHolder1.text;
	}

	public void Score_UpdateNormal () {
		animHUD_ScoreHolder.clip = animClipHUD_ScoreArr [0];
		animHUD_ScoreHolder.Play ();
	}

	public void Score_UpdateAnimNew () {
		animHUD_ScoreHolder.clip = animClipHUD_ScoreArr [0];
		animHUD_ScoreHolder.Play ();
	}

	public void Score_EnterHUD () {
		Debug.LogWarning ("Enter HUD SCORE!?");
		animHUD_ScoreHolder.clip = animClipHUD_ScoreArr [1];
		animHUD_ScoreHolder.Play ();
	}

	public void Score_LeaveHUD () {
		animHUD_ScoreHolder.clip = animClipHUD_ScoreArr [2];
		animHUD_ScoreHolder.Play ();
	}
}
