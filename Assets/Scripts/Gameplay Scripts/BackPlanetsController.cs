﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackPlanetsController : MonoBehaviour {

	public Transform transOtherBackPlanet;
	public BackPlanetsController backPlanetController_Other;

	public SpriteRenderer spriteRenderer_SpacePlanet;
	public SpriteRenderer spriteRenderer_SpaceCloud1;
	public SpriteRenderer spriteRenderer_SpaceCloud2;
	public SpriteRenderer spriteRenderer_SpaceCloud3;

	public int intBackPlanetSet;
	public int intBackPlanetColorSet;

	public bool isFirstBackPlanet;

	public Transform transSpacePlanet_Holder;
	public Transform transSpaceCloud1_Holder;
	public Transform transSpaceCloud2_Holder;
	public Transform transSpaceCloud3_Holder;

	private Transform[] transArr_BackPlanetsRefSet;
	private Transform transPlayerHolder;

	private bool canUpdateHappen;

	private int intColorCounter;

	void Start () {
		transArr_BackPlanetsRefSet = GameController.Instance.otherMatController.transBackPlanetRefSetArr;
		canUpdateHappen = true;

		if (isFirstBackPlanet) {
			GameController.Instance.intBackPlanet_SetHeigth = Random.Range (47, 69);
			StartCoroutine (BackPlanet_Setup ());

			StartCoroutine (StartSecondBackPlanet ());
			BackPlanet_PartsEnabler (true);
		} else {
			BackPlanet_PartsEnabler (false);
		}
	}

	IEnumerator StartSecondBackPlanet () {
		yield return null;
		yield return new WaitForSeconds (0.2F);
		StartCoroutine (backPlanetController_Other.BackPlanet_Setup ());
		yield return new WaitForSeconds (0.1F);
		backPlanetController_Other.BackPlanet_PartsEnabler (true);

	}

	public void BackPlanet_PartsEnabler (bool isEnabled) {
		spriteRenderer_SpacePlanet.enabled = isEnabled;
		spriteRenderer_SpaceCloud1.enabled = isEnabled;
		spriteRenderer_SpaceCloud2.enabled = isEnabled;

		// Front / 3rd layer of clouds disabled because of bad visuals
		spriteRenderer_SpaceCloud3.enabled = false;
		spriteRenderer_SpaceCloud3.gameObject.SetActive (false);
	}

	void OnEnable () {
		PlayerController.OnPlayerAttack += onPlayerAttack;
	}

	void OnDisable () {
		PlayerController.OnPlayerAttack -= onPlayerAttack;
	}

	void onPlayerAttack () {
		if ((GameController.Instance.camController.transform.position.y - transform.position.y) > 132) {
			StartCoroutine (BackPlanet_Setup ());
//			Destroy (this.gameObject);
		}
	}

	void Update () {
		if (canUpdateHappen) {
			if ((GameController.Instance.camController.transform.position.y - transform.position.y) > 132) {
				canUpdateHappen = false;
				StartCoroutine (DelayNewHeight ());

				StartCoroutine (BackPlanet_Setup ());
			}
		}
	}

	void BackPlanet_HeightIncrease () {
		GameController.Instance.intBackPlanet_SetHeigth += Random.Range (135, 160);
//		Debug.LogError ("New height = " + GameController.Instance.intBackPlanet_SetHeigth);
	}

	IEnumerator DelayNewHeight () {
		yield return new WaitForSeconds (2);
		canUpdateHappen = true;
	}

	public IEnumerator BackPlanet_Setup () {
//		BackPlanet_PartsEnabler (false);
		transform.localPosition = new Vector3 (Random.Range (-1.5F, 1.5F), GameController.Instance.intBackPlanet_SetHeigth, 0);

		intBackPlanetSet = Random.Range (0, 5);

		transSpacePlanet_Holder.localPosition = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localPosition;
		transSpacePlanet_Holder.localEulerAngles= transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localEulerAngles;
		transSpacePlanet_Holder.localScale = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localScale;

		yield return null;

		transSpacePlanet_Holder.localPosition = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localPosition;
		transSpacePlanet_Holder.localEulerAngles= transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localEulerAngles;
		transSpacePlanet_Holder.localScale = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(0).localScale;

		transSpaceCloud1_Holder.localPosition = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(1).localPosition;
		transSpaceCloud1_Holder.localEulerAngles= transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(1).localEulerAngles;
		transSpaceCloud1_Holder.localScale = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(1).localScale;

		transSpaceCloud2_Holder.localPosition = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(2).localPosition;
		transSpaceCloud2_Holder.localEulerAngles= transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(2).localEulerAngles;
		transSpaceCloud2_Holder.localScale = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(2).localScale;

		// Front / 3rd layer of clouds disabled because of bad visuals
//		transSpaceCloud3_Holder.localPosition = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(3).localPosition;
//		transSpaceCloud3_Holder.localEulerAngles= transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(3).localEulerAngles;
//		transSpaceCloud3_Holder.localScale = transArr_BackPlanetsRefSet[intBackPlanetSet].GetChild(3).localScale;

		intBackPlanetColorSet = Random.Range (0, GameController.Instance.spriteController.colorArr_BackPlanets.Length);
		spriteRenderer_SpacePlanet.color = GameController.Instance.spriteController.colorArr_BackPlanets [intBackPlanetColorSet];

		Color colorBuffer = GameController.Instance.spriteController.colorArr_BackSmokes [intBackPlanetColorSet];
//		colorBuffer.a = 0.6F;

		spriteRenderer_SpaceCloud1.color = colorBuffer;
		spriteRenderer_SpaceCloud2.color = colorBuffer;
//		spriteRenderer_SpaceCloud3.color = colorBuffer;

		yield return null;
//		BackPlanet_PartsEnabler (true);

		if (Random.Range (0,2) == 1) {
			transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}

		// Increase total height
		BackPlanet_HeightIncrease ();

	}
}
