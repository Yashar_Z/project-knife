﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_Collider : MonoBehaviour {

	public static System.Action OnPlayerCamDeath;

	public SpriteRenderer spriteRendHolder;

	public enum CamColliderType {
		Cam_Move,
		Cam_Death,
		Cam_Deleter
	}

	public CamColliderType camColliderType;

	void Start () {
		spriteRendHolder.enabled = false;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			Debug.Log (this.name + "CAM COLLIED = player");
			switch (camColliderType) {
			case CamColliderType.Cam_Move:
				break;
			case CamColliderType.Cam_Death:
				if (GameController.Instance.canDie && (PlayerController.playerStatus != PlayerController.PlayerStatus.IsDead)) {
					if (OnPlayerCamDeath != null) {
						OnPlayerCamDeath ();

                            // SOUND EFFECTS: Player Death By Camera (done)
                            if (SoundManager.Instance != null)
                            {
                                SoundManager.Instance.MargBaKhoroj.Play();
                            }      
					}
				}
				break;
			case CamColliderType.Cam_Deleter:
				break;
			default:
				break;
			}
		} else {
			if (camColliderType == CamColliderType.Cam_Deleter) {
//				Debug.LogError ("CAM COLLIED DELETE");
				if (other.tag == "Planet") {
					Destroy (other.GetComponent<Planet_ColliderScript> ().planetController.gameObject);
				}
		
				if (other.tag == "Moon") {
					Destroy (other.gameObject);
				}
			}
		}
	}
}
