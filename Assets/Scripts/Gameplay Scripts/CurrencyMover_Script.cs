﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyMover_Script : MonoBehaviour {

	public bool isSingleCoin;
	public bool canMove;
	public Animation animCurrMoverHolder;

	private float floatSpeed = 0.6F;
	private Vector3 v3_BufferTarget;

	// Update is called once per frame

	void Start () {
		if (isSingleCoin) {
			StartCoroutine (CoinMoveDelay ());
		}
	}

	IEnumerator CoinMoveDelay () {
		animCurrMoverHolder.Play ();
		yield return new WaitForSeconds (0.25F);
		canMove = true;
	}

	void Update () {
		if (canMove) {
			v3_BufferTarget = GameController.Instance.transCurrMoverTargetHolder.position;

			transform.position = Vector3.MoveTowards (transform.position, v3_BufferTarget, Time.deltaTime * floatSpeed);
//			Debug.LogError (this.name + " is going from " + transform.localPosition + " from " + GameController.Instance.transUI_CurrHolder.position); 

			if (floatSpeed < 30) {
				floatSpeed = Mathf.MoveTowards (floatSpeed, 30, Time.deltaTime * 18);
			}

			if (Vector3.Distance (transform.localPosition, GameController.Instance.transCurrMoverTargetHolder.position) < 0.01F) {
//				GameController.Instance.particleCurrencyMoonHolder_NewLoop.transform.SetParent (GameController.Instance.transParentCurrParticle);

				GameController.Instance.hudScriptHolder.Currency_Update_NoInstant ();
				Destroy (this.gameObject);
			}
		}
	}
}
