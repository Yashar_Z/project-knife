﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner_Planet : MonoBehaviour {

	[Header ("")]
	public PlanetType planetType;

	[Header ("Planet Elements")]

	public float planetCoreSize;
	public float planetOrbitSize;

	public float planetSpeed_Core;
	public float planetSpeed_Orbit;

	public bool isOrbitSpeedPreset;

	[Header ("Special Planet Elements")]
	public float planetSpecialSpeed;
	public int planetWhichAnim;

	[Header ("Moon Elements")]
	public MoonType[] moonTypesArr;
	public bool areMoonsPreset;

//	private MoonController[] moonControllerArr;

	private int intMoonCount_Hazard;
	private int intMoonCount_Total;
	private float floatMoonResizeBuffer;

	private bool isFirstLastHazard;
	private int intMoonType_LastHazard_One;
	private int intMoonType_LastHazard_Two;

	[HideInInspector]
	public PlanetController planetController;
	[HideInInspector]
	public MoonController moonController;

	void Start () {
		StartCoroutine (BlockSpawn_Planet ());
	}

	IEnumerator BlockSpawn_Planet () {
		for (int i = 0; i < transform.GetSiblingIndex (); i++) {
			yield return null;
		}

		yield return null;
		GameObject objNewPlanet = Instantiate (GameController.Instance.gameSpawner.objPlanetSource);
		Transform transNewPlanet = objNewPlanet.transform;

//		yield return null;
		transNewPlanet.SetParent (transform);
		transNewPlanet.localScale = Vector3.one;
		transNewPlanet.localPosition = Vector3.zero;

		yield return null;
		planetController = objNewPlanet.GetComponent <PlanetController> ();

		planetController.floatPlanetCoreSize = planetCoreSize;

		planetController.floatOrbitSize = planetOrbitSize;
		planetController.floatSpeed_Orbit = planetSpeed_Orbit;
		planetController.floatSpeed_Planet = planetSpeed_Core;

		// Assign normal OR preset moon
		if (!areMoonsPreset) {
			planetController.areMoonsPreset = false;
		} else {
			planetController.areMoonsPreset = true;
			planetController.moonTypesArr = moonTypesArr;
//			planetController.moonControllerArr = moonControllerArr;
		}

		planetController.planetType = planetType;
		planetController.isDesignerPlanet = true;

		planetController.floatDesigner_SpecialPlanetAnimSpeed = planetSpecialSpeed;
		planetController.intDesigner_SpecialPlanetAnimNumber = planetWhichAnim;

		if (!isOrbitSpeedPreset) {
			planetController.Planet_RandomSpeeds ();
		}

		planetController.Planet_PreSetup_Block ();
	}

}
