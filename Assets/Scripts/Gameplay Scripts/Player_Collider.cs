﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Collider : MonoBehaviour {

	public enum PlayerColliderType {
		Orbiter,
		Body,
		Other
	}

	public PlayerController playerController;

	public PlayerColliderType playerColliderType;

	public BoxCollider2D boxCollider2D;

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Planet") {
			switch (playerColliderType) {
			case PlayerColliderType.Orbiter:
				if (PlayerController.playerStatus == PlayerController.PlayerStatus.IsFlying) {
					playerController.OnCollider_Orbiter (other);
				}
				break;
			case PlayerColliderType.Body:
				break;
			case PlayerColliderType.Other:
				break;
			default:
				Debug.LogError ("Default Error");
				break;
			}
		} else {
			if (other.tag == "Stars") {
//				Debug.LogError ("Stars Touch!?");
				other.transform.localPosition = new Vector2 (other.transform.localPosition.x, other.transform.localPosition.y + 22);
			}
		}
	}
}
