﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour {

	public static System.Action OnPlayerShrinkingDeath;

	[Header ("Planet Elements")]
	public PlanetType planetType;

	public float floatPlanetCoreSize;

	public SpriteRenderer spritePlanetHolder;
	public SpriteRenderer spriteSliceHolder1;
	public SpriteRenderer spriteSliceHolder2;

	public bool isDesignerPlanet;
	public float floatDesigner_SpecialPlanetAnimSpeed;
	public int intDesigner_SpecialPlanetAnimNumber;

	public Transform transPlanetSpecialParts;

	[Header ("Orbit Elements")]
	public float floatOrbitSize;
	public float floatSpeed_Orbit;
	public float floatSpeed_Planet;

	public SpriteRenderer spriteOrbitHolder;
	public SpriteRenderer spriteHaloHolder;

	[Header ("Moon Elements")]
	public bool areMoonsPreset;
	public MoonType[] moonTypesArr;
	public MoonController[] moonControllerArr;

	public int intMoonCount_Hazard;
	public int intMoonCount_Total;
	public float floatMoonResizeBuffer;

	public bool isFirstLastHazard;
	public int intMoonType_LastHazard_One;
	public int intMoonType_LastHazard_Two;

	[Header ("Sliced Elements")]
	public float floatSlicedParts_Force;
	public Rigidbody2D rigid2D_SlicedHolder1;
	public Rigidbody2D rigid2D_SlicedHolder2;

//	public Planet_ColliderScript planetColliderHolder;

	[Header ("Animations")]
	public AnimationClip[] animClipPlanetArr;
	public Animation animPlanetHolder;

	public AnimationClip[] animClipOrbitArr_BigSmall;
	public AnimationClip[] animClipOrbitArr_IrregularRotate;
	public AnimationClip[] animClipOrbitArr_ShrinkingBomb;
	public Animation animOrbitMoverHolder;

	public AnimationClip[] animCliBodyArr_ShrinkingBomb;
	public Animation animBodyHolder;

	[Header ("")]
	public Animation animPlanetHolder_Core;
	public Animation animPlanetHolder_Orbit;
	public Animation animPlanetHolder_Sliced;

	public AnimationState animStateOrbitBuffer;
	public AnimationState animStateCoreBuffer;

	public Transform transPlanetCoreHolder;
	public Transform transOrbitHolder;
	public Transform transMoonSourceParentHolder;
	public Transform transMoonHolder;
	public Transform transAfterDeathHolder_Core;
	public Transform transAfterDeathHolder_Orbit;
	public Transform transMoonsParentHolder;
	public Transform transSlicedHolder;

	public GameObject objPlanetMain_Holder;
	public GameObject objPlanetOrbit_Holder;
	public GameObject objPlanetSliced_Holder;

	public GameObject objMoonSource;

	public Transform transParticlePlanetSpecial;
	public ParticleSystem particlePlanetSpecial_Buffer;

	[Header ("")]
	public ParticleSystem particlePlanetEnter_GlowCore;
	public ParticleSystem particlePlanetEnter_GlowOrbit;

	[Header ("")]
//	public ParticleSystem particlePlanetDestroy_Orbit;
	public ParticleSystem particlePlanetDestroy_Glow1;
	public ParticleSystem particlePlanetDestroy_Glow2;
	public ParticleSystem particlePlanetDestroy_Glow3;

	public ParticleSystem particlePlanetDestroy_CutPoof1;
	public ParticleSystem particlePlanetDestroy_CutPoof2;

	public ParticleSystem particlePlanetDestroy_Slash;

	public int intMoonAngleSingle;
	public int intMoonAngleTotal;

	public int intPlanetNumber;

	[HideInInspector]
	public float floatOrbitSpecialPlanetSpeed;

	[HideInInspector]
	public Color colorBuffer_Halo;
	[HideInInspector]
	public Color colorBuffer_PoofCut;

	public bool wasDestroyed;

	private Vector3 v3_OribtAngleBuffer;
	private MoonController moonControllerBuffer;

	private float floatAnimSpeedPauseBuffer_Orbit;
	private float floatAnimSpeedPauseBuffer_Core;

	private bool canBeHazardMoon;
//	private bool isOrangeNormal;

	private bool hasBecomeCheckpointPlanet;

	private int intMoonChance_Hazard;
	private int intMoonChance_Score1;
	private int intMoonChance_Score2;
	private int intMoonChance_Score3;
	private int intMoonChance_Total;

	private int intRandomChanceMoon;

	private MoonType moonTypeBuffer;

	private Transform transCheckpointParticles;

	private GameObject objSpecialParticleRef;
	private GameObject objSpecialAssetRef;

	/*
	void OnBecameInvisible() {
		Debug.Log (this.name + " is invisible at " + transform.position);

		spritePlanetHolder.enabled = false;
		spriteSliceHolder1.enabled = false;
		spriteSliceHolder2.enabled = false;

		spriteOrbitHolder.enabled = false;
		spriteHaloHolder.enabled = false;
	}
	void OnBecameVisible() {
		Debug.Log (this.name + " is visible at " + transform.position);

		spritePlanetHolder.enabled = true;
		spriteSliceHolder1.enabled = true;
		spriteSliceHolder2.enabled = true;

		spriteOrbitHolder.enabled = true;
		spriteHaloHolder.enabled = true;
	}
	*/

	public void Start () {
		transMoonSourceParentHolder.gameObject.SetActive (false);
	}

	public void Planet_PreSetup_Random (int whatNumber) {
		intPlanetNumber = whatNumber;

		// First Hazard Allowed
		if (intPlanetNumber > GameController.Instance.intFirstPlanetCanHazard) {
			canBeHazardMoon = true;
		} else {
			canBeHazardMoon = false;
		}

		// Planet core size (Now use orbit size in a later position)
//		floatPlanetCoreSize = Random.Range (GameController.Instance.floatPlanetSize_Min, GameController.Instance.floatPlanetSize_Max);

		// Planet orbit size
		if (!GameController.Instance.isIntroSectionSizeDone) {
			floatOrbitSize = Random.Range (GameController.Instance.floatOrbitSize_IntroMin, GameController.Instance.floatOrbitSize_IntroMax);
		} 
		else {
			floatOrbitSize = Random.Range (GameController.Instance.floatOrbitSize_Min, GameController.Instance.floatOrbitSize_Max);
		}

		if (planetType == PlanetType.BigSmall || planetType == PlanetType.ShrinkingBomb) {
			floatPlanetCoreSize = floatOrbitSize - Random.Range (1, 1.5F);

			// This was used to for shrinking planet size check
//			while (Mathf.Abs (floatPlanetCoreSize - floatOrbitSize) < 1.2F) {
//				floatOrbitSize += 0.06F;
//			}
		} else {

			if (GameController.Instance.isIntroSectionSizeDone) {
				floatPlanetCoreSize = floatOrbitSize - Random.Range (0.9F, 1.5F);

				if (floatOrbitSize < 0.5F) {
					Debug.LogError ("WTF!");
				}
			} else {
				floatPlanetCoreSize = floatOrbitSize - Random.Range (1, 1.86F);
			}
		}

		if (floatPlanetCoreSize < 0.35F) {
			if (planetType != PlanetType.BigSmall) {
				floatPlanetCoreSize = Random.Range (0.4F, 0.6F);
			} else {
				floatPlanetCoreSize = Random.Range (0.6F, 0.75F);
			}

			Debug.LogError ("WTF Planet Size!");

			// Size correction (IF need needed!")
			if (GameController.Instance.isIntroSectionSizeDone) {
				while ((floatOrbitSize - floatPlanetCoreSize) < 0.5F) {
					floatPlanetCoreSize -= 0.05F;
					floatOrbitSize += 0.05F;
				}

//				while ((floatOrbitSize - floatPlanetCoreSize) > 1.75F) {
//					floatPlanetCoreSize += 0.05F;
//					floatOrbitSize -= 0.05F;
//				}
			}
		}

		// To keep all moons at the same size
		floatMoonResizeBuffer = 2 / floatOrbitSize;

		// Now random speeds happen here
		Planet_RandomSpeeds ();

		// For Random Rotate
//		if (Random.Range (0, 2) == 0) {
//			floatSpeed_Orbit = 0.22F * floatSpeed_Orbit;
//			floatSpeed_Planet = 0.1F * floatSpeed_Planet;
//		} else {
//			floatSpeed_Orbit = -0.22F * floatSpeed_Orbit;
//			floatSpeed_Planet = 0.1F * floatSpeed_Planet;
//		}

		if (!areMoonsPreset) {
			moonTypesArr = new MoonType[Random.Range (GameController.Instance.intMoonCountMin, GameController.Instance.intMoonCountMax + 1)];

//			if ((moonTypesArr.Length > 0) && (GameController.Instance.intMoonCountMin > 0)) {
			if ((moonTypesArr.Length > 0)) {
				if (GameController.Instance.intMoonZeroChancePercent > Random.Range (0, 100)) {
					moonTypesArr = new MoonType[0];
				}
			}

			// To Decrease moons with ZERO moons
//			if (moonTypesArr.Length == 0) {
//				moonTypesArr = new MoonType[Random.Range (GameController.Instance.intMoonCountMin, GameController.Instance.intMoonCountMax)];
//			}

			intMoonChance_Hazard = GameController.Instance.intMoonChance_Hazard;
			intMoonChance_Score1 = GameController.Instance.intMoonChance_Currency + intMoonChance_Hazard;
			intMoonChance_Score2 = GameController.Instance.intMoonChance_SuperScore + intMoonChance_Score1;
			intMoonChance_Score3 = GameController.Instance.intMoonChance_Autoplay + intMoonChance_Score2;
			intMoonChance_Total = intMoonChance_Score3;
		}

		Planet_SetupCall ();
	}

	public void Planet_RandomSpeeds () {
		if (GameController.Instance.floatOrbitSpeed_AbsoluteMax < 3) {
			floatSpeed_Orbit = Random.Range (GameController.Instance.floatOrbitSpeed_Min, GameController.Instance.floatOrbitSpeed_Max);
			floatSpeed_Planet = Random.Range (GameController.Instance.floatPlanetSpeed_Min, GameController.Instance.floatPlanetSpeed_Max);
		} else {
			floatSpeed_Orbit = Random.Range (GameController.Instance.floatOrbitSpeed_AfterAbsolute_Min, GameController.Instance.floatOrbitSpeed_AfterAbsolute_Max);
			floatSpeed_Planet = Random.Range (GameController.Instance.floatPlanetSpeed_AfterAbsolute_Min, GameController.Instance.floatPlanetSpeed_AfterAbsolute_Max);
		}

		// Max rotate speed count increase
		if (floatSpeed_Orbit > GameController.Instance.floatOrbitSpeed_AbsoluteMax) {
			GameController.Instance.intTimesMaxOrbitSpeedReached++;
		}
	}

	public void Planet_PreSetupGetMoon () {
		GameObject objNewMoonSource = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		objNewMoonSource.transform.SetParent (transMoonSourceParentHolder);
		objNewMoonSource.transform.localScale = Vector3.one;
		objNewMoonSource.transform.localPosition = new Vector3 (0, 2.97F, 0);

		objMoonSource = objNewMoonSource;
	}

	public void Planet_PreSetup_Block () {
		// To keep all moons at the same size
		floatMoonResizeBuffer = 2 / (floatOrbitSize);

		Planet_SetupCall ();
	}

	void Planet_SetupCall () {
		if (moonTypesArr.Length > 0) {
			// Get source of the moon
//			Planet_PreSetupGetMoon ();

			Spawn_MoonsAll ();
		}

		StartCoroutine (Planet_SetupRoutine ());
	}

	IEnumerator Planet_SetupRoutine () {
		yield return null;
		transPlanetCoreHolder.localScale = new Vector3 (floatPlanetCoreSize, floatPlanetCoreSize, floatPlanetCoreSize); 
		transSlicedHolder.localScale = transPlanetCoreHolder.localScale;

		// Size of first planet (Now use intro method)
//		if (intPlanetNumber == 0 && !GameController.Instance.isDesignerMode) {
//			floatOrbitSize = Random.Range (2.86F, 3F);
//		}

		transOrbitHolder.localScale = new Vector3 (floatOrbitSize, floatOrbitSize, floatOrbitSize);

//		if (floatOrbitSize > 1.9) {
//			spriteOrbitHolder.sprite = GameController.Instance.spriteController.spriteArr_DottedLine [1];
//		} else {
//			spriteOrbitHolder.sprite = GameController.Instance.spriteController.spriteArr_DottedLine [2];
//		}

		transAfterDeathHolder_Core.localScale = transPlanetCoreHolder.localScale;
		transAfterDeathHolder_Orbit.localScale = transOrbitHolder.localScale;

		yield return null;
		animStateOrbitBuffer = animPlanetHolder_Orbit [animPlanetHolder_Orbit.clip.name];
		animStateCoreBuffer = animPlanetHolder_Core [animPlanetHolder_Core.clip.name];

		animStateOrbitBuffer.speed = floatSpeed_Orbit;
		animStateCoreBuffer.speed = floatSpeed_Planet;

		// Speed of first planet
//		if (intPlanetNumber == 0) {
//			if (animStateOrbitBuffer.speed > 0) {
//				animStateOrbitBuffer.speed = Random.Range (0.06F, 0.08F);
//			} else {
//				animStateOrbitBuffer.speed = Random.Range (-0.08F, -0.06F);
//			}
//		}

		animStateOrbitBuffer.normalizedTime = Random.Range (0, 1F);
		animStateCoreBuffer.normalizedTime = Random.Range (0, 1F);

		animPlanetHolder_Core.Play ();
		animPlanetHolder_Orbit.Play ();

		yield return null;
		Planet_TypeSet ();

//		Debug.Break ();
	}

	public void Planet_OrbitReverse () {
		floatSpeed_Orbit *= -1;
	}

	public void Planet_TypeSet () {
		switch (planetType) {
		case PlanetType.Normal:

			if (Random.Range (0, 9) > 5) {
//				isOrangeNormal = false;
			} else {
//				isOrangeNormal = true;
				spritePlanetHolder.color = new Color (1, Random.Range (0.94F, 1), Random.Range (0.85F, 0.92F));
			}

			// To make sure small holed ones aren't used for small normal planets
			if (floatPlanetCoreSize < 1.25F) {
				spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_Normal [0];
			} else {
				if (floatPlanetCoreSize > 1.52F) {
					if (Random.Range (0, 10) > 6) {
						spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_Normal [0];
					} else {
						spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_Normal [1];
					}
				} else {
					if (Random.Range (0, 10) > 2) {
						spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_Normal [0];
					} else {
						spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_Normal [1];
					}
				}
			}
			spriteHaloHolder.color = GameController.Instance.spriteController.colorArr_PlanHalos_Normal [0];
			spriteSliceHolder1.sprite = GameController.Instance.spriteController.spriteArr_PlanSlice_Normal [0];
			spriteSliceHolder2.sprite = spriteSliceHolder1.sprite;

			colorBuffer_PoofCut = GameController.Instance.spriteController.colorArr_Particles_Normal[Random.Range (0,2)];

			break;
		case PlanetType.BigSmall:
			Planet_OrbitTypeAnimate (animClipOrbitArr_BigSmall, GameController.Instance.floatPlanetSpecSpeed_BigSmall_Min, GameController.Instance.floatPlanetSpecSpeed_BigSmall_Max, false);
//			Planet_OrbitTypeAnimate (animClipOrbitArr_BigSmall, 0.25F, 0.5F, false);
			//
			spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_BigSmall [0];
			spriteHaloHolder.color = GameController.Instance.spriteController.colorArr_PlanHalos_BigSmall [0];
			//
			spriteSliceHolder1.sprite = GameController.Instance.spriteController.spriteArr_PlanSlice_BigSmall [0];
			spriteSliceHolder2.sprite = spriteSliceHolder1.sprite;

			spriteOrbitHolder.color = Color.yellow;
//			spritePlanetHolder.color = Color.cyan;
			colorBuffer_PoofCut = GameController.Instance.spriteController.color_Particles_BigSmall;

			// Planet Special Particle
			GameObject objNewParticleSpec1 = Instantiate (GameController.Instance.otherMatController.particlePlanetSpecial_BigSmall.gameObject);
			objNewParticleSpec1.transform.SetParent (transParticlePlanetSpecial);
			objNewParticleSpec1.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_BigSmall.transform.localScale;
			objNewParticleSpec1.transform.localPosition = Vector3.zero;
			objNewParticleSpec1.transform.SetParent (transPlanetSpecialParts.parent);

			objSpecialParticleRef = objNewParticleSpec1;

//			UnityEditorInternal.ComponentUtility.CopyComponent (GameController.Instance.otherMatController.particlePlanetSpecial_BigSmall);
//			UnityEditorInternal.ComponentUtility.PasteComponentValues (particlePlanetSpecial_Holder);
//			particlePlanetSpecial_Holder.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_BigSmall.transform.localScale;
//			particlePlanetSpecial_Holder.Play ();

			break;
		case PlanetType.IrregularRotate:
			Planet_OrbitTypeAnimate (animClipOrbitArr_IrregularRotate, GameController.Instance.floatPlanetSpecSpeed_Irregular_Min, GameController.Instance.floatPlanetSpecSpeed_Irregular_Max, false);
//			Planet_OrbitTypeAnimate (animClipOrbitArr_IrregularRotate, 0.12F, 0.25F, false);
			spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_IrregularRotate [0];
			spriteHaloHolder.color = GameController.Instance.spriteController.colorArr_PlanHalos_IrregularRotate [0];
			//
			spriteSliceHolder1.sprite = GameController.Instance.spriteController.spriteArr_PlanSlice_IrregularRotate [0];
			spriteSliceHolder2.sprite = spriteSliceHolder1.sprite;

			spriteOrbitHolder.color = new Color (0.28F, 0.88F, 0.88F, 1);
//			spritePlanetHolder.color = Color.magenta;
			colorBuffer_PoofCut = GameController.Instance.spriteController.color_Particles_IrregularRotate;

			// Planet Special Particle
			GameObject objNewParticleSpec2 = Instantiate (GameController.Instance.otherMatController.particlePlanetSpecial_Irregular.gameObject);
			objNewParticleSpec2.transform.SetParent (transParticlePlanetSpecial);
			objNewParticleSpec2.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_Irregular.transform.localScale;
			objNewParticleSpec2.transform.localPosition = Vector3.zero;
			objNewParticleSpec2.transform.SetParent (transPlanetSpecialParts.parent);

			objSpecialParticleRef = objNewParticleSpec2;

//			UnityEditorInternal.ComponentUtility.CopyComponent (GameController.Instance.otherMatController.particlePlanetSpecial_Irregular);
//			UnityEditorInternal.ComponentUtility.PasteComponentValues (particlePlanetSpecial_Holder);
//			particlePlanetSpecial_Holder.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_Irregular.transform.localScale;
//			particlePlanetSpecial_Holder.Play ();

			GameObject objNewPlanetSoecRing = Instantiate (GameController.Instance.otherMatController.objPlanetSpecialSource_IrregularRing);
			objNewPlanetSoecRing.transform.SetParent (transPlanetSpecialParts);
			objNewPlanetSoecRing.transform.localScale = Vector3.one;
			objNewPlanetSoecRing.transform.localPosition = Vector3.zero;
			transPlanetSpecialParts.localScale = new Vector3 (floatPlanetCoreSize, floatPlanetCoreSize, floatPlanetCoreSize);
			if (Random.Range (0, 1) == 0) {
				transPlanetSpecialParts.localEulerAngles = new Vector3 (0, 0, Random.Range (3, 20));
			} else {
				transPlanetSpecialParts.localEulerAngles = new Vector3 (0, 0, Random.Range (-3, -20));
			}

			transPlanetSpecialParts.GetComponentInChildren <Animation> ().Play ();

			objSpecialAssetRef = objNewPlanetSoecRing;

			break;
		case PlanetType.ShrinkingBomb:
			Planet_OrbitTypeAnimate (animClipOrbitArr_ShrinkingBomb, GameController.Instance.floatPlanetSpecSpeed_Shrinking_Min, GameController.Instance.floatPlanetSpecSpeed_Shrinking_Max, true);
//			Planet_OrbitTypeAnimate (animClipOrbitArr_ShrinkingBomb, 0.3F, 0.5F, true);

			//
			spritePlanetHolder.sprite = GameController.Instance.spriteController.spriteArr_PlanCores_ShrinkingBomb [0];
			spriteHaloHolder.color = GameController.Instance.spriteController.colorArr_PlanHalos_ShrinkingBomb [0];
			//
			spriteSliceHolder1.sprite = GameController.Instance.spriteController.spriteArr_PlanSlice_ShrinkingBomb [0];
			spriteSliceHolder2.sprite = spriteSliceHolder1.sprite;

			spriteOrbitHolder.color = new Color (1, 0.3F, 0);
//			spritePlanetHolder.color = Color.red;
			colorBuffer_PoofCut = GameController.Instance.spriteController.color_Particles_ShrinkingBomb;

			// Planet Special Particle
			GameObject objNewParticleSpec3 = Instantiate (GameController.Instance.otherMatController.particlePlanetSpecial_Shrinking.gameObject);
			objNewParticleSpec3.transform.SetParent (transParticlePlanetSpecial);
			objNewParticleSpec3.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_Shrinking.transform.localScale;
			objNewParticleSpec3.transform.localPosition = Vector3.zero;
//			objNewParticleSpec3.transform.SetParent (transPlanetSpecialParts);

			objSpecialParticleRef = objNewParticleSpec3;

//			UnityEditorInternal.ComponentUtility.CopyComponent (GameController.Instance.otherMatController.particlePlanetSpecial_Shrinking);
//			UnityEditorInternal.ComponentUtility.PasteComponentValues (particlePlanetSpecial_Holder);
//			particlePlanetSpecial_Holder.transform.localScale = GameController.Instance.otherMatController.particlePlanetSpecial_Shrinking.transform.localScale;
//			particlePlanetSpecial_Holder.Play ();

			break;
		case PlanetType.Other:
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}
	}

	public void Planet_OrbitTypeAnimate (AnimationClip[] animClipArr, float minSpeed, float maxSpeed, bool isDelayed) {
		if (!isDesignerPlanet) {
			animOrbitMoverHolder.clip = animClipArr [Random.Range (0, animClipArr.Length)];
			floatOrbitSpecialPlanetSpeed = Random.Range (minSpeed, maxSpeed);
		} else {
//			animOrbitMoverHolder.clip = animClipArr [intDesigner_SpecialPlanetAnimNumber];
			floatOrbitSpecialPlanetSpeed = floatDesigner_SpecialPlanetAnimSpeed;
			animOrbitMoverHolder.clip = animClipArr [Random.Range (0, animClipArr.Length)];

			Debug.LogWarning ("Here!? " + this.name + " floatOrbitSpecialPlanetSpeed = " 
				+ floatOrbitSpecialPlanetSpeed + "\n while animOrbitMoverHolder.speed = " + animOrbitMoverHolder[animOrbitMoverHolder.clip.name].speed);

			if (floatOrbitSpecialPlanetSpeed == 0) {
				floatOrbitSpecialPlanetSpeed = 0.3F;
			}
		}
			
		if (!isDelayed) {
			Planet_SpecialAnimate ();
		}
	}

	void Planet_SpecialAnimate () {
		animOrbitMoverHolder [animOrbitMoverHolder.clip.name].speed = Mathf.Abs (floatOrbitSpecialPlanetSpeed);
		animOrbitMoverHolder.Play ();

		Debug.LogWarning ("END Here!? " + this.name + " floatOrbitSpecialPlanetSpeed = " 
			+ floatOrbitSpecialPlanetSpeed + "\n while animOrbitMoverHolder.speed = " + animOrbitMoverHolder[animOrbitMoverHolder.clip.name].speed);
	}

	public void Planet_PlayerEntered () {
		particlePlanetEnter_GlowCore.Play ();
		particlePlanetEnter_GlowOrbit.Play ();

		animPlanetHolder.clip = animClipPlanetArr [0];
		animPlanetHolder.Play ();

		// Change color of dotted after entry
//		spriteOrbitHolder.color = new Color (spriteOrbitHolder.color.r + 0.5f, spriteOrbitHolder.color.g + 0.5f, spriteOrbitHolder.color.b + 0.5f, 0.8F);
//		spriteOrbitHolder.enabled = false;

		// Moved shrinking start animate to the INSIDE of rotate mini pause

		// Things that are ONLY for non-checkpoint events
		if (GameController.Instance.isCheckpointTimeActive) {
			// To become CHECKPOINT
			Planet_BeCheckpoint ();
		}

		StartCoroutine (Planet_MoonsRemove ());
		// Pause rotation
		StartCoroutine (Planet_RotateMiniPause ());
		// Rotate Back
//		StartCoroutine (Planet_RotateReAdjustRoutine ());

		// SOUND EFFECTS: Player Enter Orbit (done)
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.EnterToCheese.Play ();
		}
	}

	public void Planet_BeCheckpoint () {
		hasBecomeCheckpointPlanet = true;
		GameController.Instance.playerController.Player_AnimSizeReset ();

		if (planetType != PlanetType.Normal) {
			objSpecialParticleRef.SetActive (false);
		}

		animOrbitMoverHolder.Stop ();

		floatAnimSpeedPauseBuffer_Orbit = animStateOrbitBuffer.speed;
		floatAnimSpeedPauseBuffer_Core = animStateCoreBuffer.speed;
		animStateOrbitBuffer.speed = 0;
      	animStateCoreBuffer.speed = 0;

		// Old stop method
//		animPlanetHolder_Core.Stop ();
//		animPlanetHolder_Orbit.Stop ();

		Planet_CamToCheckpointStarted ();

		// Now called from camera (moment of stopping)
		// Planet_AfterCheckpointEnd ();
	}

	public void Planet_CamToCheckpointStarted () {
		transCheckpointParticles = GameController.Instance.otherMatController.TransCheckpointParticle.GetChild (0);
		transCheckpointParticles.SetParent (transOrbitHolder.transform);
		transCheckpointParticles.localScale = Vector3.one;
		transCheckpointParticles.localPosition = Vector3.zero;
	}

	public void Planet_CamAtCheckpoint () {
		StartCoroutine (Planet_CamtAtCheckpoint_Routine ());
	}

	IEnumerator Planet_CamtAtCheckpoint_Routine () {
		// Start zoom in via camera
		GameController.Instance.camController.Camera_CheckpointZoomIn ();

		yield return null;
		GameController.Instance.particleCheckScriptHolder.OrbitStart_OtherParticles ();

//		yield return new WaitForSeconds (0.5F);
//		StartCoroutine (Planet_EndCheckpoint_Routine ());
	}

	public void Planet_EndCheckpoint () {
		StartCoroutine (Planet_EndCheckpoint_Routine ());
	}

	IEnumerator Planet_EndCheckpoint_Routine () {
//		yield return new WaitForSeconds (1);

		// Start zoom out via camera
		GameController.Instance.camController.Camera_CheckpointZoomOut ();

		yield return null;

		if (planetType == PlanetType.IrregularRotate) {
			objSpecialAssetRef.SetActive (false);
		}

		planetType = PlanetType.Normal;
		Planet_TypeSet ();

		yield return new WaitForSeconds (1.5F);

		//		animOrbitMoverHolder.Play ();

		animStateOrbitBuffer.speed = floatAnimSpeedPauseBuffer_Orbit;
		animStateCoreBuffer.speed = floatAnimSpeedPauseBuffer_Core;

		// Old play after checkpoint pause method
//		animPlanetHolder_Core.Play ();
//		animPlanetHolder_Orbit.Play ();

		GameController.Instance.AfterCheckpoint_ReturnNormal ();

		// Return Checkpoint Particles
		GameController.Instance.otherMatController.TransCheckpointParticle.gameObject.SetActive (true);
		GameController.Instance.otherMatController.TransCheckpointParticle.SetParent (GameController.Instance.otherMatController.TransCheckpointParticle.transform.parent);
		transCheckpointParticles.SetParent (GameController.Instance.otherMatController.TransCheckpointParticle);

		yield return new WaitForSeconds (1.5F);

		transCheckpointParticles.localScale = Vector3.one;
		transCheckpointParticles.localPosition = Vector3.zero;
	}

	public IEnumerator Planet_MoonsRemove () {
		yield return null;
		if (moonTypesArr.Length > 0) {
			for (int i = 0; i < moonControllerArr.Length; i++) {
				if (moonControllerArr [i] != null) {
					moonControllerArr [i].MoonWastePoof ();
				}
				yield return null;
			}
            // SOUND EFFECTS - poof (done)
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Poof.Play();
            }
        }
	}

	public IEnumerator Planet_RotateMiniPause () {
//		if (animStateOrbitBuffer != null) {
//			Debug.Log ("NULL! " + animStateOrbitBuffer.name);
//		} else {
//			Debug.Log ("NOT NULL! " + animStateOrbitBuffer.name);
//		}

		float floatSpeedBuffer = animStateOrbitBuffer.normalizedSpeed;

		animStateOrbitBuffer.normalizedSpeed = 0;
		int counter = 0;
		while (counter < 6) {
			counter++;
			yield return null;
		}

		counter = 0;
		while (counter < 20) {
			counter++;
			animStateOrbitBuffer.normalizedSpeed = Mathf.MoveTowards (animStateOrbitBuffer.normalizedSpeed, floatSpeedBuffer, Time.deltaTime * 5);
			yield return null;
		}

		animStateOrbitBuffer.normalizedSpeed = floatSpeedBuffer;
		Shrinking_PlanetEnterStuff ();

//		animPlanetHolder_Orbit.Stop ();
//		yield return new WaitForSeconds (0.2F);
//		animPlanetHolder_Orbit.Play ();
	}

	public void Shrinking_PlanetEnterStuff () {
		if ((planetType == PlanetType.ShrinkingBomb) && !GameController.Instance.isCheckpointTimeActive && !hasBecomeCheckpointPlanet) {
			floatOrbitSpecialPlanetSpeed = floatSpeed_Orbit * 1.5F;
			Planet_SpecialAnimate ();

			// Start player shrinking
			GameController.Instance.playerController.Player_AnimSizeShrinkStart(floatOrbitSpecialPlanetSpeed);

			animBodyHolder.clip = animCliBodyArr_ShrinkingBomb [0];
			animBodyHolder [animBodyHolder.clip.name].speed = Mathf.Abs(floatOrbitSpecialPlanetSpeed);
			animBodyHolder.Play ();

			// Warning Particle
			GameObject objNewShrinkOrbitWarnPart = Instantiate (GameController.Instance.otherMatController.objPlanetSpecial_ShrinOrbitWarn);
			objNewShrinkOrbitWarnPart.transform.SetParent (particlePlanetEnter_GlowCore.transform.parent);
			objNewShrinkOrbitWarnPart.transform.localScale = particlePlanetEnter_GlowCore.transform.localScale;
			objNewShrinkOrbitWarnPart.transform.localPosition = Vector3.zero;

			// SOUND EFFECTS: Player Enter Shrinking Orbit START (done)
			if (SoundManager.Instance != null)
			{
				SoundManager.Instance.ShrinkingCheese.Play();
			}
		}
	}

	public IEnumerator Planet_RotateReAdjustRoutine () {
		int counter = 0;
		float floatTargetTime = animStateOrbitBuffer.normalizedTime;
		if (floatSpeed_Orbit > 0) {
			floatTargetTime -= 0.01F;
		} else {
			floatTargetTime += 0.01F;
		}
		yield return null;

		while (counter < 10) {
			counter++;
			animStateOrbitBuffer.normalizedTime = Mathf.MoveTowards (animStateOrbitBuffer.normalizedTime, floatTargetTime, Time.deltaTime);
			yield return null;
		}
	}

	public void Planet_Destroy (float degree) {
		StartCoroutine (Planet_DestroyRoutine (degree));
	}

	IEnumerator Planet_DestroyRoutine (float degree) {
		wasDestroyed = true;

		switch (planetType) {
		case PlanetType.BigSmall:
			objSpecialParticleRef.gameObject.SetActive (false);
			break;
		case PlanetType.IrregularRotate:
			
			ParticleSystem particleSpecialIrregRing = transPlanetSpecialParts.GetComponentInChildren <ParticleSystem> ();
			particleSpecialIrregRing.transform.SetParent (transform.parent);
			particleSpecialIrregRing.Play ();

			objSpecialAssetRef.SetActive (false);
			objSpecialParticleRef.gameObject.SetActive (false);
			break;
		case PlanetType.ShrinkingBomb:
//			particlePlanetDestroy_Orbit.transform.localScale /= 2;

			transSlicedHolder.localScale *= transPlanetCoreHolder.GetChild (0).localScale.x;
			break;
		default:
			break;
		}

		// Orbit Classic Particles
//		particlePlanetDestroy_Orbit.Play ();

		// Orbit Particle Glow Color
		ParticleSystem.MainModule main = particlePlanetDestroy_Glow1.main;
		Color colorHaloParticleBuffer = spriteHaloHolder.color;
		colorHaloParticleBuffer.a = 0.5F;
		main.startColor = colorHaloParticleBuffer;
		main = particlePlanetDestroy_Glow2.main;
		main.startColor = colorHaloParticleBuffer;
		main = particlePlanetDestroy_Glow3.main;
		main.startColor = colorHaloParticleBuffer;

		// Orbit Glow Particles
//		particlePlanetDestroy_Glow1.Play ();
//		particlePlanetDestroy_Glow2.Play ();
		particlePlanetDestroy_Glow3.Play ();

		// CutPoof Particles
		main = particlePlanetDestroy_CutPoof1.main;
		main.startColor = colorBuffer_PoofCut;
		main = particlePlanetDestroy_CutPoof2.main;
		main.startColor = colorBuffer_PoofCut;
		particlePlanetDestroy_CutPoof1.Play ();
		particlePlanetDestroy_CutPoof2.Play ();
		particlePlanetDestroy_CutPoof1.transform.eulerAngles = new Vector3 (0, 0, degree + 90); 
		particlePlanetDestroy_CutPoof2.transform.eulerAngles = new Vector3 (0, 0, degree + 90);

		// Slash particle
		particlePlanetDestroy_Slash.transform.SetParent (transform);
		particlePlanetDestroy_Slash.transform.localScale = new Vector3 (2.5F, 1.3F, 1.3F);
		particlePlanetDestroy_Slash.transform.eulerAngles = new Vector3 (0, 0, degree + 90); 
		particlePlanetDestroy_Slash.Play ();

		// Lava Values
//		particlePlanetDestroy_CutPoof1.transform.eulerAngles = new Vector3 (0, 0, degree + 30); 
//		particlePlanetDestroy_CutPoof2.transform.eulerAngles = new Vector3 (0, 0, degree + 45); 

		objPlanetOrbit_Holder.SetActive (false);

		yield return null;
		yield return null;
		yield return null;

//		yield return new WaitForSeconds (0.05F);

		objPlanetMain_Holder.SetActive (false);
		objPlanetSliced_Holder.SetActive (true);

		animPlanetHolder_Sliced.Play ();
		objPlanetSliced_Holder.transform.eulerAngles = new Vector3 (0, 0, degree);
		if (rigid2D_SlicedHolder1.transform.position.x < rigid2D_SlicedHolder2.transform.position.x) {
			floatSlicedParts_Force = -floatSlicedParts_Force;
		}
		rigid2D_SlicedHolder1.AddForce (new Vector2 (floatSlicedParts_Force, 0), ForceMode2D.Impulse);
		rigid2D_SlicedHolder2.AddForce (new Vector2 (-floatSlicedParts_Force, 0), ForceMode2D.Impulse);

		Destroy (this.gameObject, 2);
	}

	public void Planet_ShrinkingDeath () {
		if (GameController.Instance.canDie && (PlayerController.playerStatus != PlayerController.PlayerStatus.IsDead)) {
			if (OnPlayerShrinkingDeath != null) {
				OnPlayerShrinkingDeath ();
			}
			GameController.Instance.Player_DeathExplosion (transform.position);
		}

        // SOUND EFFECTS: Player Death By Shrinking Planet (done)
        if (SoundManager.Instance != null)
        {
			SoundManager.Instance.ShrinkingCheese.Stop();
          	SoundManager.Instance.MargBaSayare.Play();
        }

        Destroy (this.gameObject, 0.1F);
	}

	void Spawn_MoonsAll () {
		intMoonAngleSingle = 360 / moonTypesArr.Length;
		intMoonAngleTotal = 0;

		v3_OribtAngleBuffer = new Vector3 (0, 0, 0);

		for (int i = 0; i < moonTypesArr.Length; i++) {
			Spawn_Moon ();

			intMoonAngleTotal += intMoonAngleSingle;
			v3_OribtAngleBuffer.z = intMoonAngleTotal;
			transOrbitHolder.localEulerAngles = v3_OribtAngleBuffer;

			if (!areMoonsPreset) {
				intRandomChanceMoon = Random.Range (0, intMoonChance_Total);
				Moon_Type (i);
			}
		}

		moonControllerArr = new MoonController[moonTypesArr.Length];

		Moon_TypeImplement ();
	}

	public void Spawn_Moon () {
		GameObject objNewMoon = Instantiate (GameController.Instance.gameSpawner.objMoonSource);

		objNewMoon.transform.SetParent (transMoonsParentHolder);
		objNewMoon.transform.localScale = Vector3.one;
		objNewMoon.transform.position = transMoonSourceParentHolder.GetChild (0).transform.position;

		// Old methods
//		objNewMoon.transform.SetParent (transMoonSourceParentHolder);
//		objNewMoon.transform.localScale = Vector3.one;
//		objNewMoon.transform.localPosition = new Vector3 (0, 2.97F, 0);
//
//		objNewMoon.transform.SetParent (objMoonSource.transform.parent);
//		objNewMoon.transform.localPosition = objMoonSource.transform.localPosition;
//		objNewMoon.transform.SetParent (transMoonsParentHolder);

		objNewMoon.transform.localScale = Vector3.one;

//		moonControllerBuffer = objNewMoon.GetComponent<MoonController> ();
	}

	public void Moon_Type (int whatInt) {
		if (intRandomChanceMoon < intMoonChance_Hazard) {
			if (canBeHazardMoon) {
				moonTypeBuffer = MoonType.Hazard;
				intMoonCount_Hazard++;
				intMoonCount_Total++;

				if (!isFirstLastHazard) {
					intMoonType_LastHazard_One = whatInt;
				} else {
					intMoonType_LastHazard_Two = whatInt;
				}
				isFirstLastHazard = !isFirstLastHazard;

			} else {
				intRandomChanceMoon += intMoonChance_Hazard;
				Moon_Type (whatInt);

				Debug.LogError (name + " Moon RETRY");
				return;
			}
		} 
		else {
			if (intRandomChanceMoon < intMoonChance_Score1) {
				moonTypeBuffer = MoonType.Currency;
				intMoonCount_Total++;
			} 
			else {
				if (intRandomChanceMoon < intMoonChance_Score2) {
//					moonTypeBuffer = MoonType.SuperCurrency;
					moonTypeBuffer = MoonType.Currency;
					intMoonCount_Total++;
				} 
				else {
					if (intRandomChanceMoon < intMoonChance_Score3) {
//						moonTypeBuffer = MoonType.AutoPlay;
						moonTypeBuffer = MoonType.SuperCurrency;
						intMoonCount_Total++;
					} 
					else {
//						moonTypeBuffer = MoonType.Other;
						moonTypeBuffer = MoonType.SuperCurrency;
						intMoonCount_Total++;
					}
				}
			}
		}
			
		moonTypesArr [whatInt] = moonTypeBuffer;
//		moonControllerBuffer.Moon_Setup (moonTypeBuffer, floatMoonResizeBuffer, true);
	}

	void Moon_TypeImplement () {
		moonControllerArr = GetComponentsInChildren <MoonController>();

		// Hazard difficulty controller for randoms 
		if (!isDesignerPlanet) {
			if (moonTypesArr.Length == 3) {
				if (intMoonCount_Total == intMoonCount_Hazard) {
					moonTypesArr [intMoonType_LastHazard_One] = (MoonType)Random.Range (1, 3);
				}
			} else {
				if (moonTypesArr.Length > 3) {
					if (intMoonCount_Total <= (intMoonCount_Hazard + 1)) {
						moonTypesArr [intMoonType_LastHazard_One] = (MoonType)Random.Range (1, 3);
						moonTypesArr [intMoonType_LastHazard_Two] = (MoonType)Random.Range (1, 3);
					}
				}
			}
		}

		for (int i = 0; i < moonTypesArr.Length; i++) {
			moonControllerArr [i].isOrbitingPlanet = true;
			moonControllerArr [i].Moon_Setup (moonTypesArr [i], floatMoonResizeBuffer * 1.2F, true);
		}
	}

	public float Planet_SizeToSlashSpeed () {
		if (planetType != PlanetType.ShrinkingBomb) {
			return (floatOrbitSize / 2);
		} else {
			return (floatOrbitSize / 5);
		}
	}
		
}
