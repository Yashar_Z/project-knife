﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

	public AnimationClip[] animCamMainClipArr;
	public Animation animCamMainHolder;

	public Vector2 v2_CamMoveStart;
	public Vector2 v2_CamMoveTarget;

	public bool canMoveCamera;
	public float floatCamSpeed;

	public float floatCamHorizLimit;

	[Header ("Cam Vertical Offset")]
	public float floatCammVerticalOffset;

	[Header ("Cam Speeds")]
	public float floatCamSpeed_Slowest;
	public float floatCamSpeed_Fastest;

	[Header ("Cam Child Groups / Parents")]
	public bool isNoEdgeCamera;
	public bool isNoBorderCamera;

	public GameObject objCamEdgeCollidersParent;
	public GameObject objCamBlackBordersParent;

//	[Header ("For Cam Movement Time")]
//	public System.DateTime dateTimeSpentForCam;
//	public UnityEngine.UI.Text textTimeToCamCenter;

	[HideInInspector]
	public bool camMove_SlowFollow;

	[HideInInspector]
	public bool camMove_StartFastFollow;
	private float floatStartFastSpeed;

	private bool isCamGainMomentum;
	private bool isCamStopped;

	private bool canBreak;

	private bool isCamToCheckpoint;

	private float floatTimePassed_Main;
	private float floatTimePassed_Speed;

	private float floatCamPosit_Difference;
//	private float floatCamPosit_DifferencePREV;

	private float floatCamSpeedDecreaseTimeBuffer;

	private float floatCamFollowAngle;
	private float floatSlowFollowSpeed;
	private Vector3 v3_CamFollowAngle;

	void Start () {
		// Moved to first time entering orbit by the player
//		objCamEdgeCollidersParent.SetActive (!isNoEdgeCamera);

//		objCamBlackBordersParent.SetActive (!isNoBorderCamera);

//		if (GameController.Instance.isDesignerMode) {
//			onPlayerAttack ();
//		}
	}

	void OnEnable () {
		PlayerController.OnPlayerEnterOrbit += onPlayerEnterOrbit;
		PlayerController.OnPlayerAttack += onPlayerAttack;
	}

	void OnDisable () {
		PlayerController.OnPlayerEnterOrbit -= onPlayerEnterOrbit;
		PlayerController.OnPlayerAttack -= onPlayerAttack;
	}

	public void Start_MenuToGameCam () {
		StartCoroutine (MenuToGame_CamRoutine ());
	}

	IEnumerator MenuToGame_CamRoutine () {
		Camera_MenuToGame ();

		yield return new WaitForSeconds (0.9F);
		onPlayerAttack ();

		//		isCamStopped = true;
		//		camMove_SlowFollow = true;
		//		camMove_StartFastFollow = true;
		//
		//		floatStartFastSpeed = 1;
		//		StartCoroutine (MoveSpeed_StartFastFollow ());
		//		StartCoroutine (Camera_StartFastFollow ());
		//
	}

	public void Start_GameCam () {
		onPlayerAttack ();
	}

	public void Cam_Reposition (float y) {
		Vector3 v3_Buffer = transform.GetChild(0).localPosition;
		v3_Buffer.y = y;
		transform.GetChild(0).localPosition = v3_Buffer;
	}

	public void AfterPlayerRevive () {
////		Cam_ZoomIn ();
//		Cam_ZoomOut ();

		Camera_NewTarget (true);
	}

	public void onPlayerEnterOrbit () {
//		Cam_ZoomIn ();
////		Cam_ZoomOut ();

		Camera_NewTarget (false);
	}

	public void Camera_NewTarget (bool isResussrection) {
		v2_CamMoveStart = transform.position;
		v2_CamMoveTarget = GameController.Instance.v3_planetSelectedPosit;

		if (isResussrection) {
			v2_CamMoveTarget = GameController.Instance.v3_planetRevivePosit;
			v2_CamMoveTarget.y -= 5;
		}

		// Lower the y so the cam shows more of the above
		v2_CamMoveTarget.y += floatCammVerticalOffset;

		// Horiz limit for cam move
		if (v2_CamMoveTarget.x < -floatCamHorizLimit) {
			v2_CamMoveTarget.x = -floatCamHorizLimit;
		} else {
			if (v2_CamMoveTarget.x > floatCamHorizLimit) {
				v2_CamMoveTarget.x = floatCamHorizLimit;
			}
		}

		StartCoroutine (Camera_Move ());
	}

	public void onPlayerAttack () {
		isCamStopped = true;

//		Cam_ZoomOut ();
////		Cam_ZoomIn ();

		camMove_SlowFollow = true;

		StartCoroutine (Camera_SlowFollow ());
	}

	/*
//	public void ShowCamera_MoveTime () {
//		System.TimeSpan timeSpan = System.DateTime.Now - dateTimeSpentForCam;
//		float floatTimeSpan = timeSpan.Milliseconds;
//		Debug.LogError ("Time span = " + floatTimeSpan + "  and base time " + timeSpan);
//		textTimeToCamCenter.text = timeSpan.Seconds.ToString () + "." + floatTimeSpan.ToString ();
//	}

//	void Start () {
//		camMove_SlowFollow = true;
//		StartCoroutine (Camera_SlowFollow ());
//	}
	*/

	IEnumerator Camera_SlowFollow () {
		//		yield return null;

		floatCamFollowAngle = GameController.Instance.playerController.transform.eulerAngles.z;
		floatCamFollowAngle += 90;

		v3_CamFollowAngle = (PolarCoordinates (floatCamFollowAngle * Mathf.Deg2Rad, 1));

		while (camMove_SlowFollow && !isCamToCheckpoint) {
			transform.Translate (v3_CamFollowAngle * Time.deltaTime * 3.5F);
			yield return null;
		}
	}

	IEnumerator Camera_StartFastFollow () {
		yield return new WaitForSeconds (0.2F);;

		while (camMove_SlowFollow) {
			transform.Translate (Vector2.up * Time.deltaTime * floatStartFastSpeed);
			Debug.LogWarning ("Is FAST MOving?");
			yield return null;
		}
	}

	IEnumerator MoveSpeed_StartFastFollow () {
		//		yield return null;

		while (floatStartFastSpeed < 100) {
			floatStartFastSpeed = Mathf.MoveTowards (floatStartFastSpeed, 100, Time.deltaTime * 40);
			Debug.LogWarning ("Is Speed Increasing ? " + floatStartFastSpeed);
			yield return null;
		}
	}

	/*	Cam different angle and mover (Andgle updater and more) - Didn't use
	IEnumerator Camera_SlowFollow_Other () {
//		yield return null;

		Debug.LogError ("WTF!");

		floatCamFollowAngle = GameController.Instance.playerController.transform.eulerAngles.z;
		floatCamFollowAngle += 90;

		v3_CamFollowAngle = (PolarCoordinates (floatCamFollowAngle * Mathf.Deg2Rad, 1));

		while (camMove_SlowFollow) {
			Debug.LogError ("Cam Angle = " + v3_CamFollowAngle.z);
			// TODO:
			transform.Translate (v3_CamFollowAngle * Time.deltaTime * 3.5F);
//			transform.Translate (v3_CamFollowAngle * Time.deltaTime * 7.5F);

			yield return null;
		}
	}

	IEnumerator Camera_Follow_AngleUpdater (float camAngleGoal) {
		while (camMove_SlowFollow) {
			camAngleGoal = Mathf.Atan2 (GameController.Instance.planetController_Selected.transform.localPosition.y - transform.localPosition.y, 
				GameController.Instance.planetController_Selected.transform.localPosition.x - transform.localPosition.x) * Mathf.Rad2Deg;

			floatCamFollowAngle = Mathf.MoveTowards (floatCamFollowAngle,  camAngleGoal, Time.deltaTime * 7.5F);
			v3_CamFollowAngle = (PolarCoordinates (floatCamFollowAngle * Mathf.Deg2Rad, 1));

			yield return null;
		}
	}

	public IEnumerator Camera_Move () {
		floatCamSpeed = 0.2F;
		floatTimePassed_Main = 0;

		// TODO: Need to return for cam follow
		floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, v2_CamMoveTarget));
		floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, GameController.Instance.planetController_Selected.transform.localPosition));

		isCamStopped = false;
		isCamGainMomentum = true;

		// TODO: Need to return for cam follow
//		camMove_SlowFollow = false;
//		StartCoroutine (Camera_SpeedIncrease ());

		floatCamPosit_DifferencePREV = floatCamPosit_Difference;

		while (floatCamPosit_Difference > 0.1F) {

			// TODO
			var x = Mathf.Atan2 (GameController.Instance.planetController_Selected.transform.localPosition.y - transform.localPosition.y, 
				GameController.Instance.planetController_Selected.transform.localPosition.x - transform.localPosition.x) * Mathf.Rad2Deg;
			StartCoroutine (Camera_Follow_AngleUpdater (x));

			floatTimePassed_Main += Time.deltaTime;

			// TODO: Need to return for cam follow
			floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, v2_CamMoveTarget));

			floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, GameController.Instance.planetController_Selected.transform.localPosition));

			if ((floatCamPosit_Difference < 1.5F) && isCamGainMomentum) {
				//				
				isCamGainMomentum = false;

				// TODO: Need to return for cam follow
				// Determine speed increase timebuffer
				if (floatCamSpeed > 1.5F) {
//					Debug.LogError ("FAST    Slow Down!   +   floatcamspeed = " + floatCamSpeed);
					floatCamSpeedDecreaseTimeBuffer = 7;
				} else {
//					Debug.LogError ("SLOW    Slow Down!   +   floatcamspeed = " + floatCamSpeed);
					floatCamSpeedDecreaseTimeBuffer = 3.5F;
				}
				StartCoroutine (Camera_SpeedDecrease ());


				//				Debug.Break ();
				//				ShowCamera_MoveTime ();
			}

			// New Move Towards Method
			//			v2_CamMoveStart = Vector2.MoveTowards (v2_CamMoveStart, v2_CamMoveTarget, Time.deltaTime * 5 * floatCamSpeed);
			//			transform.position = v2_CamMoveStart;

			//OTHER
			Prev position check
			if (floatCamPosit_DifferencePREV < floatCamPosit_Difference) {
				Debug.LogError ("FUCK!");

				if (!canBreak) {
					Debug.Break ();
					canBreak = true;
				}
			}

			floatCamPosit_DifferencePREV = floatCamPosit_Difference;

			yield return null;
		}

		// TODO: Needs to be removed
		Debug.LogError ("WTF2");
		camMove_SlowFollow = false;

		//		Debug.LogWarning ("Stopped");

		isCamGainMomentum = false;
		isCamStopped = true;

		yield return null;
	}
	*/

	public Vector3 PolarCoordinates(float angle, float radius)
	{
		// Assuming movement is on XY plane
		Vector3 relativePosition = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), 0);

		return relativePosition;
	}

	public void Cam_ZoomIn () {
		animCamMainHolder.clip = animCamMainClipArr [0];
		animCamMainHolder.Play ();
	}

	public void Cam_ZoomOut () {
		animCamMainHolder.clip = animCamMainClipArr [1];
		animCamMainHolder.Play ();
	}

	public void Cam_GoFast_CheckPoint () {
		isCamToCheckpoint = true;
		v2_CamMoveTarget.y -= (floatCammVerticalOffset / 2);
	}
		
	public IEnumerator Camera_Move () {
		floatCamSpeed = 0.2F;
		floatTimePassed_Main = 0;

		floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, v2_CamMoveTarget));

		isCamStopped = false;
		isCamGainMomentum = true;
		camMove_SlowFollow = false;

		StartCoroutine (Camera_SpeedIncrease ());

//		floatCamPosit_DifferencePREV = floatCamPosit_Difference;

		while ((floatCamPosit_Difference > 0.1F)) {

			floatTimePassed_Main += Time.deltaTime;

			floatCamPosit_Difference = Mathf.Abs(Vector2.Distance (transform.position, v2_CamMoveTarget));

			if ((floatCamPosit_Difference < 1.3F) && isCamGainMomentum) {

				isCamGainMomentum = false;

				// Determine speed increase timebuffer
				if (floatCamSpeed > 1.7F) {
//					Debug.LogError ("FAST    Slow Down!   +   floatcamspeed = " + floatCamSpeed);
					floatCamSpeedDecreaseTimeBuffer = 9;
				} else {
					if (floatCamSpeed > 1.4F) { 
						floatCamSpeedDecreaseTimeBuffer = 6;
					} else {
//						Debug.LogError ("SLOW    Slow Down!   +   floatcamspeed = " + floatCamSpeed);
						floatCamSpeedDecreaseTimeBuffer = 3.5F;
					}
				}
				StartCoroutine (Camera_SpeedDecrease ());

				//				Debug.Break ();
				//				ShowCamera_MoveTime ();
			}

			// To make sure have normal speed for NON checkpoint planet reach
			if (!isCamToCheckpoint) {
				v2_CamMoveStart = Vector2.MoveTowards (v2_CamMoveStart, v2_CamMoveTarget, Time.deltaTime * 5 * floatCamSpeed);
			} 

			// To make sure have SUPER speed for checkpoint planet reach
			else {
				v2_CamMoveStart = Vector2.MoveTowards (v2_CamMoveStart, v2_CamMoveTarget, Time.deltaTime * 16);
			}

			transform.position = v2_CamMoveStart;

			/*
			Prev position check
			if (floatCamPosit_DifferencePREV < floatCamPosit_Difference) {
				Debug.LogError ("FUCK!");

				if (!canBreak) {
					Debug.Break ();
					canBreak = true;
				}
			}
			*/
//			floatCamPosit_DifferencePREV = floatCamPosit_Difference;

			yield return null;
		}
		//		Debug.LogWarning ("Stopped");

		isCamGainMomentum = false;
		isCamStopped = true;

		// The moment that cam checkpoint move has ended
		if (isCamToCheckpoint) {
			isCamToCheckpoint = false;

			// To allow change of planet
			GameController.Instance.playerController.planetControllerRef.Planet_CamAtCheckpoint ();
		}

		yield return null;
	}

	public IEnumerator Camera_SpeedIncrease () {

		floatTimePassed_Speed = 0;
		while ((floatTimePassed_Speed < 2) && isCamGainMomentum) {
//			Debug.Log ("SPEEDING");

			floatTimePassed_Speed += Time.deltaTime;
			floatCamSpeed = Mathf.MoveTowards (floatCamSpeed, floatCamSpeed_Fastest, Time.deltaTime * 3.2F);

			yield return null;

			if (isCamStopped) {
				yield break;
			}
		}

		yield return null;
	}

	public IEnumerator Camera_SpeedDecrease () {

		floatTimePassed_Speed = 0;
		while (floatTimePassed_Speed < 2) {
//			Debug.Log ("Slowing");

			floatTimePassed_Speed += Time.deltaTime;
			floatCamSpeed = Mathf.MoveTowards (floatCamSpeed, floatCamSpeed_Slowest, Time.deltaTime * floatCamSpeedDecreaseTimeBuffer);

			yield return null;

			if (isCamStopped) {
				yield break;
			}
		}

		yield return null;
	}

	public void Camera_CheckpointZoomIn () {
		animCamMainHolder.clip = animCamMainClipArr [2];
		animCamMainHolder.Play ();
	}

	public void Camera_CheckpointZoomOut () {
		animCamMainHolder.clip = animCamMainClipArr [3];
		animCamMainHolder.Play ();
	}

	public void Camera_DeathKnife () {
		animCamMainHolder.clip = animCamMainClipArr [4];
		animCamMainHolder.Play ();
	}

	public void Camera_MenuToGame () {
		animCamMainHolder.clip = animCamMainClipArr [5];
		animCamMainHolder.Play ();
	}

	// Sovled the knife anim problem for [4]
	public void Camera_BonusContin () {
		if (animCamMainHolder.clip == animCamMainClipArr [4]) {
			animCamMainHolder [animCamMainHolder.clip.name].time = 0;
		}

		animCamMainHolder.Stop ();
		animCamMainHolder.clip = animCamMainClipArr [6];
		animCamMainHolder.Play ();
	}
}
