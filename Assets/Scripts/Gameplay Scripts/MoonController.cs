﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonController : MonoBehaviour {

	public static System.Action OnPlayerHazardDeath;

	public CircleCollider2D circleColl2d_Holder;

	public MoonType moonType;

	public SpriteRenderer spriteMoonCoreHolder;

	public SpriteRenderer spriteMoonSliceHolder1;
	public SpriteRenderer spriteMoonSliceHolder2;

	[Header ("Halo / Orbit Stuff")]
	public SpriteRenderer spriteMoonHaloHolder;
	public Transform transMoonHaloParentHolder;

	[Header ("Animations & More")]
	public Animation animMoonHolder;
	public Animation animMoonHolder_Sliced;

	public AnimationClip[] animClipMoonHolder_IdleArr;
	public Animation animMoonHolder_IdleBase;
	public Animation animMoonHolder_IdleTingleRotate;

	public Color colorBuffer_MoonHalo;
	public Color colorBuffer_MoonCutPoof;

	public ParticleSystem particleMoonPickHolder;
	public ParticleSystem particleMoonFlareHolder;
	public ParticleSystem particleMoonCutPoofHolder;
	public ParticleSystem particleMoonWastePoof; 

	public ParticleSystem particleMoonSuperCurr_Sparks; 
	public ParticleSystem particleMoonSuperCurr_Glows; 

	public GameObject objMoonMain_Holder;
	public GameObject objMoonOrbit_Holder;
	public GameObject objMoonSliced_Holder;

	public GameObject objMoonHazard_SpecialParticle;
	public GameObject objMoonSuperCurr_SpecialParticle;

	public bool isOrbitingPlanet;
	public bool isDesignerMoon;

	[Header ("Sliced Elements")]
	public float floatSlicedParts_Force;
	public Rigidbody2D rigid2D_SlicedHolder1;
	public Rigidbody2D rigid2D_SlicedHolder2;

	private Sprite spriteBuffer_Left;
	private Sprite spriteBuffer_Right;

	// Random Currency Moons
	private ParticleSystem.TextureSheetAnimationModule textureMod;
	private int randomInt;
	private float floatMoonSizeBuffer;

	/*
	void OnBecameInvisible() {
		Debug.Log (this.name + " is invisible at " + transform.position);

		spriteMoonCoreHolder.enabled = false;

		spriteMoonSliceHolder1.enabled = false;
		spriteMoonSliceHolder2.enabled = false;

		spriteMoonHaloHolder.enabled = false;
	}
	void OnBecameVisible() {
		Debug.Log (this.name + " is visible at " + transform.position);

		spriteMoonCoreHolder.enabled = true;

		spriteMoonSliceHolder1.enabled = true;
		spriteMoonSliceHolder2.enabled = true;

		spriteMoonHaloHolder.enabled = true;
	}
	*/

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") {
			switch (moonType) {
			case MoonType.Hazard:
				if (GameController.Instance.canDie && (PlayerController.playerStatus == PlayerController.PlayerStatus.IsFlying)) {
                        // SOUND EFFECTS: Player Death By Pepper (done)

                        if (SoundManager.Instance != null)
                        {
                            SoundManager.Instance.MargBaFelfel.Play();
                        }
					// Changing player status to dead is now inside GameController's Player_Death
					if (OnPlayerHazardDeath != null) {
						OnPlayerHazardDeath ();
					}

					MoonDestroyHazard ();
					return;
				}

				// Player is dead
				else {
					MoonWastePoof ();
				}
				break;
			case MoonType.Currency:
				MoonCurrency (1);
				MoonDestroy ();
				break;
			case MoonType.SuperCurrency:
				MoonCurrency (5);
				MoonDestroy ();
				break;
			case MoonType.AutoPlay:
				MoonCurrency (3);
				break;
			case MoonType.Other:
				break;
			default:
				Debug.LogError ("Default Error");
				break;
			}
		} 

		else {
			if (other.tag == "Player Body") {
				switch (moonType) {
				case MoonType.Hazard:
					MoonWastePoof ();
					break;
				case MoonType.Currency:
					MoonCurrency (1);
					MoonDestroy ();
					break;
				case MoonType.SuperCurrency:
					MoonCurrency (5);
					MoonDestroy ();
					break;
				case MoonType.AutoPlay:
					MoonCurrency (3);
					MoonDestroy ();
					break;
					// No hazard for body
//				case MoonType.Hazard:
//					if (PlayerController.playerStatus == PlayerController.PlayerStatus.IsFlying) {
//						Debug.LogError ("Player Death");
//						GameController.Instance.onPlayerCamDeath ();
//						return;
//					}
//					break;
				case MoonType.Other:
					break;
				default:
					Debug.LogError ("Default Error");
					break;
				}
			}
		}
	}

	public void Moon_Setup (MoonType whatType, float size, bool isOnPlanet) {
		moonType = whatType;

		switch (moonType) {
		case MoonType.Hazard:
			spriteMoonCoreHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsCores [0];
			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Normal [0];

			spriteBuffer_Left = GameController.Instance.spriteController.spriteArr_MoonsSlices_Left [0];
			spriteBuffer_Right = GameController.Instance.spriteController.spriteArr_MoonsSlices_Right [0];

			circleColl2d_Holder.radius = 0.9F;
			transMoonHaloParentHolder.localScale = new Vector3 (1.1F, 1.1F, 1.1F);
			//
			colorBuffer_MoonCutPoof = GameController.Instance.spriteController.colorArr_Particles_MoonsCutPoofs [0];

			// Spcecial Hazard Particle
			objMoonHazard_SpecialParticle.SetActive (true);

			// Moon Idle Anim Clip
			animMoonHolder_IdleBase.clip = animClipMoonHolder_IdleArr [0];

			break;
		case MoonType.Currency:
			/* Non-Sliced
			int randomInt = Random.Range (1, 4);

			spriteMoonCoreHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsCores [randomInt];
			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Normal [randomInt];

			spriteBuffer_Left = GameController.Instance.spriteController.spriteArr_MoonsSlices_Left [randomInt];
			spriteBuffer_Right = GameController.Instance.spriteController.spriteArr_MoonsSlices_Right [randomInt];

			circleColl2d_Holder.radius = 1.2F;
			transMoonHaloParentHolder.localScale = new Vector3 (1.2F, 1.2F, 1.2F);

			//
			colorBuffer_MoonCutPoof = GameController.Instance.spriteController.colorArr_Particles_MoonsCutPoofs [1];
			*/

			// Sliced Particle for bread and biscuit
			randomInt = Random.Range (1, 4);

			spriteMoonCoreHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsCores [randomInt];
			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Normal [randomInt];

			spriteBuffer_Left = GameController.Instance.spriteController.spriteArr_MoonsSlices_Left [randomInt];
			spriteBuffer_Right = GameController.Instance.spriteController.spriteArr_MoonsSlices_Right [randomInt];

			switch (randomInt) {
			case 1:
				floatMoonSizeBuffer = 1.2F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for toast bread
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			case 2:
				floatMoonSizeBuffer = 1.2F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for biscuit
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 2;
				break;
			case 3:
				floatMoonSizeBuffer = 1.25F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for banana
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			default:
				floatMoonSizeBuffer = 1.2F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for toast default
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			}

			//
			colorBuffer_MoonCutPoof = GameController.Instance.spriteController.colorArr_Particles_MoonsCutPoofs [randomInt];

			// Moon Idle Anim Clip
			animMoonHolder_IdleBase.clip = animClipMoonHolder_IdleArr [randomInt];

			break;
		case MoonType.SuperCurrency:
			// Sliced Particle for bread and biscuit
			randomInt = Random.Range (1, 4);

			spriteMoonCoreHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsCores [randomInt];
			// For super currency, use different sprites
			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Super [randomInt];
//			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Normal [randomInt];

			spriteMoonHaloHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsHaloType [1];

			spriteBuffer_Left = GameController.Instance.spriteController.spriteArr_MoonsSlices_Left [randomInt];
			spriteBuffer_Right = GameController.Instance.spriteController.spriteArr_MoonsSlices_Right [randomInt];

			// Art size increase for supercurrency
			float floatArtSizeBuffer = spriteMoonCoreHolder.transform.localScale.x;
			floatArtSizeBuffer *= 1.12F;
			spriteMoonCoreHolder.transform.localScale = new Vector3 (floatArtSizeBuffer, floatArtSizeBuffer, floatArtSizeBuffer);

			switch (randomInt) {
			case 1:
				floatMoonSizeBuffer = 1.4F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for toast bread
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			case 2:
				floatMoonSizeBuffer = 1.35F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for biscuit
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 2;
				break;
			case 3:
				floatMoonSizeBuffer = 1.4F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for banana
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			default:
				floatMoonSizeBuffer = 1.5F;
				circleColl2d_Holder.radius = floatMoonSizeBuffer;
				transMoonHaloParentHolder.localScale = new Vector3 (floatMoonSizeBuffer, floatMoonSizeBuffer, floatMoonSizeBuffer);

				// Particle shape for toast default
				textureMod = particleMoonCutPoofHolder.textureSheetAnimation;
				textureMod.frameOverTime = 0;
				break;
			}

			//
			colorBuffer_MoonCutPoof = GameController.Instance.spriteController.colorArr_Particles_MoonsCutPoofs [randomInt];

			// Moon Idle Anim Clip
			animMoonHolder_IdleBase.clip = animClipMoonHolder_IdleArr [randomInt];

			// Spcecial SuperCurr Particle
			objMoonSuperCurr_SpecialParticle.SetActive (true);

			// Special size increase for sausage slices
//			objMoonSliced_Holder.transform.localScale *= 1.32F;

			break;
		case MoonType.AutoPlay:
//			spriteMoonCoreHolder.sprite = GameController.Instance.spriteController.spriteArr_MoonsCores [4];
//			spriteMoonHaloHolder.color = GameController.Instance.spriteController.colorArr_MoonsHalos_Normal [4];
//
//			spriteBuffer_Left = GameController.Instance.spriteController.spriteArr_MoonsSlices_Left [4];
//			spriteBuffer_Right = GameController.Instance.spriteController.spriteArr_MoonsSlices_Right [4];

			Debug.LogWarning ("WWTTTTTTTFFFFFFFFFFFFF");
			Debug.LogWarning (this.name + " WWTTTTTTTFFFFFFFFFFFFF " + transform.parent.parent.name);

			break;
		case MoonType.Other:
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}

		// Give Color to Halo
		colorBuffer_MoonHalo = spriteMoonHaloHolder.color;

		// Particle Color Glow
		ParticleSystem.MainModule main = particleMoonPickHolder.main;
		colorBuffer_MoonHalo.a = 0.8F;
		main.startColor = colorBuffer_MoonHalo;

		// Particle Color Flare
		main = particleMoonFlareHolder.main;
		colorBuffer_MoonHalo.a = 0.6F;
		main.startColor = colorBuffer_MoonHalo;

		// Particle for Super Curr
		if (moonType == MoonType.SuperCurrency) {
			colorBuffer_MoonHalo.a = 0.85F;
			main = particleMoonSuperCurr_Sparks.main;
			main.startColor = colorBuffer_MoonHalo;

			main = particleMoonSuperCurr_Glows.main;
			main.startColor = colorBuffer_MoonHalo;
		}

		// Particle Poof
		main = particleMoonCutPoofHolder.main;
		main.startColor = colorBuffer_MoonCutPoof;

		// Waste poof color
		main = particleMoonWastePoof.main;
		main.startColor = colorBuffer_MoonCutPoof;

		transform.localScale *= size;

		// Loop anims for moon
		if (!isOrbitingPlanet) {
			animMoonHolder_IdleBase.Play ();
			animMoonHolder_IdleBase [animMoonHolder_IdleBase.clip.name].time = Random.Range (0.1F, 1);
		}
		StartCoroutine (Moon_TingleRotateAnim ());
	}

	IEnumerator Moon_TingleRotateAnim () {
		yield return new WaitForSeconds (Random.Range (2, 5));
		animMoonHolder_IdleTingleRotate.Play ();
	}

	public void MoonCurrency (int whatCurrency) {

		if (moonType == MoonType.SuperCurrency) {
            // SOUND EFFECTS: Moon Hit SuperCurrency (Particle)
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Banana.Play();
            }
		} else {
            // SOUND EFFECTS: Moon Hit Other Score (Particle)
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Bread.Play();
            }
        }


		GameController.Instance.CurrencyIncrease_NoInstantHUD (whatCurrency);
//		GameController.Instance.CurrencyIncrease_YesInstantHUD (whatCurrency);

		GameController.Instance.CurrencyParticleMoon (whatCurrency, transform.position);
	}

	public void MoonWastePoof () {
		animMoonHolder.Play ();

		if (particleMoonWastePoof != null) {
			particleMoonWastePoof.Play ();
		}
	}

	void MoonDestroyHazard () {
		GameController.Instance.Player_DeathExplosion (transform.position);
		Destroy (this.gameObject);
	}

	void MoonDestroy () {
		circleColl2d_Holder.enabled = false;
		float degree = GameController.Instance.playerController.transform.eulerAngles.z;

		particleMoonPickHolder.Play ();
		particleMoonPickHolder.transform.SetParent (transform.root);


		particleMoonCutPoofHolder.Play ();
		particleMoonCutPoofHolder.transform.SetParent (transform.root);
		

		// To prevent the scale.z of particles from becoming zero AFTER parent change
//		if (isDesignerMoon) {
//			particleMoonPickHolder.transform.localScale = new Vector3 (particleMoonPickHolder.transform.localScale.x, particleMoonPickHolder.transform.localScale.x, particleMoonPickHolder.transform.localScale.x);
//			particleMoonCutPoofHolder.transform.localScale = new Vector3 (particleMoonCutPoofHolder.transform.localScale.x, particleMoonCutPoofHolder.transform.localScale.x, particleMoonCutPoofHolder.transform.localScale.x);
//		}

		objMoonMain_Holder.SetActive (false);

		// Sliced Part
		objMoonSliced_Holder.SetActive (true);

		objMoonSliced_Holder.transform.SetParent (GameController.Instance.gameSpawner.transform);

		animMoonHolder_Sliced.Play ();
		objMoonSliced_Holder.transform.eulerAngles = new Vector3 (0, 0, degree);

		// Determine which one is left or right at the moment of destroy / cut
		if (rigid2D_SlicedHolder1.transform.position.x < rigid2D_SlicedHolder2.transform.position.x) {
			spriteMoonSliceHolder1.sprite = spriteBuffer_Left;
			spriteMoonSliceHolder2.sprite = spriteBuffer_Right;

			rigid2D_SlicedHolder1.AddForce (new Vector2 (-floatSlicedParts_Force, 0), ForceMode2D.Impulse);
			rigid2D_SlicedHolder2.AddForce (new Vector2 (floatSlicedParts_Force, 0), ForceMode2D.Impulse);
		} else {
			spriteMoonSliceHolder1.sprite = spriteBuffer_Right;
			spriteMoonSliceHolder2.sprite = spriteBuffer_Left;

			rigid2D_SlicedHolder1.AddForce (new Vector2 (floatSlicedParts_Force, 0), ForceMode2D.Impulse);
			rigid2D_SlicedHolder2.AddForce (new Vector2 (-floatSlicedParts_Force, 0), ForceMode2D.Impulse);
		}

		Destroy (objMoonSliced_Holder, 2);
		//

		Destroy (this.gameObject, 1);
	}
}
