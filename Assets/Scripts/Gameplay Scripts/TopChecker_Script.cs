﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopChecker_Script : MonoBehaviour {

	public enum TopCheckerType {
		PlanetCheck,
		MoonCheck,
		BackCheck,
		OtherCheck
	}

	public TopCheckerType topCheckerType;

	void OnTriggerEnter2D (Collider2D other) {
		Debug.LogWarning ("Top Checker Planet Collision! WITH " + other.name + "  with tag of " + other.tag);
		switch (topCheckerType) {
		case TopCheckerType.PlanetCheck:
			if (other.tag == "Player") {
				GameController.Instance.Spawn_MorePlanets ();

//				GameController.Instance.Spawn_MorePlanets ();
//				GameController.Instance.Spawn_MorePlanets ();
//				GameController.Instance.Spawn_MorePlanets ();

				Destroy (this.gameObject);
			}
			break;
		default:
			Debug.LogError ("Default Error");
			break;
		}
	}
}
