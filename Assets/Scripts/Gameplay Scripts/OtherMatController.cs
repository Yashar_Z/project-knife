﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherMatController : MonoBehaviour {

	public GameObject objPlanetSpecialSource_IrregularRing;
	public GameObject objPlanetSpecial_ShrinOrbitWarn;

	public ParticleSystem particlePlanetSpecial_BigSmall;
	public ParticleSystem particlePlanetSpecial_Irregular;
	public ParticleSystem particlePlanetSpecial_Shrinking;

	public Transform[] transBackPlanetRefSetArr; 

	public Transform TransCheckpointParticle;
}
