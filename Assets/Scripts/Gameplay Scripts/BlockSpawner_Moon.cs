﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner_Moon : MonoBehaviour {

	[Header ("")]
	public bool isMoonGroup;

	[Header ("Single Moon Elements")]
	public MoonType moonTypeSingle;

	[Header ("Moon Group Elements")]
	public MoonPath_GroupType moonGroupType;
	public bool isRandomMoonGroupBehavior;
	public bool isMoonMoveSpeedPreset;
	public bool areMoonsMovelessBehavior;

	public MoonType[] moonTypesArr;

	public float floatMoonGroupAngle;
	public float floatMoonGroupAnimSpeed;
	public float floatMoonGroupEachDistance;

	[HideInInspector]
	public MoonController moonController;

	private MoonPath_GroupSpawner moonPath_GroupSpawnerRef;
	private GameObject objMoonPathGroupSource;

	void Start () {
		StartCoroutine (BlockSpawn_Moon ());
	}

	IEnumerator BlockSpawn_Moon () {
		for (int i = 0; i < transform.GetSiblingIndex (); i++) {
			yield return null;
		}

		if (!isMoonGroup) {
			StartCoroutine (Single_Moon (moonTypeSingle));
		} else {
			// Random
//			objMoonPathGroupSource = GameController.Instance.gameSpawner.transMoonPathGroupParent.GetChild (Random.Range (0, 2)).gameObject;
			yield return null;

			switch (moonGroupType) {
			case MoonPath_GroupType.Double_1:
			case MoonPath_GroupType.Double_2:
			case MoonPath_GroupType.Double_3:
			case MoonPath_GroupType.Double_4_MidHoriz:
			case MoonPath_GroupType.Double_5_MidHoriz:
			case MoonPath_GroupType.Double_6_MidHoriz:
			case MoonPath_GroupType.Double_7_ShortHoriz:
			case MoonPath_GroupType.Double_8_ShortHoriz:
			case MoonPath_GroupType.Double_9_ShortHoriz:
				objMoonPathGroupSource = GameController.Instance.gameSpawner.transMoonPathGroupParent.GetChild (0).gameObject;
				break;
			case MoonPath_GroupType.Triple_1:
			case MoonPath_GroupType.Triple_2:
			case MoonPath_GroupType.Triple_3:
			case MoonPath_GroupType.Triple_4_MidHoriz:
			case MoonPath_GroupType.Triple_5_MidHoriz:
			case MoonPath_GroupType.Triple_6_MidHoriz:
			case MoonPath_GroupType.Triple_7_ShortHoriz:
			case MoonPath_GroupType.Triple_8_ShortHoriz:
			case MoonPath_GroupType.Triple_9_ShortHoriz:
				objMoonPathGroupSource = GameController.Instance.gameSpawner.transMoonPathGroupParent.GetChild (1).gameObject;
				break;
			default:
				break;
			}

			GameObject objNewMoon = Instantiate (objMoonPathGroupSource);
			objNewMoon.transform.SetParent (transform);
			objNewMoon.transform.localScale = Vector3.one;
			objNewMoon.transform.localPosition = Vector3.zero;
			objNewMoon.transform.eulerAngles = new Vector3 (0, 0, floatMoonGroupAngle);

			//			Debug.LogWarning ("PlanetsOrbitsDistance_Calculate = " + PlanetsOrbitsDistance_Calculate ());
			moonPath_GroupSpawnerRef = objNewMoon.GetComponent<MoonPath_GroupSpawner> ();
			moonPath_GroupSpawnerRef.MoonTypeArr = moonTypesArr;
			moonPath_GroupSpawnerRef.floatAnimMoonPath = floatMoonGroupAnimSpeed;

			moonPath_GroupSpawnerRef.isMovelessGroup = areMoonsMovelessBehavior;

			moonPath_GroupSpawnerRef.isDesignerMoon = true;
			moonPath_GroupSpawnerRef.isMoonMoveSpeedPreset = isMoonMoveSpeedPreset;

			yield return null;

			moonPath_GroupSpawnerRef.moonPath_GroupType = moonGroupType;
			moonPath_GroupSpawnerRef.MoonPathGroup_Setup (floatMoonGroupEachDistance * 0.16F, isRandomMoonGroupBehavior);

//			for (int i = 0; i < moonTypesArr.Length; i++) {
//				StartCoroutine (Single_Moon (moonTypesArr[i]));
//			}
		}

	}

	IEnumerator Single_Moon (MoonType moonType) {
		yield return null;
		GameObject objNewMoon = Instantiate (GameController.Instance.gameSpawner.objMoonSource);
		Transform transNewMoon = objNewMoon.transform;

		transNewMoon.SetParent (transform);
		transNewMoon.localScale = Vector3.one;
		transNewMoon.localPosition = Vector3.zero;

		yield return null;
		moonController = objNewMoon.GetComponent <MoonController> ();
		moonController.isDesignerMoon = true;
		moonController.Moon_Setup (moonTypeSingle, GameController.Instance.gameSpawner.floatSingleMoonStandardSize, false);
	}

}
