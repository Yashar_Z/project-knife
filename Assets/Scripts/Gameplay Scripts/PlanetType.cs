﻿public enum PlanetType {
	Normal,
	BigSmall,
	IrregularRotate,
	ShrinkingBomb,
	Other
}

public enum PlanetColliderType {
	OrbitCollider,
	SpawnerCollider,
	Other
}

public enum MoonType {
	Hazard,
	Currency,
	SuperCurrency,
	AutoPlay,
	Other
}

public enum MoonPath_GroupType {
	Double_1,
	Double_2,
	Double_3,
	Triple_1,
	Triple_2,
	Triple_3,

	Double_4_MidHoriz,
	Double_5_MidHoriz,
	Double_6_MidHoriz,
	Triple_4_MidHoriz,
	Triple_5_MidHoriz,
	Triple_6_MidHoriz,

	Double_7_ShortHoriz,
	Double_8_ShortHoriz,
	Double_9_ShortHoriz,
	Triple_7_ShortHoriz,
	Triple_8_ShortHoriz,
	Triple_9_ShortHoriz,
	Other
}
