﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCheckpoint_Script : MonoBehaviour {

	public bool isParticleParent;

	public ParticleSystem particleOrbGather;
	public ParticleSystem particleOrbPulse;
	public ParticleSystem particleOrbPulse2;
	public ParticleSystem particleLensFlare;
	public ParticleSystem particleOrbEnd;
	public ParticleSystem particleOrbSpread;

	public ParticleCheckpoint_Script partCheckpointScriptHolder;

	private float floatTimeScaleBuffer;

	public void OnParticleSystemStopped()
	{
		if (!isParticleParent) {
			Debug.Log (this.name + "Stop");
			partCheckpointScriptHolder.OrbitEnd_Particles ();

			StartCoroutine (partCheckpointScriptHolder.ChangePlanet ());

			// Change checkpoint Number / name
			GameController.Instance.hudScriptHolder.Checkpoint_FullNameUpdate ();
		}
	}

	public void OrbitStart_OnlyGatherParticle () {
		ParticleSystem.MainModule particleMain1 = particleOrbGather.main;
		particleMain1.startLifetime = 0.6F;

		particleOrbGather.Play ();

		GameController.Instance.particleWhiteFlash_Slow.Play ();

		// SOUND EFFECTS: Checkpoint Planet Prequel (Empty)
	}

	public void OrbitStart_OtherParticles () {
		particleOrbPulse.Play ();
		particleLensFlare.Play ();
//		particleOrbPulse2.Play ();

		StartCoroutine (OrbitEnd_OnlyGatherParticle ());

		floatTimeScaleBuffer = Time.timeScale;
		Time.timeScale = 1.2F;

        // SOUND EFFECTS: Checkpoint Planet Main Part (done)
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.CheckPoint.Play();
        }
    }

	IEnumerator OrbitEnd_OnlyGatherParticle () {
		yield return new WaitForSeconds (1.5F);
		particleOrbGather.Stop ();
	}

	public void OrbitEnd_Particles () {
//		particleOrbGather.Stop ();

		ParticleSystem.MainModule particleMain2 = particleOrbGather.main;
		particleMain2.startLifetime = 0.2F;

		particleOrbEnd.Play ();
		particleOrbSpread.Play ();

		Time.timeScale = floatTimeScaleBuffer;

		GameController.Instance.particleWhiteFlash_Quick.Play ();
	}

	IEnumerator ChangePlanet () {
		yield return null;
		GameController.Instance.planetController_Selected.Planet_EndCheckpoint ();
	}
}
