﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public static System.Action OnPlayerAttack;
	public static System.Action OnPlayerEnterOrbit;

	public enum PlayerStatus
	{
		IsOrbiting,
		IsFlying,
		IsAutoChase,
		IsDead
	}

	public static PlayerStatus playerStatus;

//	public Rigidbody2D rigid2D_PlayerHolder;
//	public PolygonCollider2D polyColl2d_Holder;

	public SpriteRenderer spriteRenderPlayer;

	public Player_Collider playCollider_Orbitter;
	public Player_Collider playCollider_Body;

	[HideInInspector]
	public Collider2D coll2dBuffer;

	[HideInInspector]
	public PlanetController planetControllerRef;

	[Header ("Player Normal Speed")]
//	public float floatNormalSpeed;
	public float floatNormalSpeed_Translate;

	[Header ("")]
//	public float floatNormalSpeedBuffer;
	public float floatSlashSpeed_RefSize;
	public float floatSlashSpeed_Start_Initial;
	public float floatSlashSpeed_During_Initial;

	[HideInInspector]
	public float floatSlashSpeed_Start;
	[HideInInspector]
	public float floatSlashSpeed_During;

	public bool canPlay;
	public bool canFly;

	[Header ("Animation Player")]
	public AnimationClip[] animClipPlayerArr;
	public Animation animPlayerHolder;

	public AnimationClip[] animClipPlayerArtKnifeArr;
	public Animation animPlayerArtKnifeHolder;

	[Header ("Tutorial Particle")]
	public GameObject objTutorialLinesParticle;
	private bool isTutParticleDestroyed;

	[Header ("")]
	public Vector3 v3_RotateBuffer;
	public Vector2 v2_MoveBuffer;

	public Transform transFreeMoveHolder;

	public bool hasEnterAtLeastOneOrbit;

//	private float floatPlanetAngle;
	private float floatRotateTarget;
	private Transform transKnifeChildHolder;

	private Vector3 v3_PlayerSizeBuffer;

//	void Start () {
//	}

	public void Start_GamePlayer () {
		hasEnterAtLeastOneOrbit = false;

		Player_ParentFree ();

		canFly = GameController.Instance.canFly;

		if (canFly) {
			if (!GameController.Instance.isMenuMode) {
				StartCoroutine (StartPlayer ());
			} else {
				StartCoroutine (StartPlayer_NoDelay ());
			}
		}

		// For speed from fast to normal
		//		floatNormalSpeedBuffer = floatNormalSpeed;

		// Save base player size
		v3_PlayerSizeBuffer = transform.localScale;
	}

	IEnumerator GameSlowMo () {
		Time.timeScale = 0.8F;
		yield return new WaitForSeconds (0.1F);

		Time.timeScale = 0.65F;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;

		Time.timeScale = 0.4F;
		yield return null;
		yield return null;
		yield return null;

		Time.timeScale = 0.65F;
		yield return null;
		yield return null;
		yield return null;
		yield return null;

		Time.timeScale = 0.75F;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;

		Time.timeScale = 0.8F;
		yield return new WaitForSeconds (0.1F);
		Time.timeScale = 0.9F;
		yield return new WaitForSeconds (0.1F);
		Time.timeScale = 1;
	}

	public void OnCollider_Orbiter (Collider2D other) {
//			StartCoroutine (GameSlowMo ());

		// For the first time entering an orbit
		if (!hasEnterAtLeastOneOrbit) {
			hasEnterAtLeastOneOrbit = true;
			GameController.Instance.camController.objCamEdgeCollidersParent.SetActive (!GameController.Instance.camController.isNoEdgeCamera);
			Player_SpriteLayer_Normal ();

			// Show tutorial particle
			if (objTutorialLinesParticle != null) {
				objTutorialLinesParticle.SetActive (true);
			}
		}

		playerStatus = PlayerStatus.IsOrbiting;

		canFly = false;

//		floatPlanetAngle = other.transform.parent.localEulerAngles.z;

		transform.SetParent (other.transform.parent);
		v3_RotateBuffer = transform.eulerAngles;

//			transform.eulerAngles = Vector3.zero;
//			Debug.LogError ("other rotation = " + other.transform.eulerAngles);

		floatRotateTarget = Mathf.Atan2 ((transform.localPosition.y - other.transform.localPosition.y), (transform.localPosition.x - other.transform.localPosition.x)) * Mathf.Rad2Deg;
		floatRotateTarget += 90;

		StartCoroutine (Knife_RotateCorrect ());

		other.enabled = false;
		coll2dBuffer = other;

		StartCoroutine (Player_CanPlay (0.1F));

		planetControllerRef = coll2dBuffer.GetComponentInParent <PlanetController> ();
		GameController.Instance.SelectedPlanet_Update (planetControllerRef);

		// Start scaling the player knife to oppose orbits scale (Moved to the planet script in the Shrinking_PlanetEnterStuff
//		if (planetControllerRef.planetType == PlanetType.ShrinkingBomb) {
//			animPlayerHolder.clip = animClipPlayerArr [1];
//			animPlayerHolder [animPlayerHolder.clip.name].speed = Mathf.Abs(planetControllerRef.floatOrbitSpecialPlanetSpeed);
//
//			Debug.LogWarning ("END PLAYER Here!? " + this.name + " floatOrbitSpecialPlanetSpeed = " 
//				+ planetControllerRef.floatOrbitSpecialPlanetSpeed + "\n while animPlayerHolder.speed = " + animPlayerHolder[animPlayerHolder.clip.name].speed);
//			animPlayerHolder.Play ();
//		}

		Player_AnimSizeReset ();
		planetControllerRef.Planet_PlayerEntered ();
			
		// Reset / Disable Dark Trail the moment knife enters new planet
//		animPlayerArtKnifeHolder.Stop ();
//		animPlayerArtKnifeHolder.clip = animClipPlayerArtKnifeArr[1];
//		animPlayerArtKnifeHolder.Play ();

		// Scale knife with hit animation
		animPlayerArtKnifeHolder.Stop ();
		animPlayerArtKnifeHolder.clip = animClipPlayerArtKnifeArr[2];
		animPlayerArtKnifeHolder.Play ();

		if (OnPlayerEnterOrbit != null) {
			OnPlayerEnterOrbit ();
		}

		CheckpointReach_Check ();
	}

	public void CheckpointReach_Check () {
		
		// Checkpoint Reached!
		if (GameController.Instance.isCheckpointTimeActive) {
			// Show effects and checkpoint!
			GameController.Instance.camController.Cam_GoFast_CheckPoint ();

			// Disable HUD Checkpoint
			GameController.Instance.hudScriptHolder.CheckpointRemainEnabler (false);

			// Start gathering particles
			GameController.Instance.particleCheckScriptHolder.OrbitStart_OnlyGatherParticle ();

			// FireBase Event: Checkpoint Funnel Start
			FirebaseManager.FireBaseEventStartLevel (GameController.Instance.intCheckpoint_TotalTarget.ToString ());

			// FireBase Event: Checkpoint Funnel End
			FirebaseManager.FireBaseEventEndLevel (GameController.Instance.intCheckpoint_TotalReached.ToString ());
		} 

		else {
			// Don't do anything
		}
	}

	public void Player_ReviveReset (Vector3 v3_Location) {
		canFly = false;

		transFreeMoveHolder.eulerAngles = Vector3.zero;
		Player_ParentFree ();
		transform.localEulerAngles = Vector3.zero;

		// Don't use size = V3.One because need to use buffer.
		transform.localScale = v3_PlayerSizeBuffer;
		transform.position = new Vector3 (v3_Location.x, v3_Location.y - 5, v3_Location.z);

		// Reset size for players that died in shrinking
		Player_AnimSizeReset ();

		Player_ResurrectKnifeAnimate ();

		StartCoroutine (StartPlayer ());
		StartCoroutine (PlayerSpeed_ReviveRoutine ());
	}

	IEnumerator PlayerSpeed_ReviveRoutine () {
		float floatBuffer = floatNormalSpeed_Translate;

		floatNormalSpeed_Translate = 0F;
		yield return new WaitForSeconds (0.5F);

		floatNormalSpeed_Translate = 0.1F;
		yield return new WaitForSeconds (0.1F);

		floatNormalSpeed_Translate = 0.2F;
		yield return new WaitForSeconds (0.1F);

		while (floatNormalSpeed_Translate < floatBuffer) {
			floatNormalSpeed_Translate = Mathf.MoveTowards (floatNormalSpeed_Translate, floatBuffer, Time.deltaTime * 20);
			yield return null;
		}
	}

	IEnumerator Player_CanPlay (float delay) {
		yield return new WaitForSeconds (delay);

		if (!GameController.Instance.isCheckpointTimeActive) {
			canPlay = true;
		}
	}

	IEnumerator Knife_RotateCorrect () {
		yield return null;

		// Re-rotate the player knife parent / main
		v3_RotateBuffer.z = floatRotateTarget;
		transform.localEulerAngles = v3_RotateBuffer;

		/*
		float floatFinalKnifeAngle = floatRotateTarget + floatPlanetAngle;
		if (floatFinalKnifeAngle > 360) {
			floatFinalKnifeAngle = floatFinalKnifeAngle % 360;
		}


		Debug.LogError ("floatPlanetAngle = " + floatPlanetAngle + "   floatRotateTarget = " + floatRotateTarget + "   floatFinalKnifeAngle = " + floatFinalKnifeAngle);

		Vector3 v3_KnifeChild_RotateBuffer = transKnifeChildHolder.localEulerAngles;
		v3_KnifeChild_RotateBuffer.z -= (floatFinalKnifeAngle);
		transKnifeChildHolder.localEulerAngles = v3_KnifeChild_RotateBuffer;

		float floatKnifeChild_Target;
		if (floatFinalKnifeAngle > 180) {
			floatKnifeChild_Target = 0;
		} else {
			floatKnifeChild_Target = 0;
		}

		Debug.LogError ("floatKnifeChild_Target = " + floatKnifeChild_Target + "     v3_KnifeChild_RotateBuffer.z = " + v3_KnifeChild_RotateBuffer.z);

//		Debug.Break ();

		canRotate = true;

		yield return null;
		while (canRotate) {
//			transKnifeChildHolder.eulerAngles =

			Debug.LogWarning ("v3_KnifeChild_RotateBuffer.z = " + v3_KnifeChild_RotateBuffer.z + "   floatKnifeChild_Target = " + floatKnifeChild_Target);
			v3_KnifeChild_RotateBuffer.z = Mathf.MoveTowards (v3_KnifeChild_RotateBuffer.z, floatKnifeChild_Target, Time.deltaTime * 40);
			transKnifeChildHolder.localEulerAngles = v3_KnifeChild_RotateBuffer;

			if (v3_RotateBuffer.z == floatKnifeChild_Target) {
				canRotate = false;
			}
			yield return null;
		}
		*/
	}

	public void Player_ParentFree () {
		transform.SetParent (transFreeMoveHolder);
	}

	IEnumerator StartPlayer () {
		yield return new WaitForSeconds (0.4F);
		playerStatus = PlayerStatus.IsFlying;

		canFly = true;
		StartCoroutine (Player_MoveRoutine ());
	}

	IEnumerator StartPlayer_NoDelay () {
		playerStatus = PlayerStatus.IsFlying;
		canFly = true;

		StartCoroutine (Player_MoveRoutine ());
		yield return null;
	}

	IEnumerator Player_RushRoutine () {
		playerStatus = PlayerStatus.IsFlying;
		yield return null;

		v2_MoveBuffer = transform.localPosition;
		v2_MoveBuffer.y += floatSlashSpeed_Start;
		transform.localPosition = v2_MoveBuffer;

		int counter = 0;
		while (counter < 10) {
			transform.Translate (Vector3.up * floatSlashSpeed_During * Time.deltaTime);
			counter++;
		}

//		StartCoroutine (Slash_SpeedToNormal ());
		StartCoroutine (Player_MoveRoutine ());
	}

	IEnumerator Player_MoveRoutine () {
		playerStatus = PlayerStatus.IsFlying;
		yield return null;

		while (canFly) {
			transform.Translate (Vector3.up * floatNormalSpeed_Translate * Time.deltaTime);

			yield return null;
		}
	}

	/*
	IEnumerator Slash_SpeedToNormal () {
		// Moved to top (Before this)
//		floatNormalSpeedBuffer = floatNormalSpeed;

		floatNormalSpeed = floatSlashSpeed_During;

		while (floatNormalSpeed != floatNormalSpeedBuffer) {
			Debug.Log ("Speed Normalized");
			floatNormalSpeed = Mathf.MoveTowards (floatNormalSpeed, floatNormalSpeedBuffer, Time.deltaTime / 2);
			yield return null;
		}

		yield return null;
	}
	*/

	public void Player_ResurrectKnifeAnimate () {
		animPlayerArtKnifeHolder.Stop ();
		animPlayerArtKnifeHolder.clip = animClipPlayerArtKnifeArr[4];
		animPlayerArtKnifeHolder.Play ();
	}

	public void Player_MenuToGameAnimate () {
		animPlayerArtKnifeHolder.Stop ();
		animPlayerArtKnifeHolder.clip = animClipPlayerArtKnifeArr[3];
		animPlayerArtKnifeHolder.Play ();
	}

	public void Player_SpriteEnabler (bool isActive) {
		spriteRenderPlayer.enabled = isActive;
	}

	public void Player_SpriteChange (Sprite sprite) {
		spriteRenderPlayer.sprite = sprite;
	}

	public void Player_SpriteLayer_FrontUI () {
		spriteRenderPlayer.sortingLayerName = "UI";
		spriteRenderPlayer.sortingOrder = 5;
	}

	public void Player_SpriteLayer_Normal () {
		spriteRenderPlayer.sortingLayerName = "Player";
		spriteRenderPlayer.sortingOrder = 1;
	}

	public void Player_ColliderDoubleEnabler (bool isActive) {
		playCollider_Orbitter.enabled = isActive;
		playCollider_Body.enabled = isActive;
	}

	public void Player_AnimSizeShrinkStart (float floatSpeed) {
		animPlayerHolder.clip = animClipPlayerArr [1];
		animPlayerHolder [animPlayerHolder.clip.name].speed = Mathf.Abs(floatSpeed);
		animPlayerHolder.Play ();

		Debug.LogWarning ("END PLAYER Here!? " + this.name + " floatSpeed = " 
			+ floatSpeed + "\n while animPlayerHolder.speed = " + animPlayerHolder[animPlayerHolder.clip.name].speed);
	}

	public void Player_AnimSizeReset () {
		animPlayerHolder.Stop ();
		animPlayerHolder.clip = animClipPlayerArr [0];
		animPlayerHolder.Play ();
	}

//	IEnumerator Player_ColliderDoubleToggle () {
//		polyColl2d_Holder.enabled = false;
//		yield return new WaitForSeconds (Time.deltaTime);
//		polyColl2d_Holder.enabled = true;
//	}

	void Player_ReturnToNormal () {
		Time.timeScale = 1;
		transform.localScale = v3_PlayerSizeBuffer;
	}

	void Update () {
		if (Input.GetMouseButton (0) || Input.GetKeyDown ("space")) {
			if (canPlay && !canFly) {
				canPlay = false;

				// Base the slash speed on the size of planet
				floatSlashSpeed_RefSize = planetControllerRef.Planet_SizeToSlashSpeed ();
				floatSlashSpeed_Start = (floatSlashSpeed_Start_Initial * floatSlashSpeed_RefSize / 1.4F);
				floatSlashSpeed_During = (floatSlashSpeed_During_Initial * floatSlashSpeed_RefSize / 1.4F);

				//
				if (!planetControllerRef.wasDestroyed) {
					planetControllerRef.Planet_Destroy (transform.eulerAngles.z);
				}

				// Order of these 2 is IMPRTANT! 
				transFreeMoveHolder.eulerAngles = transform.eulerAngles;
				transform.SetParent (transFreeMoveHolder);

				canFly = true;
				StartCoroutine (Player_RushRoutine ());
//				StartCoroutine (Player_MoveRoutine ());

				Player_AnimSizeReset ();

				// Reset scaling the player knife to oppose orbits scale
				switch (planetControllerRef.planetType) {

                    // SOUND EFFECTS: Knife Slice Planet (done)
                    case PlanetType.Normal:
                        if (SoundManager.Instance != null)
                        {
                            SoundManager.Instance.CutMamoli.Play();
                        }
                        break;
				case PlanetType.BigSmall:
                        if (SoundManager.Instance != null)
                        {
                            SoundManager.Instance.CutPanirMoraba.Play();
                        }
                        break;
				case PlanetType.IrregularRotate:
                        if (SoundManager.Instance != null)
                        {
                            SoundManager.Instance.CutSayareAbi.Play();
                        }
                        break;
				case PlanetType.ShrinkingBomb:
                        // SOUND EFFECTS: Player Leave Shrinking Orbit END (done)
                        if (SoundManager.Instance != null)
                        {
                            SoundManager.Instance.ShrinkingCheese.Stop();
                        }

                        break;
				default:
					break;
				}
					
				// Player Knife Shadow and squish anim
				animPlayerArtKnifeHolder.clip = animClipPlayerArtKnifeArr[0];
				animPlayerArtKnifeHolder.Play ();

				// Return base player size when shooting
				Player_ReturnToNormal ();

				// Full screen flash with planet desctruction
				GameController.Instance.particlePlanetCutFlash.Play ();

				// Update last planet for continue (To make sure new continue planet is not spawned on a orbit player just entered before death)
				GameController.Instance.SelectedPlanet_ContinuefterDestroy (planetControllerRef.transform.position);

				// Continue Counter Increase
				GameController.Instance.Continue_GiverCheck ();

				// Score player
				for (int i = 0; i < GameController.Instance.particlePlayerScoreArr.Length; i++) {
					GameController.Instance.particlePlayerScoreArr [i].Play ();
				}

				if (OnPlayerAttack != null) {
					OnPlayerAttack ();
				}

				if (!isTutParticleDestroyed) {
					isTutParticleDestroyed = true;
					Destroy (objTutorialLinesParticle);
				}

			}
		}

		if (Input.GetKeyDown ("r")) {
//			ClearLog ();
			GameController.Instance.RestartGameNOW ();
		}

		if (Input.GetKeyDown ("s")) {
			if (Time.timeScale == 1) {
				Time.timeScale = 3;
			} else {
				Time.timeScale = 1;
			}
		}

		if (Input.GetKeyDown ("d")) {
			GameController.Instance.Player_Death ();
		}

		if (Input.GetKeyDown ("g")) {
			GameController.Instance.menuScript.DailyGift_Activator (true);
		}
	}
	#if UNITY_EDITOR
	public void ClearLog()
	{
		var assembly = System.Reflection.Assembly.GetAssembly(typeof(UnityEditor.Editor));
		var type = assembly.GetType("UnityEditor.LogEntries");
		var method = type.GetMethod("Clear");
		method.Invoke(new object(), null);
	}
	#endif
}
