﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteController : MonoBehaviour {

	public Sprite[] spriteArr_PlanCores_Normal;
	public Sprite[] spriteArr_PlanSlice_Normal;
	//
	public Color[] colorArr_PlanHalos_Normal;
	public Color[] colorArr_Particles_Normal;

	public Sprite[] spriteArr_PlanCores_BigSmall;
	public Sprite[] spriteArr_PlanSlice_BigSmall;
	public Color[] colorArr_PlanHalos_BigSmall;
	public Color color_Particles_BigSmall;

	public Sprite[] spriteArr_PlanCores_IrregularRotate;
	public Sprite[] spriteArr_PlanSlice_IrregularRotate;
	public Color[] colorArr_PlanHalos_IrregularRotate;
	public Color color_Particles_IrregularRotate;

	public Sprite[] spriteArr_PlanCores_ShrinkingBomb;
	public Sprite[] spriteArr_PlanSlice_ShrinkingBomb;
	public Color[] colorArr_PlanHalos_ShrinkingBomb;
	public Color color_Particles_ShrinkingBomb;

	public Sprite[] spriteArr_MoonsCores;
	public Sprite[] spriteArr_MoonsSlices_Left;
	public Sprite[] spriteArr_MoonsSlices_Right;
	public Sprite[] spriteArr_MoonsHaloType;
	public Color[] colorArr_MoonsHalos_Normal;
	public Color[] colorArr_MoonsHalos_Super;
	public Color[] colorArr_Particles_MoonsCutPoofs;

	public Sprite[] spriteArr_DottedLine;

	public Color[] colorArr_BackPlanets;
	public Color[] colorArr_BackSmokes;

	// Menu Colors
	[Header ("")]

	public Color[] colorArrMenu_PlayButton;
	public Color[] colorArrMenu_PlayTextColor;

	public Color[] colorArrMenu_OtherButton;
	public Color[] colorArrMenu_OtherTextColor;
}
