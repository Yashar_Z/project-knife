﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet_OrbitAnimsScript : MonoBehaviour {

	public PlanetController planetController;

	public void AnimEvent_OrbitScaleEnd () {
		planetController.Planet_ShrinkingDeath ();
	}
}
