﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyMoveMulti_Script : MonoBehaviour {

	public CurrencyMover_Script[] currMoverScriptArr;

	// Use this for initialization
	void Start () {
		StartCoroutine (CurrMover_MultiRoutine ());
	}

	IEnumerator CurrMover_MultiRoutine () {
		yield return new WaitForSeconds (0.2F);
		for (int i = 0; i < currMoverScriptArr.Length; i++) {
			currMoverScriptArr [i].canMove = true;
			currMoverScriptArr [i].transform.SetParent (GameController.Instance.transParentCurrParticle);
			yield return new WaitForSeconds (0.2F);
		}
	}
}
