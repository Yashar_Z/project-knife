﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour {

	public static GameController Instance;

	[Header ("Designer Mode")]
	public bool isDesignerMode;

	public Vector3Int[] v3Int_BlockPoolChancesArr;
	public int[] int_BlockEventChancesArr;

	[Header ("Menu Mode")]
	public bool isMenuMode;
	public bool isQuickRestart;

	[Header ("Moon Type Chances")]
	public int intMoonChance_Hazard;
	public int intMoonChance_Currency;
	public int intMoonChance_SuperScore;
	public int intMoonChance_Autoplay;

	private int intMoonChance_Hazard_Initial;
	private int intMoonChance_Currency_Initial;
	private int intMoonChance_SuperScore_Initial;
	private int intMoonChance_Autoplay_Initial;

	[Header ("")]
	public int intFirstPlanetCanHazard;

	[Header ("")]
	public int intMoonCountMin;
	public int intMoonCountMax;
	public int intPlanetsIncrease_MoonMin;
	public int intPlanetsIncrease_MoonMax;

	public int intMoonZeroChancePercent;

	private int intMoonCountMin_Initial;
	private int intMoonCountMax_Initial;

	[Header ("Orbit Sizes")]
	public float floatOrbitSize_Min;
	public float floatOrbitSize_Max;

	public float floatOrbitSize_MinDecrease;
	public float floatOrbitSize_MaxDecrease;

	[Header ("")]
	public float floatOrbitSize_AbsoluteMin;
	public float floatOrbitSize_AfterAbsolute_Min;
	public float floatOrbitSize_AfterAbsolute_Max;

	public int intPlanetsDecrease_Size;

	[Header ("")]
	public float floatOrbitSize_IntroMin;
	public float floatOrbitSize_IntroMax;

	public float floatOrbitSize_IntroMinDecrease;
	public float floatOrbitSize_IntroMaxDecrease;

//	public float floatOrbitSize_IntroAbsMin;
//	public float floatOrbitSize_AfterIntro_Min;
//	public float floatOrbitSize_AfterIntro_Max;

	public int intPlanetsIncrease_SizeReach;

	[HideInInspector]
	public bool isIntroSectionSizeDone;

	[HideInInspector]
	public int intIntroSizeSpawnCounter;

	[HideInInspector]
	public int intTimesMaxOrbitSpeedReached = 0;

	[HideInInspector]
	public int intTimesMaxOrbitSizeReached = 0;

	[Header ("")]
	public float floatPlanetHorizLimit;

	[Header ("Orbit Speeds")]
	public float floatOrbitSpeed_Min;
	public float floatOrbitSpeed_Max;

	private float floatOrbitSpeed_Min_Initial;
	private float floatOrbitSpeed_Max_Initial;

	public float floatOrbitSpeed_MinIncrease;
	public float floatOrbitSpeed_MaxIncrease;

	[Header ("")]
	public float floatOrbitSpeed_AbsoluteMax;
	public float floatOrbitSpeed_AfterAbsolute_Min;
	public float floatOrbitSpeed_AfterAbsolute_Max;

	public int intPlanetsIncrease_SpeedReach;

	[Header ("Planet Sizes & Speeds")]
	// Now use orbits for sizes
//	public float floatPlanetSize_Min;
//	public float floatPlanetSize_Max;

	public float floatPlanetSpeed_Min;
	public float floatPlanetSpeed_Max;

	private float floatPlanetSpeed_Min_Initial;
	private float floatPlanetSpeed_Max_Initial;

	public float floatPlanetSpeed_MinIncrease;
	public float floatPlanetSpeed_MaxIncrease;

	[Header ("")]
	public float floatPlanetSpeed_AbsoluteMax;
	public float floatPlanetSpeed_AfterAbsolute_Min;
	public float floatPlanetSpeed_AfterAbsolute_Max;

	[Header	("Difficulty Counters (Heights)")]
	public int intDifficultyIncreasePerPlanet;
	public int intDifficultyCounter;
	public int intDifficultyMax;

	[Header ("Difficulty Hazard Chance Increase After Planets")]
	public int intPlanetsIncrease_HazardChance;
	public int intHazardItselfIncrease;

	public int intMoonChance_Hazard_AbsoluteMax;
	public int intMoonChance_Hazard_AfterMax;

	[Header ("Step Chance for ALL Difficulty Increase After Planets")]
	public int intPlanetsCounter_Step;

	[Header ("Difficulty Forced Trigger")]
	public int intForcedTrigger_Hazard;
	public int intForcedTrigger_SpecialPlanet;
	public int intForcedTrigger_MoonPath;
	public int intForcedTrigger_MoonPathGroup;
	public int intForcedTrigger_Event;

	[Header ("")]
	public int intForcedTrigger_Hazardstep;
	public int intForcedTriggerSpecPlanet_Step;
	public int intForcedTriggerMoonPath_Step;
	public int intForcedTriggerMoonPathGroup_Step;
	public int intForcedTriggerEvent_Step;

	[Header ("Special Planet Speeds")]
	public float floatPlanetSpecSpeed_BigSmall_Min;
	public float floatPlanetSpecSpeed_BigSmall_Max;
	public float floatPlanetSpecSpeed_Irregular_Min;
	public float floatPlanetSpecSpeed_Irregular_Max;
	public float floatPlanetSpecSpeed_Shrinking_Min;
	public float floatPlanetSpecSpeed_Shrinking_Max;

	[Header ("Special Planets Chances")]
	public int intPlanetSpecialChance_All;
	public int intPlanetsIncrease_SpecialChanceAll;

	[Header ("")]
	public int intPlanetSpecialChance_BigSmall;
	public int intPlanetSpecialChance_IrregularRotate;
	public int intPlanetSpecialChance_ShrinkingBomb;

	[Header ("Event Planets Set")]
//	public int intEventPlanets_Set;
	public int intEventPlanets_CountMin;
	public int intEventPlanets_CountMax;

	public int intEventPlanets_MixedChancePercent;

	[Header ("Spawn Count Per Call")]
	public int intSpawnHowMany_Initial;
	public int intSpawnHowMany_Step;

	[Header ("")]
	public int intSpawnHowMany;

	[Header ("")]
	public float floatPlanetHorizDistance_Forced;
	public int intScore_Curr;

	public int intCurrency_Curr;
	public int intScore_Highscore;

	public bool canFly;
	public bool canDie;

	[Header ("Checkpoint")]
	public int intCheckpoint_CounterCurr;
	public int intCheckpoint_Target;

	public int intCheckpoint_TotalTarget;
	public int intCheckpoint_TotalReached;
	public int[] intCheckpoint_TargetsArr;

	public bool isCheckpointTimeActive;

	[Header ("Continue Stuff")]
	public int intContinue_AfterThis_Base;
	public int intContinue_AfterThis_HighScoreTreshold;

	private bool wasInstantContinueGiven;
	private bool wasContinueGiven;

	private int intInstantContinueCount = 0;
	private int intContinueCount = 0;

	private int intContinue_GiveCounter;

	[Header ("Controllers & Scripts")]
	public Menu_Script menuScript;
	public CamController camController;
	public PlayerController playerController;
	public SpriteController spriteController;
	public OtherMatController otherMatController;
	public ParticleCheckpoint_Script particleCheckScriptHolder;
	public HUD_Script hudScriptHolder;

	[Header ("Daily Gift & Other Rewards")]
	public int intDailyGift_TimeForEach;
	public int intDailyGift_RewardAmount;
	public bool isDailyGiftAvailable;

	public int intGameOver_VidRewardAmount;

	[Header ("Ads & Highscore")]
	public bool isHighScore;
	public bool isAdVideoAvailable_Reward;
	public bool isAdVideoAvailable_Forced;

	public int intAdForced_HowManyPlays;

	[HideInInspector]
	public PlanetController planetController_Selected; 

	[HideInInspector]
	public Vector3 v3_planetSelectedPosit; 

	[HideInInspector]
	public Vector3 v3_planetRevivePosit;

	[HideInInspector]
	public int intBackPlanet_SetHeigth;

	[Header ("")]
	public Game_Spawner gameSpawner;

	public Transform transPlayerHolder;
	public Transform transFreeMoveHolder;
	public Transform transDeathExplodesParent;
	public Transform transDeathKnifeParent;

	public Transform transCurrMoverTargetHolder;
	public Transform transParentCurrParticle;
	public GameObject objCurrMoverSource_Single;
	public GameObject objCurrMoverSource_Multi;
	public int intCurrMoverCounter;

	public GameObject objCamViewPort;

	public GameObject objTeamLogoIntro_Top;
	public GameObject objTeamLogoIntro_Bottom;

	public ParticleSystem particleFadeToBlack;
	public ParticleSystem particleFadeToBlackFAST;
	public ParticleSystem particleFadeToVisible;
	public ParticleSystem particleRedAlarm;
	public ParticleSystem particleRedAlarmMini;
	public ParticleSystem particleWhiteFlash_Slow;
	public ParticleSystem particleWhiteFlash_Quick;
	public ParticleSystem particleFullscreenBackStarsFastHolder;
	public ParticleSystem particlePlanetCutFlash;

	public ParticleSystem[] particlePlayerScoreArr;

	public ParticleSystem particleCheckpointFlare_Mini;
	public ParticleSystem particleCheckpointFlare_Full;

//	public ParticleSystem particleCurrencyMoonHolder;
//	public ParticleSystem particleCurrencyMoonHolder_NewLoop;

	public ParticleSystem[] particleDeathExplodeArr;
	public ParticleSystem[] particleResurrRaysArr;

	public ParticleSystem particleBonusContinQuickBlue;

	public GameObject objDeathBlackHolder;
	public GameObject objMenuBlackFrontsHolder;

	public GameObject objMenuVideoBlocker;

//	[HideInInspector]
	public Sprite spritePlayerBuffer;

	void OnEnable () {
		PlayerController.OnPlayerAttack += onPlayerAttack;

		// Death Events
		Cam_Collider.OnPlayerCamDeath += onPlayerCamDeath;
		MoonController.OnPlayerHazardDeath += onPlayerHazardDeath;
		PlanetController.OnPlayerShrinkingDeath += onPlayerShrinkingDeath;

		if (PlayerData.Instance != null) {
			isAdVideoAvailable_Reward = PlayerData.Instance.isAdVideoAvailable_Reward;
			isAdVideoAvailable_Forced = PlayerData.Instance.isAdVideoAvailable_Forced;
		}
	}

	void OnDisable () {
		PlayerController.OnPlayerAttack -= onPlayerAttack;

		// Death Events
		Cam_Collider.OnPlayerCamDeath -= onPlayerCamDeath;
		MoonController.OnPlayerHazardDeath -= onPlayerHazardDeath;
		PlanetController.OnPlayerShrinkingDeath -= onPlayerShrinkingDeath;

		if (PlayerData.Instance != null) {
			PlayerData.Instance.isAdVideoAvailable_Reward = isAdVideoAvailable_Reward;
			PlayerData.Instance.isAdVideoAvailable_Forced = isAdVideoAvailable_Forced;
		}
	}

	void Awake () {
		Instance = this;
		GameController_Startup ();

		// Enable or disable intro logos at the start
		if (PlayerData.Instance != null) {
			if (PlayerData.Instance.gameMenuType == PlayerData.GameMenuType.Startup) {
				objTeamLogoIntro_Top.SetActive (true);
				objTeamLogoIntro_Bottom.SetActive (true);
			} else {
				objTeamLogoIntro_Top.SetActive (false);
				objTeamLogoIntro_Bottom.SetActive (false);
			}
		}
	}

	void Start () {
		// CanDie moved to gamecontroller_startup. ALSO: Use this for invinciblity
//		canDie = true;

		hudScriptHolder.Score_LeaveHUD ();

		// Check for coming from menu or not
		if (PlayerData.Instance != null) {
			GetPlayerDataValues ();
			Checkpoint_TargetUpdate ();

			SelectMenuType ();

			// Enable menus for this (Moved inside Select Menu Type)
//			PlayMenuNow ();

			// Moved to PlayMenuNow
//			CurrencyUpdate (intCurrency_Curr);
		} 

		else {
			Checkpoint_TargetUpdate ();

			// Moved this to PlayGameNow
//			objMenuBlackFrontsHolder.SetActive (false);

			if (!isMenuMode) {
				// Values for non player data
				ResetScoreNCurrency ();

				QuickGameplay ();

				/* Moved to Quick Gameplay
				// Only for quick tests
				PlayGameNow ();

				// Disable menus for this
				menuScript.MainMenu_Activator (false);

				gameSpawner.Spawner_Start ();
				*/

				// Moved inside PlayGameNow
//				camController.Cam_Reposition (0);
			} else {
				// Enable menus for this
				PlayMenuNow ();

				// Moved insde StartMneu
//				camController.Cam_Reposition (-40);
			}
		}
	}

	public void QuickGameplay () {
		// Give knife from the restart to player AND Gamecontroller buffer in case player used restart (To avoid no knife bug)
		if (PlayerData.Instance != null) {
			spritePlayerBuffer = PlayerData.Instance.spritePlayerKnifeHolder;
			playerController.Player_SpriteChange (spritePlayerBuffer);
		}

		// Reset curr value for restart bug (After quick restart the currency value is not shown properly due to intCurrHUDBuffer not being set);
		UpdateScoreNCurrency ();

		// Only for quick tests
		PlayGameNow ();

		// Disable menus for this
		menuScript.MainMenu_Activator (false);

		gameSpawner.Spawner_Start ();
	}

	public void PlayGameNow () {
		objMenuBlackFrontsHolder.SetActive (false);
		hudScriptHolder.Score_EnterHUD ();

		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.InGame;

			// AdForced (Play or Restart)
			PlayerData.Instance.intTimesPlayedThisSession++;
			if (PlayerData.Instance.intTimesPlayedThisSession >= intAdForced_HowManyPlays) {
				Tapligh_Script.Instance.ShowAdForcedType ();
				PlayerData.Instance.intTimesPlayedThisSession = 0;
			}

			// FireBase Event: Start or Restart Game
			FirebaseManager.FireBaseEventStartRestartGame ();

			if (intCheckpoint_TotalReached == 0) {
				// FireBase Event: Checkpoint Funnel Start
				FirebaseManager.FireBaseEventStartLevel (GameController.Instance.intCheckpoint_TotalReached.ToString ());
			}
		}

		if (!isMenuMode) {
			particleFadeToVisible.Play ();

			camController.Start_GameCam ();
			playerController.Start_GamePlayer ();
			camController.Cam_Reposition (0);
		} else {
			camController.Start_MenuToGameCam ();
			particleFullscreenBackStarsFastHolder.Play ();
			playerController.Player_MenuToGameAnimate ();
		}
	}

	public void PlayMenuNow () {
		// Moved this to select menu type
//		GameController.Instance.isMenuMode = true;

		menuScript.MainMenu_Activator (true);
		menuScript.Start_MainMenu ();
		camController.Cam_Reposition (-50);
	}

	public void PlayGameDelayed () {
		StartCoroutine (PlayGame_Delay ());
	}

	IEnumerator PlayGame_Delay () {
//		particleFadeToBlackFAST.Play ();

		// Change player art
		playerController.Player_SpriteChange (spritePlayerBuffer);
		playerController.Player_SpriteLayer_FrontUI ();

		// To start spawning
//		gameSpawner.Spawner_Start ();
		PlayGameNow ();

		yield return new WaitForSeconds (0.3F);
//		particleFadeToVisible.Play ();

		gameSpawner.Spawner_Start ();
//		PlayGameNow ();

		yield return null;
		particleFadeToBlackFAST.Clear ();
	}

	public void GetPlayerDataValues () {
		intCurrency_Curr = PlayerData.Instance.player_Currency;
		intScore_Highscore = PlayerData.Instance.player_Score_Highscore;
		intCheckpoint_TotalReached = PlayerData.Instance.player_CheckpointTotal;

		// Updated the target
		intCheckpoint_TotalTarget = intCheckpoint_TotalReached;
//		if (intCheckpoint_TotalReached == 0) {
//			intCheckpoint_TotalTarget = 0;
//		} else {
//			intCheckpoint_TotalTarget = intCheckpoint_TotalReached + 1;
//		}
	}

	public void SendToPlayerDataValues () {
		PlayerData.Instance.player_Currency = intCurrency_Curr;
		PlayerData.Instance.player_Score_Highscore = intScore_Highscore;
		PlayerData.Instance.player_CheckpointTotal = intCheckpoint_TotalReached;
	}

	public void SelectMenuType () {
//		Debug.LogError ("Game Controller Start = " + PlayerData.Instance.gameMenuType);

		switch (PlayerData.Instance.gameMenuType) {
		case PlayerData.GameMenuType.Startup:
			isMenuMode = true;
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.MainMenu;
			PlayMenuNow ();
//			menuScript.MainMenu_Activator (true);

			break;
		case PlayerData.GameMenuType.MainMenu:
			isMenuMode = true;
			PlayMenuNow ();
//			menuScript.MainMenu_Activator (true);

			break;
		case PlayerData.GameMenuType.InGame:
			isMenuMode = false;
			QuickGameplay ();

			break;
		case PlayerData.GameMenuType.Continue:
			break;
		case PlayerData.GameMenuType.GameOver:
			break;
		default:
			Debug.LogError ("Default Menu Type Error");
			break;
		}
	}

	void GameController_Startup () {
		Time.timeScale = 1;
		Application.targetFrameRate = 60;

		objCamViewPort.SetActive (false);
		objMenuBlackFrontsHolder.SetActive (true);

		// Fade in for start (Moved to play options / PlayGameNow for quick restart)
//		particleFadeToVisible.Play ();

		// High score has not happened
		isHighScore = false;

		// Can Die
		canDie = true;

		// No need to turn this to zero
//		ScoreUpdate (0);
//		CurrencyUpdate (0);
//		ResetScoreNCurrency ();
	}

	public void ResetScoreNCurrency () {
		ScoreUpdate (0);
		CurrencyUpdate (0);
	}

	public void UpdateScoreNCurrency () {
		ScoreUpdate (intScore_Curr);
		CurrencyUpdate (intCurrency_Curr);
	}

	void onPlayerAttack () {
		Destroy_Planet ();

		ScoreIncrease (1);
		CheckpointUpdate ();
	}

	public void Destroy_Planet () {
//		StartCoroutine (planetController_Selected.Planet_DestroyMe ());
	}

	public void Spawn_MorePlanets () {
		intSpawnHowMany = Random.Range (intSpawnHowMany_Initial, intSpawnHowMany_Initial + intSpawnHowMany_Step);
		StartCoroutine (gameSpawner.Planet_Spawn_Many (intSpawnHowMany));
	}

	public void ValuesAfterDeath () {
		if (intScore_Curr > intScore_Highscore) {
			intScore_Highscore = intScore_Curr;
			isHighScore = true;
		} else {
			isHighScore = false;
		}

		if (PlayerData.Instance != null) {
			SendToPlayerDataValues ();
			SaveLoad.Save ();

			// Send info to googleplay
			GooglePlayManager.Instance.AddScoreToLeaderboard (GPGSIds.leaderboard_high_score, intScore_Curr);
			GooglePlayManager.Instance.AddScoreToLeaderboard (GPGSIds.leaderboard_highest_checkpoint, intCheckpoint_TotalReached);

			// FireBase Event: Post Score after Game Over
			FirebaseManager.FireBaseEventPostScore (intScore_Curr);
		}
	}

	public void onPlayerCamDeath () {
		Debug.LogError ("Death By Camera");
		Player_Death ();
	}

	public void onPlayerHazardDeath () {
		Debug.LogError ("Death By Hazard");
		Player_Death ();
	}

	public void onPlayerShrinkingDeath () {
		Debug.LogError ("Death By Shrinking Planet");
		Player_Death ();
	}

	public void Player_Death () {
		Debug.Log ("Player Deathy!");
		PlayerController.playerStatus = PlayerController.PlayerStatus.IsDead;

		ValuesAfterDeath ();

		playerController.Player_ParentFree ();
		canDie = false;

		// To make sure continue does not lead to checkpoint
		intCheckpoint_CounterCurr--;
//		if (intCheckpoint_CounterCurr > 0) {
//			intCheckpoint_CounterCurr--;
//		}

		// Disable Player Stuff
		playerController.Player_ColliderDoubleEnabler (false);
		playerController.Player_SpriteEnabler (false);

		if (!wasInstantContinueGiven) {
			if (!isAdVideoAvailable_Reward) {
				if (intContinueCount > 0) {
					InstantContinue_Evaluate (9);
				} else {
					InstantContinue_Evaluate (3);
				}
			} else {
				if (intContinueCount > 0) {
					InstantContinue_Evaluate (6);
				} else {
					InstantContinue_Evaluate (2);
				}
			}

			if (((intCheckpoint_Target - intCheckpoint_CounterCurr) < 10) && !wasInstantContinueGiven) {
				InstantContinue_Evaluate (25);
			}
		}

		if (intInstantContinueCount > 0) {
			intInstantContinueCount--;
			Player_Death_Continue (true);
		} 

		else {
			if (isAdVideoAvailable_Reward && (intContinueCount > 0)) {
				Player_Death_Continue (false);
			} else {
				if (PlayerData.Instance != null) {
					if (PlayerData.Instance.isNoAds && (intContinueCount > 0)) {
						Player_Death_Continue (false);
					} else {
						Player_Death_GameOver ();
					}
				} 

				else {
					Player_Death_GameOver ();
				}
			}
		}
	}

	public void InstantContinue_Evaluate (float floatChance) {
		if (Random.Range (0, 100) <= floatChance) {
			wasInstantContinueGiven = true;
			intInstantContinueCount = 1;
		}
	}

	public void Player_DeathExplosion (Vector3 v3Position) {
		transDeathExplodesParent.position = v3Position;
		transDeathKnifeParent.position = v3Position;
		camController.Camera_DeathKnife ();

		Debug.LogError ("Death explode at = " + v3Position);

		for (int i = 0; i < particleDeathExplodeArr.Length; i++) {
			particleDeathExplodeArr [i].Play ();
		}
	}


	/* Not used
	public void Player_ExplosionLayersUI () {
		ParticleSystemRenderer particleRenderBuffer;

		transDeathExplodesParent.localScale = new Vector3 (13, 13, 13);

		// Need to use get child 1 because the normal one is in camera and is turned on via animation of Camer_DeathKnife
		transDeathKnifeParent.GetChild(0).gameObject.SetActive (false);
//		transDeathKnifeParent.localScale = new Vector3 (6, 6, 6);
//		transDeathKnifeParent.GetComponentInChildren <SpriteRenderer> ().sortingLayerName = "UI";

		for (int i = 0; i < particleDeathExplodeArr.Length; i++) {
			particleRenderBuffer = particleDeathExplodeArr [i].GetComponent<ParticleSystemRenderer> ();
			particleRenderBuffer.sortingLayerName = "UI";
		}
	}
	*/

	public void Player_Death_Continue (bool isInstant) {
		// Instead of stopping time, use slow follow stop
		camController.camMove_SlowFollow = false;

		// This one is for death that goes to continue
//		Time.timeScale = 0;

		if (isInstant) {
			Debug.LogError ("Death CONTINUE - INSTANT");

			StartCoroutine (Continue_Revive (true));
		} else {
			// Only show red for no-instant
			particleRedAlarmMini.Play ();

			Debug.LogError ("Death CONTINUE - NORMAL");

			StartCoroutine (ContinueRoutine_NonInstant ());
		}
	}

	public void Resurrect_ParticlesReviveReset (Vector3 v3_Posit) {
		particleResurrRaysArr [0].transform.parent.localPosition = new Vector3 (v3_Posit.x, v3_Posit.y - 1, v3_Posit.z);
		for (int i = 0; i < particleResurrRaysArr.Length; i++) {
			particleResurrRaysArr [i].Play ();
		}
	}

	public IEnumerator Continue_Revive (bool isBonusContinue) {
//		particleRedAlarm.Play ();
//		particleFadeToBlack.Play ();

		particleBonusContinQuickBlue.Play ();

		// To make sure two planets are NOT spawned on top of each other
		if (planetController_Selected != null) {
			if (!planetController_Selected.wasDestroyed) {
				planetController_Selected.Planet_Destroy (playerController.transform.eulerAngles.z);
			}
		}

		Debug.LogError ("Position of new continue planet = " + v3_planetRevivePosit);

		Resurrect_ParticlesReviveReset (v3_planetRevivePosit);

		// First Bonus Check in this method (Particles)
		if (isBonusContinue) {
			menuScript.BonusContinue_Particles ();
		} else {
			menuScript.ContinueResurrect_Particles ();
		}

		yield return new WaitForSecondsRealtime (0.1F);
		// Second Bonus Check in this method (Anim)
		if (isBonusContinue) {
			camController.Camera_BonusContin ();
		}

		yield return new WaitForSecondsRealtime (0.1F);
		gameSpawner.Planet_SpawnContinue (v3_planetRevivePosit);
//		gameSpawner.Planet_SpawnContinue (planetController_Selected.transform.position);

		camController.AfterPlayerRevive ();

		yield return new WaitForSecondsRealtime (2.2F);
		playerController.Player_ReviveReset (v3_planetRevivePosit);
//		playerController.Player_ReviveReset (planetController_Selected.transform.position);

		yield return new WaitForSecondsRealtime (0.2F);

//		camController.AfterPlayerRevive ();

//		particleFadeToVisible.Play ();

		yield return null;
		particleFadeToBlack.Clear ();
//		Time.timeScale = 1;

		intContinueCount--;
//		wasContinueUsed = true;

		yield return null;
		// Disable Player Stuff
		playerController.Player_ColliderDoubleEnabler (true);
		playerController.Player_SpriteEnabler (true);

		// To prevent the player from dying too soon
		yield return new WaitForSecondsRealtime (1F);
		canDie = true;
	}

	public void Player_Death_GameOver () {
		particleRedAlarm.Play ();

		// Instead of stopping time, use slow follow stop
		camController.camMove_SlowFollow = false;

		// Normal menu after death
		if (!isQuickRestart) {

			// Either way go to game over
			StartCoroutine (GameOverRoutine ());
		} 

		// Quick restart after death
		else {
			StartCoroutine (RestartGameRoutine ());
		}
	}

	IEnumerator ContinueRoutine_NonInstant () {
		yield return new WaitForSeconds (0.5F);

		menuScript.MainMenu_Activator (true);
		menuScript.Start_Continue ();
	}

	IEnumerator GameOverRoutine () {
		yield return new WaitForSeconds (0.5F);

		menuScript.MainMenu_Activator (true);
		menuScript.Start_GameOver ();
	}

	public void RestartGame () {
		StartCoroutine (RestartGameRoutine ());
	}

	IEnumerator RestartGameRoutine () {
//		particleFadeToBlack.Play ();
		particleFadeToBlackFAST.Play ();
		yield return new WaitForSecondsRealtime (0.5F);

		objDeathBlackHolder.SetActive (true);
		yield return new WaitForSecondsRealtime (0.1F);

		RestartGameNOW ();
	}

	public void RestartGameNOW () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void ScoreIncrease (int whatScore) {
		intScore_Curr += whatScore;
		hudScriptHolder.Score_Update (intScore_Curr);
	}

	public void ScoreUpdate (int whatScore) {
		intScore_Curr = whatScore;
		hudScriptHolder.Score_Update (intScore_Curr);
	}

	public void CurrencyIncrease_YesInstantHUD (int whatCurrency) {
		intCurrency_Curr += whatCurrency;
		hudScriptHolder.Currency_Update_YesInstant (intCurrency_Curr);

		if (PlayerData.Instance != null) {
			PlayerData.Instance.player_Currency += whatCurrency;
		}
	}

	public void CurrencyIncrease_NoInstantHUD (int whatCurrency) {
		intCurrency_Curr += whatCurrency;
//		hudScriptHolder.Currency_Update_NoInstant (intCurrency_Curr);

		if (PlayerData.Instance != null) {
			PlayerData.Instance.player_Currency += whatCurrency;
		}
	}

	public void CurrencyUpdate (int whatCurrency) {
		intCurrency_Curr = whatCurrency;
		hudScriptHolder.Currency_Update_YesInstant (intCurrency_Curr);

		if (PlayerData.Instance != null) {
			PlayerData.Instance.player_Currency = whatCurrency;
		}
	}

	public void CurrencyParticleMoon (int whatScore, Vector3 v3_Posit) {
//		v3_Posit.z = 0;
//		particleCurrencyMoonHolder.transform.position = v3_Posit;
//
//		particleCurrencyMoonHolder.Emit (whatScore);

		intCurrMoverCounter++;
		if (whatScore == 1) {
			GameObject newCurMover = Instantiate (objCurrMoverSource_Single, transParentCurrParticle);
			newCurMover.name += intCurrMoverCounter.ToString ();
			newCurMover.transform.position = v3_Posit;
		} else {
			GameObject newCurMover = Instantiate (objCurrMoverSource_Multi, transParentCurrParticle);
			newCurMover.name += intCurrMoverCounter.ToString ();
			newCurMover.transform.position = v3_Posit;
		}

//		particleCurrencyMoonHolder_NewLoop.transform.SetParent (newCurMover.transform);
//		particleCurrencyMoonHolder_NewLoop.Emit (whatScore);
	}

	public void Checkpoint_TargetUpdate () {
		intCheckpoint_Target = intCheckpoint_TargetsArr [intCheckpoint_TotalTarget];
		intCheckpoint_TotalTarget++;
		intCheckpoint_TotalReached = intCheckpoint_TotalTarget - 1;

		// Increase of checkpoint
		if (intCheckpoint_TotalTarget >= intCheckpoint_TargetsArr.Length) {
			intCheckpoint_Target = intCheckpoint_TargetsArr [intCheckpoint_TargetsArr.Length - 1] + (intCheckpoint_TotalTarget - intCheckpoint_TargetsArr.Length) * 5;
		}

		// Save highest checkpoint
		if (PlayerData.Instance != null) {
			PlayerData.Instance.player_CheckpointTotal = intCheckpoint_TotalReached;
			Debug.Log ("SAVE CHECKPOINT = PlayerData.Instance.player_CheckpointTotal = " + PlayerData.Instance.player_CheckpointTotal);
			SaveLoad.Save ();
		}
	}

	public void CheckpointUpdate () {
		intCheckpoint_CounterCurr++;
		if ((intCheckpoint_Target - intCheckpoint_CounterCurr) <= 0) {
			intCheckpoint_CounterCurr = 0;
			isCheckpointTimeActive = true;

			// Update Target Value
			Checkpoint_TargetUpdate ();
		}

		hudScriptHolder.Checkpoint_RemainUpdate (intCheckpoint_Target - intCheckpoint_CounterCurr);
	}

	public void AfterCheckpoint_ReturnNormal () {
		isCheckpointTimeActive = false;
		playerController.canPlay = true;
	}

	public void SelectedPlanet_Update (PlanetController planetController) {
		planetController_Selected = planetController;
		v3_planetSelectedPosit = planetController.transform.position;

//		Debug.Log ("Selected Planet Update: planetController.transform.position.y = " + planetController.transform.position.y + "\n" + "v3_planetCheckpointPosit.y = " + v3_planetCheckpointPosit.y);

		// Moved to separate method for the spawn on new planet bug
//		if (planetController.transform.position.y > v3_planetCheckpointPosit.y) {
//			v3_planetCheckpointPosit = planetController.transform.position; 
//		}
	}

	public void SelectedPlanet_ContinuefterDestroy (Vector3 v3_NewPlanetPosit) {
		if (v3_NewPlanetPosit.y > v3_planetRevivePosit.y) {
			v3_planetRevivePosit = v3_NewPlanetPosit; 
		}
		Debug.LogWarning ("Selected planet name = " + planetController_Selected.name + "\n at " + v3_planetRevivePosit);
	}

	public void MenuInteraction_Disable () {
//		Time.timeScale = 1;
		menuScript.graphicRaycasterMainMenuHolder.enabled = false;
		objMenuVideoBlocker.SetActive (true);

		StartCoroutine (MenuInteraction_ForcedDisable (8F));
	}

	IEnumerator MenuInteraction_ForcedDisable (float seconds)
	{
		yield return new WaitForSecondsRealtime (seconds);
		MenuInteraction_Enable ();
	}

	public void MenuInteraction_Enable () {
		//		Time.timeScale = 0;
		menuScript.graphicRaycasterMainMenuHolder.enabled = true;
		objMenuVideoBlocker.SetActive (false);
	}

	public IEnumerator TooltipDisablerCheck () {
		yield return new WaitForSeconds (1.8F);

		if (menuScript.tooltipGeneralScript.gameObject.activeInHierarchy) {
			menuScript.tooltipGeneralScript.gameObject.SetActive (false);
		}
	}

	public void Continue_GiverCheck () {
//		if (intInstantContinueCount == 0 && !wasContinueGiven) {
//		} 

//		wasContinueGiven = true;
//		intContinueCount++;

		if (intContinueCount == 0 && !wasContinueGiven) {
			intContinue_GiveCounter++;
			if (intContinue_GiveCounter >= intContinue_AfterThis_Base) {
				if ((intScore_Highscore < intContinue_AfterThis_HighScoreTreshold) || (intScore_Curr >= intContinue_AfterThis_HighScoreTreshold * 1.5F)) {
					intContinue_GiveCounter = 0;

					wasContinueGiven = true;
					intContinueCount++;
				}
			}
		}
	}
}
