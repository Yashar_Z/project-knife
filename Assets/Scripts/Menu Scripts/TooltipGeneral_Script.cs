﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipGeneral_Script : MonoBehaviour {

	public SpriteRenderer spriteRenderTooltipFrame;
	public SpriteRenderer spriteRenderTooltipOutline;

	public Text textTooltipGeneral;

	public Animation animTooltipGeneralHolder;

	public void TooltipGeneral_Setup (string stringText, float floatFrameLength) {
		this.gameObject.SetActive (true);
		animTooltipGeneralHolder.Play ();

		textTooltipGeneral.text = stringText;

		Vector2 v2_FrameSizeBuffer = spriteRenderTooltipFrame.size;
		v2_FrameSizeBuffer.x = floatFrameLength;
		spriteRenderTooltipFrame.size = v2_FrameSizeBuffer;
		spriteRenderTooltipOutline.size = v2_FrameSizeBuffer;
	}

	public void AnimEvent_TooltipAnimEnd () {
		this.gameObject.SetActive (false);
	}
}
