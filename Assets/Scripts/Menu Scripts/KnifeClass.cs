﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeClass : MonoBehaviour {

	public enum KnifeType
	{
		Free,
		Money_Currency,
		Money_Real,
		Vids,
		Checkpoints,
		Other
	}

	public KnifeType knifeType;

	public int intIndexOrder;

//	public int cost_Money_Real;
//	public int cost_Money_Currency;
//	public int cost_Vids;
//	public int cost_Checkpoints;

	public int cost_General;

	public bool isLongForm;
	public bool hasParticle;

	[Header ("")]
	public SpriteRenderer spriteRenderKnife;
//	public ParticleSystem particleKnifeBodyHolder;
	public BoxCollider2D boxColl2D_Holder;
	public GameObject objKnifeLockHolder;

	public Animation animKnifeHolder;

	public override string ToString ()
	{
		return string.Format ("cost_General {0} intIndexOrder {1} ", cost_General, intIndexOrder);
	}

	public void AnimEvent_KnifeVisualEnd () {
		GameController.Instance.menuScript.particleKnifeVisualEndSquare.Play ();
		GameController.Instance.menuScript.Menu_AnimMainPlay (6);
	}
}
