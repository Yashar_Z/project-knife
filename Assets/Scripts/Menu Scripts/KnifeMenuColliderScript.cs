﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeMenuColliderScript : MonoBehaviour {

	public Menu_Script menuScript;
	private KnifeClass knifeClassCenterRef;

	private KnifeClass.KnifeType knifeTypeRef;
	private int intKnifeCostRef;

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MenuKnife") {
			knifeClassCenterRef = other.GetComponent <KnifeClass> ();
			knifeTypeRef = knifeClassCenterRef.knifeType;
			intKnifeCostRef = knifeClassCenterRef.cost_General;

			menuScript.intKnifeIndexMain = knifeClassCenterRef.transform.GetSiblingIndex ();
			menuScript.PlayButtonChange_KnifeSelect (knifeTypeRef, intKnifeCostRef);

//			Debug.Log ("Selected Index " + knifeClassCenterRef.transform.GetSiblingIndex () + " for " + knifeClassCenterRef.name);
//			Debug.Log ("knifeClassCenterRef name = " + knifeClassCenterRef.name + "\n" + knifeClassCenterRef.knifeType);
		}
	} 
}
