﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_Script : MonoBehaviour {

	public KnifeClass.KnifeType knifeTypeMain;

	public enum MenuButtonsType
	{
		MainMenu_Button,
		Continue_Buttons,
		GameOver_Buttons,
		Other
	}

	public MenuButtonsType menuButtonType;

	[Header ("Animation")]
	public AnimationClip[] animClip_MenuArr;
	public Animation animMenuHolder;

	public Animation animMainLogoHolder;

	[Header ("Play Button")]
	public SpriteRenderer spriteRenderPlayButton; 
	public SpriteRenderer spriteRenderPlay_VideoIcon;

	// spriteRenderPlay_CurrencyIcon;
	public GameObject objPlay_CurrencyIcon;

	public Transform transPlayParentHolder;
	public Transform transTextPlayButton;
	public Text textPlayButton;

	public Button buttonPlayHolder;

	// For play button idle
	public Animation animPlayButtonArtHolder;

	// For Parent Press 
	public AnimationClip[] animClip_PlayButtonParentArr;
	public Animation animPlayButtonParentHolder;

	[Header ("Other Button")]
	public SpriteRenderer spriteRenderOtherButton; 
	public SpriteRenderer spriteRenderOther_VideoIcon;

	// spriteRenderOther_CurrencyIcon
	public GameObject objOther_CurrencyIcon;

	public Transform transOtherParentHolder;
	public Transform transTextOtherButton;
	public Text textOtherButton;

	public Button buttonOtherHolder;

	// For play button idle
	public Animation animOtherButtonArtHolder;

	// For Parent Press 
	public Animation animOtherButtonParentHolder;

	[Header ("Main Icon - Settings")]
	public ParticleSystem particleMainIcon_Settings;
	public ParticleSystem particleSettings_HomeButton;

	public ParticleSystem particleToSettingsAndBack;

	[Header ("Main Icon - No Ads")]
	public GameObject objNodAdsIcon_On;
	public GameObject objNodAdsIcon_Off;
	public ParticleSystem particleMainIcon_NoAds;

	[Header ("Main Icon - Leader B")]
	public ParticleSystem particleMainIcon_LeaderB;

	[Header ("Main Icon - Daily Gift")]
	public Button buttonDailyGiftHolder;

	public GameObject objDailyGiftFlashHolder;
	public Animation animDailyGiftFlashHolder;

	public GameObject objDailyGiftParticlesParent;
	public ParticleSystem particleDailyGiftParticle_1;
	public ParticleSystem particleDailyGiftParticle_2;
	public ParticleSystem particleDailyGiftParticle_3;

	public ParticleSystem particleDailyGift_IsReadyNotif;

	public System.TimeSpan timeSpanForDailyGift;

	[Header ("Texts Infos ")]
	public Text textMenu_Checkpoint;
	public Text textMenu_HighScore;

	// Use highscore text for normal score during continue
//	public Text textContinue_ScoreCurr;
	public Text textGameOver_Highscore;
	public Text textGameOver_ScoreCurr;

	[Header ("Menu And Settings And Other Objects")]
	public GameObject objMainMenuHolder;
	public GameObject objSettingsMenuHolder;

	public GameObject objContinueExplosionsParent;

	[Header ("Knife Line Stuff")]
	public KnifeLineController knifeLineController;
	public SpriteRenderer spriteRendContinueKnife;

	public ParticleSystem particleKnifeVidWatched;
	public ParticleSystem particleKnifeVisualEndSquare;
	public ParticleSystem[] particleKnifeBuyArr;

	[Header ("Resurrection Stuff")]
//	public float floatAnimContinueTime;
	public Animation animContinuePlanetMover;

	public GameObject objContinueResurrParent;
	public ParticleSystem[] particleContinueResurrArr;

	public GameObject objBonusContinueParent;
	public ParticleSystem[] particleBonusContinueArr;

	[Header ("")]
	public TooltipGeneral_Script tooltipGeneralScript;

	public Canvas canvasMainMenuHolder;
	public GraphicRaycaster graphicRaycasterMainMenuHolder;

	public ParticleSystem particleMenuStarsOnly;

	public int intCostMain;
	public int intKnifeIndexMain;

	private Vector2 v2_ButtonSizeBuffer;

	void Start () {
		// Setup for start / No ads
		NoAdsSetup ();
		StartCoroutine (DailyGift_RountineChecks ());
	}

	public void Start_MainMenu () {
		// Setup for start / No ads (Also in start above)
		NoAdsSetup ();

		// Disable settings for any start
		objSettingsMenuHolder.SetActive (false);

		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.MainMenu;
		}

		menuButtonType = Menu_Script.MenuButtonsType.MainMenu_Button;

		// HUD Values Update
		GameController.Instance.UpdateScoreNCurrency ();

		Menu_AnimMainPlay (0);

		Menu_MainDisplayValues ();

		// SOUND EFFECTS: Intro Splash To Menu
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.BlendMusic (0);
		}
	}

	public void Start_Continue () {
		menuButtonType = Menu_Script.MenuButtonsType.Continue_Buttons;
		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.Continue;
		}

		spriteRendContinueKnife.sprite = GameController.Instance.spritePlayerBuffer;

		// Continue Buttons
		PlayButtonChange_Continue (new Vector3 (0, -240, 0));
		OtherButtonChange_Continue (new Vector3 (0, -360, 0));
//		PlayButtonChange_Continue (new Vector3 (0, -160, 0));
//		OtherButtonChange_Continue (new Vector3 (0, -271, 0));

		Menu_AnimMainPlay (2);

		Menu_MainDisplayValues ();
		Menu_GameOverDisplayValues ();
	}

	public void Continue_To_Resurrect () {
//		menuButtonType = Menu_Script.MenuButtonsType.Continue_Buttons;
		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.InGame;
		}

		graphicRaycasterMainMenuHolder.enabled = false;

		// Animate Back Menu
		Menu_AnimMainPlay (3);

		StartCoroutine (GameController.Instance.Continue_Revive (false));
	}

	public void ContinueResurrect_Particles () {
		objContinueResurrParent.SetActive (true);
		for (int i = 0; i < particleContinueResurrArr.Length; i++) {
			particleContinueResurrArr [i].Play ();	
		}
	}

	public void BonusContinue_Particles () {
		Debug.LogError ("WTF!");
		objBonusContinueParent.SetActive (true);
		for (int i = 0; i < particleBonusContinueArr.Length; i++) {
			particleBonusContinueArr [i].Play ();	
		}
	}

	public void BonusContinue_ParticlesDisable () {
		objBonusContinueParent.SetActive (false);
	}

	public void Continue_To_GameOvers () {
		objContinueExplosionsParent.SetActive (true);
		Destroy (objContinueExplosionsParent, 2);

		Menu_AnimMainPlay (4);

		// Inside Anim Event for countdown anims script
//		GameOver_AllSetup ();
	}

	// Straight to Game Over
	public void Start_GameOver () {
		menuButtonType = Menu_Script.MenuButtonsType.GameOver_Buttons;
		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.GameOver;
		}

		// Change buttons
		GameOver_AllSetup ();

		Menu_AnimMainPlay (5);

		Menu_MainDisplayValues ();
		Menu_GameOverDisplayValues ();
	}

	void GameOver_AllSetup () {
		// AdForced (Highscore)
		if (GameController.Instance.isHighScore && PlayerData.Instance != null) {
			Tapligh_Script.Instance.ShowAdForcedType ();
			PlayerData.Instance.intTimesPlayedThisSession = 0;
		}

		// Leave Score from HUD
		GameController.Instance.hudScriptHolder.Score_LeaveHUD ();

		if (GameController.Instance.isAdVideoAvailable_Reward) {
//			Debug.Log ("1");
			PlayButtonChange_GameOver (new Vector3 (0, -119.4F, 0), true);
			OtherButtonChange_GameOver (new Vector3 (0, -237F, 0), true);
		} 

		else {
//			Debug.Log ("2");
			PlayButtonChange_GameOver (new Vector3 (0, -195F, 0), true);
			OtherButtonChange_GameOver (new Vector3 (0, 0, 0), false);
		}
	}

	public void Menu_MainDisplayValues () {
		textMenu_HighScore.text = "High Score: " + GameController.Instance.intScore_Highscore;
		textMenu_Checkpoint.text = "Level: " + GameController.Instance.intCheckpoint_TotalReached;
	}

	public void Menu_ContinueDisplayValues () {
		textMenu_HighScore.text = "Score: " + GameController.Instance.intScore_Curr;
		textMenu_Checkpoint.text = "Level: " + GameController.Instance.intCheckpoint_TotalReached;
	}

	public void Menu_GameOverDisplayValues () {
		textGameOver_Highscore.text = "High Score: " + GameController.Instance.intScore_Highscore;
		textGameOver_ScoreCurr.text = GameController.Instance.intScore_Curr.ToString ();	
	}

	public void PlayButtonChange_KnifeSelect (KnifeClass.KnifeType knifeType, int intCost) {
		knifeTypeMain = knifeType;
		intCostMain = intCost;
		switch (knifeTypeMain) {
		case KnifeClass.KnifeType.Free:
			textPlayButton.text = "PLAY";
			textPlayButton.alignment = TextAnchor.MiddleCenter;
			transTextPlayButton.localPosition = Vector3.zero;

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 3.46F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [0];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [0];

//			spriteRenderPlay_CurrencyIcon.enabled = false;
			objPlay_CurrencyIcon.SetActive (false);
			spriteRenderPlay_VideoIcon.enabled = false;

			PlayButtonIdle (true);
			break;

		case KnifeClass.KnifeType.Money_Currency:
			textPlayButton.text = intCostMain.ToString ();
			textPlayButton.alignment = TextAnchor.MiddleLeft;
			transTextPlayButton.localPosition = new Vector3 (30, 0, 0);

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 4.5F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [1];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [1];

//			spriteRenderPlay_CurrencyIcon.enabled = true;
			objPlay_CurrencyIcon.SetActive (true);
			spriteRenderPlay_VideoIcon.enabled = false;

			PlayButtonIdle (false);
			break;

		case KnifeClass.KnifeType.Money_Real:
			textPlayButton.alignment = TextAnchor.MiddleCenter;
			textPlayButton.text = intCostMain.ToString () + " Tomans";
			transTextPlayButton.localPosition = Vector3.zero;

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 5.5F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [2];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [2];

//			spriteRenderPlay_CurrencyIcon.enabled = false;
			objPlay_CurrencyIcon.SetActive (false);
			spriteRenderPlay_VideoIcon.enabled = false;

			PlayButtonIdle (false);
			break;

		case KnifeClass.KnifeType.Vids:
			textPlayButton.alignment = TextAnchor.MiddleRight;

			if (PlayerData.Instance != null) {
				int intVidRemainDisplay = intCostMain - PlayerData.Instance.player_VidsPerKnifeArr [intKnifeIndexMain];
				if (intVidRemainDisplay <= 0) {
					textPlayButton.text = "CLAIM";
				} else {
					textPlayButton.text = intVidRemainDisplay.ToString () + " Left";
				}
			} else {
				textPlayButton.text = intCostMain.ToString () + " Left";
			}

			// Uses above ifs
//			textPlayButton.text = intCostMain.ToString () + " Left";
			transTextPlayButton.localPosition = new Vector3 (-40, 0, 0);

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [3];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [3];

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 4.2F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

//			spriteRenderPlay_CurrencyIcon.enabled = false;
			objPlay_CurrencyIcon.SetActive (false);
			spriteRenderPlay_VideoIcon.enabled = true;

			PlayButtonIdle (false);
			break;

		case KnifeClass.KnifeType.Checkpoints:
			textPlayButton.alignment = TextAnchor.MiddleCenter;

			if (PlayerData.Instance != null) {
				int intCheckpointDiff = intCostMain - PlayerData.Instance.player_CheckpointTotal;
				if (intCheckpointDiff <= 0) {
					textPlayButton.text = "CLAIM";
				} else {
					textPlayButton.text = "LEVEL " + intCostMain.ToString ();
				}
			} else {
				textPlayButton.text = "LEVEL " + intCostMain.ToString ();
			}
				
			transTextPlayButton.localPosition = Vector3.zero;

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 4.5F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [4];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [4];

//			spriteRenderPlay_CurrencyIcon.enabled = false;
			objPlay_CurrencyIcon.SetActive (false);
			spriteRenderPlay_VideoIcon.enabled = false;

			PlayButtonIdle (false);
			break;

		default:
			break;
		}
	}

	public void Button_PressSound () {
		// SOUND EFFECTS: Button
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.GenericTap.Play ();
		}
	}

	public void Pressed_PlayButton () {
		switch (menuButtonType) {
		case MenuButtonsType.MainMenu_Button:
			PlayButtonPress_MainMenu ();
			Debug.Log ("Play Menu Pressed - Main Menu");
			break;
		case MenuButtonsType.Continue_Buttons:
			PlayButtonPress_Continue ();
			Debug.Log ("Play Menu Pressed - Continue");
			break;
		case MenuButtonsType.GameOver_Buttons:
//			PlayButtonPress_GameOver ();
			PlayButtonPress_QuickRestart ();
			Debug.Log ("Play Menu Pressed - Game Over");
			break;
		default:
			Debug.LogError ("Default Error - Play Button");
			break;
		}

		Button_PressSound ();
	}

	public void Pressed_OtherButton () {
		switch (menuButtonType) {
		case MenuButtonsType.MainMenu_Button:
			Debug.Log ("Other Menu Pressed - Main Menu");
			break;
		case MenuButtonsType.Continue_Buttons:
			OtherSetButton_GoContinue ();
			Debug.Log ("Other Menu Pressed - Continue");
			break;
		case MenuButtonsType.GameOver_Buttons:
			OtherSetButton_GameOverVidReward ();
			Debug.Log ("Other Menu Pressed - Game Over");
			break;
		default:
			Debug.LogError ("Default Error - Other Button");
			break;
		}

		Button_PressSound ();
	}

	public void Pressed_HomeButton () {
		buttonPlayHolder.interactable = false;
		buttonOtherHolder.interactable = false;

		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.MainMenu;
			SaveLoad.Save ();
		}

		GameController.Instance.RestartGame ();

		Button_PressSound ();
	}
		
	// Pressed for bottom buttons
	public void Pressed_Settings () {
		StartCoroutine (SettingsMainParticleDelay ());
		Menus_MainToSettings ();

		Button_PressSound ();
	}

	IEnumerator SettingsMainParticleDelay () {
		Transform transParticleParentBuffer = particleMainIcon_Settings.transform.parent;
		particleMainIcon_Settings.transform.SetParent (transform);
		particleMainIcon_Settings.Play ();
		yield return new WaitForSeconds (0.5F);
		particleMainIcon_Settings.transform.SetParent (transParticleParentBuffer);
	}

	IEnumerator SettingsHomeParticleDelay () {
		Transform transParticleParentBuffer = particleSettings_HomeButton.transform.parent;
		particleSettings_HomeButton.transform.SetParent (transform);
		particleSettings_HomeButton.Play ();
		yield return new WaitForSeconds (0.5F);
		particleSettings_HomeButton.transform.SetParent (transParticleParentBuffer);
	}

	public void SettingsHomeParticle_Controller (bool isFast) {
		if (isFast) {
			particleSettings_HomeButton.Play ();
		} else {
			StartCoroutine (SettingsHomeParticleDelay ());
		}
	}

	public void Menus_MainToSettings () {
		StartCoroutine (ToSettingsAndBack ());
		objMainMenuHolder.SetActive (false);
		objSettingsMenuHolder.SetActive (true);
	}

	public void Menus_SettingsToMain () {
		StartCoroutine (ToSettingsAndBack ());
		objMainMenuHolder.SetActive (true);
		objSettingsMenuHolder.SetActive (false);
	}

	public IEnumerator ToSettingsAndBack () {
		particleToSettingsAndBack.transform.parent.gameObject.SetActive (true);
		particleToSettingsAndBack.Play ();

		yield return new WaitForSeconds (0.7F);
		particleToSettingsAndBack.transform.parent.gameObject.SetActive (false);
	}

	public void Pressed_NoAds () {

		particleMainIcon_NoAds.Play ();
		if (objNodAdsIcon_On.activeInHierarchy && PlayerData.Instance != null) {
			IAPManager.PurchaseNoAds ();
		} else {
			NoAds_Failed ();
		}

		Button_PressSound ();
	}

	public void NoAds_Success () {
		PlayerData.Instance.isNoAds = true;
		SaveLoad.Save ();
		NoAdsSetup ();
	}

	public void NoAds_Failed () {
		tooltipGeneralScript.TooltipGeneral_Setup ("Purchase Failed.", 5.4F);
	}

	public void NoAdsSetup () {
		if (PlayerData.Instance != null) {
			objNodAdsIcon_On.SetActive (!PlayerData.Instance.isNoAds);
			objNodAdsIcon_Off.SetActive (PlayerData.Instance.isNoAds);
		}
	}

	public void Pressed_LeaderB () {
		particleMainIcon_LeaderB.Play ();
		GooglePlayManager.Instance.ShowLeaderboardsUI ();

		Button_PressSound ();
	}

	public void Pressed_DailyGift () {
		DailyGift_Check ();

		if (GameController.Instance.isDailyGiftAvailable) {
			DailyGift_GiveReward ();

			if (PlayerData.Instance != null) {
				PlayerData.Instance.timeLastDailyGift = System.DateTime.Now;
				SaveLoad.Save ();

				// FireBase Event: Daily Reward
				FirebaseManager.FireBaseEventDailyGift ();
			}
		} else {
			tooltipGeneralScript.TooltipGeneral_Setup ("Gift Available After " + Mathf.Abs(timeSpanForDailyGift.Hours).ToString () + ":" + Mathf.Abs(timeSpanForDailyGift.Minutes).ToString () , 8F);
		}

		Button_PressSound ();
	}

	public IEnumerator DailyGift_RountineChecks () {
		if (PlayerData.Instance != null) {
			if (!GameController.Instance.isDailyGiftAvailable) {
				DailyGift_Check ();

				yield return new WaitForSeconds (10);
				StartCoroutine (DailyGift_RountineChecks ());
			} else {
				yield return null;
			}
		} else {
			if (GameController.Instance.isDailyGiftAvailable) {
				particleDailyGift_IsReadyNotif.Play ();
			}
		}
	}

	public void DailyGift_Check () {
		timeSpanForDailyGift = System.DateTime.Now - (PlayerData.Instance.timeLastDailyGift.AddHours (GameController.Instance.intDailyGift_TimeForEach));
//		Debug.LogWarning ("timeSpanBuffer = " + timeSpanForDailyGift.TotalHours);

		if (timeSpanForDailyGift.TotalHours > 0) {
			GameController.Instance.isDailyGiftAvailable = true;
			particleDailyGift_IsReadyNotif.Play ();
		} else {
			GameController.Instance.isDailyGiftAvailable = false;
			particleDailyGift_IsReadyNotif.Stop ();
			particleDailyGift_IsReadyNotif.Clear ();
		}
	}

	public void PlayButtonPress_MainMenu () {
		switch (knifeTypeMain) {
		case KnifeClass.KnifeType.Free:
			PlaySetButton_Free ();
			break;
		case KnifeClass.KnifeType.Money_Currency:
			PlaySetButton_Money_Currency ();
			break;
		case KnifeClass.KnifeType.Money_Real:
			PlaySetButton_Money_Real ();
			break;
		case KnifeClass.KnifeType.Vids:
			PlaySetButton_Vids ();
			break;
		case KnifeClass.KnifeType.Checkpoints:
			PlaySetButton_Checkpoints ();
			break;
		case KnifeClass.KnifeType.Other:
			break;
		default:
			Debug.LogError ("Default Error Menu");
			break;
		}
	}

	public void PlayButtonPress_Continue () {
		// Should be similar to continue view
		if (GameController.Instance.isAdVideoAvailable_Reward) {
			if (PlayerData.Instance != null) {
                Tapligh_Script.Instance.OnTaplighVerifiedSuccessful += onTaplighVerifiedSuccessful_AfterContinue;
                Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful += onTaplighVerifiedUnSuccessful_AfterContinue;
                Tapligh_Script.Instance.ShowAdRewardType ();

				GameController.Instance.MenuInteraction_Disable ();

				// Continue anim speed freeze to avoid game over
//				floatAnimContinueTime = animContinuePlanetMover [animContinuePlanetMover.clip.name].time;
				animContinuePlanetMover [animContinuePlanetMover.clip.name].speed = 0;
			}
		} 

		else {
			Debug.Log ("No Vid Available For Continue");
			tooltipGeneralScript.TooltipGeneral_Setup ("No Video Available...", 6.1F);
		}
	}

	public void PlaySetButton_Free () {
		StartCoroutine (FreeButtonToPlay ());

		// Remember the position of last knife played
		if (PlayerData.Instance != null) {
			PlayerData.Instance.floatLastKnifePosit = knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition;
		}
	}

	IEnumerator FreeButtonToPlay () {
		graphicRaycasterMainMenuHolder.enabled = false;

		// Animate Back Menu
		Menu_AnimMainPlay (1);

		// Disabled selected knife's art on KnifeLine
		knifeLineController.knifeClassArr[intKnifeIndexMain].gameObject.SetActive (false);

		// Give knife art to both Gamecontroller AND PlayerData
		GameController.Instance.spritePlayerBuffer = knifeLineController.knifeClassArr[intKnifeIndexMain].spriteRenderKnife.sprite;
		if (PlayerData.Instance != null) {
			PlayerData.Instance.spritePlayerKnifeHolder = GameController.Instance.spritePlayerBuffer;
		}

		// Start Play
		GameController.Instance.PlayGameDelayed (); 

		// SOUND EFFECTS: To Menu To Ingame
		if (SoundManager.Instance != null)
		{
			SoundManager.Instance.BlendMusic (1);
		}

//		yield return new WaitForSeconds (0.3F);
		yield return new WaitForSeconds (2.5F);

		// Moved before delay (So can't use Activator)
		MainMenu_Activator (false);
//		graphicRaycasterMainMenuHolder.enabled = false;
//		canvasMainMenuHolder.enabled = false;
//		objMainMenuHolder.SetActive (false);
	}

	public void PlaySetButton_Money_Currency () {
		if (PlayerData.Instance != null) {
			if (PlayerData.Instance.player_Currency >= intCostMain) {
				GameController.Instance.CurrencyIncrease_YesInstantHUD (-intCostMain);
				BuyKnifeNow_Visual ();
				BuyKnifeNow_Save ();
			} else {
				GameController.Instance.hudScriptHolder.CurrencyAnimate_NotEnough ();
			}
		} 

		// Not from menu
		else {
			if (GameController.Instance.intCurrency_Curr >= intCostMain) {
				GameController.Instance.intCurrency_Curr -= intCostMain;
				BuyKnifeNow_Visual ();
			} else {
				GameController.Instance.hudScriptHolder.CurrencyAnimate_NotEnough ();
			}
		}
	}

	public void PlaySetButton_Money_Real () {
		if (PlayerData.Instance != null) {
			switch (intKnifeIndexMain) {
			case 1:
				IAPManager.PurchaseGoldenDagger ();
				break;
			case 17:
				IAPManager.PurchaseGoldenElegantDagger ();
				break;
			case 19:
				IAPManager.PurchaseGoldenDragonDagger ();
				break;
			case 21:
				IAPManager.PurchaseGoldenCurvedDagger ();
				break;
			case 24:
				IAPManager.PurchaseGoldenDartDagger ();
				break;
			case 26:
				IAPManager.PurchaseGoldenArabicSword ();
				break;
			default:
				Debug.LogError ("Default Error");
				break;
			}
//			IAPManager.Purchase ();
		} else {
			NoAds_Failed ();
		}
	}

	public void BuyKnife_RealMoneySuccess () {
		BuyKnifeNow_Visual ();
		BuyKnifeNow_Save ();
	}

	public void PlaySetButton_Vids () {
		if (textPlayButton.text == "CLAIM") {
			BuyKnifeNow_Visual ();
			BuyKnifeNow_Save ();
		} 

		else {
			// Should be similar to continue view
			if (GameController.Instance.isAdVideoAvailable_Reward) {
				if (PlayerData.Instance != null) {
                    Tapligh_Script.Instance.OnTaplighVerifiedSuccessful += onTaplighVerifiedSuccessful_AfterKniveVid;
                    Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful += onTaplighVerifiedUnSuccessful_AfterKniveVid;

                    Tapligh_Script.Instance.ShowAdRewardType ();

					GameController.Instance.MenuInteraction_Disable ();
				}
			} 

			else {
				tooltipGeneralScript.TooltipGeneral_Setup ("No Video Available...", 6.1F);
			}
		}
	}

    void onTaplighVerifiedSuccessful_AfterKniveVid()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterKniveVid;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterKniveVid;

        GameController.Instance.MenuInteraction_Enable();

        // FireBase Event: Video Reward (AdReward)
        FirebaseManager.FireBaseEventVideoAd("After Knife Video");

        PlayerData.Instance.player_VidsWatched++;
		PlayerData.Instance.player_VidsPerKnifeArr [intKnifeIndexMain]++;

        SaveLoad.Save();

        PlayButtonChange_KnifeSelect(knifeTypeMain, intCostMain);
		WatchedKnifeVid_Visual ();
    }

    void onTaplighVerifiedUnSuccessful_AfterKniveVid()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterKniveVid;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterKniveVid;

        GameController.Instance.MenuInteraction_Enable();

        tooltipGeneralScript.TooltipGeneral_Setup("Failed To Load Video...", 7.1F);
    }

    void onTaplighVerifiedSuccessful_AfterContinue()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterContinue;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterContinue;

		GameController.Instance.MenuInteraction_Enable();

		Continue_To_Resurrect();
		animContinuePlanetMover [animContinuePlanetMover.clip.name].speed = 1;

		SaveLoad.Save();

        // FireBase Event: Continue was successfully used
        FirebaseManager.FireBaseEventContinueUsed();

        // FireBase Event: Video Reward (AdReward)
        FirebaseManager.FireBaseEventVideoAd("After Continue Video");
    }

    void onTaplighVerifiedUnSuccessful_AfterContinue()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterContinue;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterContinue;

		animContinuePlanetMover [animContinuePlanetMover.clip.name].speed = 1;

        GameController.Instance.MenuInteraction_Enable();
		tooltipGeneralScript.TooltipGeneral_Setup("Failed To Load Video...", 7.1F);
    }

    void onTaplighVerifiedSuccessful_AfterGameOverVidReward()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterGameOverVidReward;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterGameOverVidReward;

        GameController.Instance.MenuInteraction_Enable();

        GameOver_GiveVidReward();
        SaveLoad.Save();

        // FireBase Event: Video Reward (AdReward)
        FirebaseManager.FireBaseEventVideoAd("After Game Over");
    }
    void onTaplighVerifiedUnSuccessful_AfterGameOverVidReward()
    {
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful_AfterGameOverVidReward;
        Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful -= onTaplighVerifiedUnSuccessful_AfterGameOverVidReward;

        GameController.Instance.MenuInteraction_Enable();

		tooltipGeneralScript.TooltipGeneral_Setup("Failed To Load Video...", 7.1F);
    }

    public void PlaySetButton_Checkpoints () {

		if (textPlayButton.text == "CLAIM") {
			BuyKnifeNow_Visual ();
			BuyKnifeNow_Save ();
		} 

		else {
			tooltipGeneralScript.TooltipGeneral_Setup ("Need To Reach Level " + intCostMain.ToString (), 7.1F);
		}

		// Old method here
//		Continue_GiveVidContinue ();
	}

	public void Continue_GiveVidContinue () {
		PlayButtonPress_QuickRestart ();
	}

	public void PlayButtonPress_QuickRestart () {
		animPlayButtonParentHolder.Stop ();
		animPlayButtonParentHolder.clip = animClip_PlayButtonParentArr [0];
		animPlayButtonParentHolder.Play ();

		buttonPlayHolder.interactable = false;
		buttonOtherHolder.interactable = false;

		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.InGame;
			SaveLoad.Save ();
		}

		GameController.Instance.RestartGame ();
	}

	public void OtherSetButton_GameOverVidReward () {
		if (GameController.Instance.isAdVideoAvailable_Reward) {
			if (PlayerData.Instance != null) {
                Tapligh_Script.Instance.OnTaplighVerifiedSuccessful += onTaplighVerifiedSuccessful_AfterGameOverVidReward;
                Tapligh_Script.Instance.OnTaplighVerifiedUnSuccessful += onTaplighVerifiedUnSuccessful_AfterGameOverVidReward;
                Tapligh_Script.Instance.ShowAdRewardType ();

				GameController.Instance.MenuInteraction_Disable ();
			}
		} 

		else {
			Debug.Log ("No Vid Available For GameOver Reward");
			tooltipGeneralScript.TooltipGeneral_Setup ("No Video Available...", 6.1F);
		}
	}

	public void GameOver_GiveVidReward () {
		animOtherButtonParentHolder.Play ();
		buttonOtherHolder.interactable = false;

		StartCoroutine (BigCurrencyReward (GameController.Instance.intGameOver_VidRewardAmount));

		// Move down the play button
//		animPlayButtonParentHolder.Stop ();

		animPlayButtonParentHolder.clip = animClip_PlayButtonParentArr [1];
		animPlayButtonParentHolder.Play ();
	}

	public void DailyGift_GiveReward () {
		particleDailyGiftParticle_1.Play ();
		particleDailyGiftParticle_2.Play ();
		particleDailyGiftParticle_3.Play ();

		particleDailyGift_IsReadyNotif.Stop ();
		particleDailyGift_IsReadyNotif.Clear ();

		GameController.Instance.particleWhiteFlash_Quick.Play ();

		DailyGift_Activator (false);
		StartCoroutine (BigCurrencyReward (GameController.Instance.intDailyGift_RewardAmount));

		// SOUND EFFECT - Daily Gift Unique Sound (Empty)
	}

	public IEnumerator BigCurrencyReward (int amount) {
		// Play gather particle
		GameController.Instance.hudScriptHolder.particleCurrencyManyGatherVid.Play ();

		yield return new WaitForSeconds (0.82F);
		GameController.Instance.CurrencyIncrease_YesInstantHUD (2);

		yield return new WaitForSeconds (0.2F);
		GameController.Instance.CurrencyIncrease_YesInstantHUD (4);

		yield return new WaitForSeconds (0.1F);
		GameController.Instance.CurrencyIncrease_YesInstantHUD (3);

		yield return new WaitForSeconds (0.2F);
		GameController.Instance.CurrencyIncrease_YesInstantHUD (4);

		yield return new WaitForSeconds (0.1F);
		GameController.Instance.CurrencyIncrease_YesInstantHUD (3);

		GameController.Instance.CurrencyIncrease_YesInstantHUD (amount - 15);
	}

	public void OtherSetButton_GoContinue () {
		Continue_To_GameOvers ();
	}

	public void BuyKnifeNow_Visual () {
		StartCoroutine (BuyKnife_Particles ());
		GameController.Instance.CurrencyIncrease_YesInstantHUD (0);

		// Animate the knife itself
		knifeLineController.KnifeBuyItselfAnimate (intKnifeIndexMain);

		knifeLineController.KnifeChangeToFree (intKnifeIndexMain);
		PlayButtonChange_KnifeSelect (KnifeClass.KnifeType.Free, 0);

		// FireBase Event: Knife buy / unlocked
		FirebaseManager.FireBaseEventKnifeObtained (intKnifeIndexMain);

		// Disable interaction during knife buy animate
		StartCoroutine (BuyKnifeVisual_InteractiveRoutine ());

		// SOUND EFFECTS - Knife Unlock / Buy Sound (Empty)
	}

	IEnumerator BuyKnifeVisual_InteractiveRoutine () {
		MainMenu_OnlyGraphicRayActivator (false);
		yield return new WaitForSeconds (1.5F);
		MainMenu_OnlyGraphicRayActivator (true);
	}

	public void WatchedKnifeVid_Visual () {
		particleKnifeVidWatched.Play ();
	}

	IEnumerator BuyKnife_Particles () {
		bool isPlayingBuyParticles = true;
		particleKnifeBuyArr [0].transform.parent.gameObject.SetActive (true);
		for (int i = 0; i < particleKnifeBuyArr.Length; i++) {
			particleKnifeBuyArr [i].Play ();
		}

		yield return new WaitForSeconds (0.5F);
		isPlayingBuyParticles = false;

		yield return new WaitForSeconds (1.5F);
		if (!isPlayingBuyParticles) {
			particleKnifeBuyArr [0].transform.parent.gameObject.SetActive (false);
		}
	}

	public void BuyKnifeNow_Save () {
		PlayerData.Instance.hasKnifeArr [intKnifeIndexMain] = true;
		SaveLoad.Save ();
	}

	public void PlayButtonChange_Continue (Vector3 v3_Posit) {
		transPlayParentHolder.localPosition = v3_Posit;

		textPlayButton.text = "CONTINUE";
		textPlayButton.alignment = TextAnchor.MiddleCenter;

		if (!PlayerData.Instance.isNoAds) {
			transTextPlayButton.localPosition = new Vector3 (25, 0, 0);
			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 4.7F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;
		} 
		else {
			transTextPlayButton.localPosition = Vector3.zero;
			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 4.1F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;
		}

		textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [0];
		spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [0];

//		spriteRenderPlay_CurrencyIcon.enabled = false;
		objPlay_CurrencyIcon.SetActive (false);

		if (!PlayerData.Instance.isNoAds) {
			spriteRenderPlay_VideoIcon.enabled = true;
			spriteRenderPlay_VideoIcon.transform.localPosition = new Vector3 (-82, -1, 0);
		} else {
			spriteRenderPlay_VideoIcon.enabled = false;
			spriteRenderPlay_VideoIcon.transform.localPosition = new Vector3 (-82, -1, 0);
		}
	}

	public void OtherButtonChange_Continue (Vector3 v3_Posit) {
		transOtherParentHolder.localPosition = v3_Posit;

		textOtherButton.text = "GIVE UP!";
		textOtherButton.alignment = TextAnchor.MiddleCenter;
		transTextOtherButton.localPosition = new Vector3 (10, 0, 0);

		v2_ButtonSizeBuffer = spriteRenderOtherButton.size;
		v2_ButtonSizeBuffer.x = 4.5F;
		spriteRenderOtherButton.size = v2_ButtonSizeBuffer;

		textOtherButton.color = GameController.Instance.spriteController.colorArrMenu_OtherTextColor [0];
		spriteRenderOtherButton.color = GameController.Instance.spriteController.colorArrMenu_OtherButton [0];

//		spriteRenderOther_CurrencyIcon.enabled = false;
		objOther_CurrencyIcon.SetActive (false);
		spriteRenderOther_VideoIcon.enabled = false;
	}

	public void PlayButtonChange_GameOver (Vector3 v3_Posit, bool isActive) {
		transPlayParentHolder.gameObject.SetActive (isActive);
		transPlayParentHolder.localPosition = v3_Posit;
		if (isActive) {
			textPlayButton.text = "RESTART";
			textPlayButton.alignment = TextAnchor.MiddleCenter;
			transTextPlayButton.localPosition = Vector3.zero;

			v2_ButtonSizeBuffer = spriteRenderPlayButton.size;
			v2_ButtonSizeBuffer.x = 3.9F;
			spriteRenderPlayButton.size = v2_ButtonSizeBuffer;

			textPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayTextColor [0];
			spriteRenderPlayButton.color = GameController.Instance.spriteController.colorArrMenu_PlayButton [0];

//			spriteRenderPlay_CurrencyIcon.enabled = false;
			objPlay_CurrencyIcon.SetActive (false);
			spriteRenderPlay_VideoIcon.enabled = false;

			PlayButtonIdle (false);
		}
	}

	public void OtherButtonChange_GameOver (Vector3 v3_Posit, bool isActive) {
		transOtherParentHolder.gameObject.SetActive (isActive);
		transOtherParentHolder.localPosition = v3_Posit;
		Debug.LogWarning ("transOtherParentHolder Button Active = " + transOtherParentHolder.gameObject.activeInHierarchy + "  IsActive = " + isActive);

		if (isActive) {
			textOtherButton.text = "+" + GameController.Instance.intGameOver_VidRewardAmount;
			textOtherButton.alignment = TextAnchor.MiddleCenter;
			transTextPlayButton.localPosition = Vector3.zero;

			v2_ButtonSizeBuffer = spriteRenderOtherButton.size;
			v2_ButtonSizeBuffer.x = 4.5F;
			spriteRenderOtherButton.size = v2_ButtonSizeBuffer;

			textOtherButton.color = GameController.Instance.spriteController.colorArrMenu_OtherTextColor [1];
			spriteRenderOtherButton.color = GameController.Instance.spriteController.colorArrMenu_OtherButton [1];

//			spriteRenderOther_CurrencyIcon.enabled = true;
			objOther_CurrencyIcon.SetActive (true);
			spriteRenderOther_VideoIcon.enabled = true;

			OtherButtonIdle (true);
		}
	}

	public void PlayButtonIdle (bool canPlay) {
		if (canPlay) {
//			animPlayButtonArtHolder.clip = animClip_PlayButtonArtArr [0];
			animPlayButtonArtHolder.Play ();
		} else {
//			animPlayButtonArtHolder.clip = animClip_PlayButtonArtArr [1];
			animPlayButtonArtHolder.Rewind ();
			animPlayButtonArtHolder.Stop ();
		}
	}

	public void OtherButtonIdle (bool canPlay) {
		if (canPlay) {
			//			animPlayButtonArtHolder.clip = animClip_PlayButtonArtArr [0];
			animOtherButtonArtHolder.Play ();
		} else {
			//			animPlayButtonArtHolder.clip = animClip_PlayButtonArtArr [1];
			animOtherButtonArtHolder.Rewind ();
			animOtherButtonArtHolder.Stop ();
		}
	}

	IEnumerator KnifeLastPosit_Routine () {
		int i = 0;
		while (i < 200) {
			i++;
			knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition = 
				Mathf.MoveTowards (knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition, 
				PlayerData.Instance.floatLastKnifePosit, Time.deltaTime);
			if (knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition == PlayerData.Instance.floatLastKnifePosit) {
				i = 200;
			}
			yield return null;
		}

		knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition = PlayerData.Instance.floatLastKnifePosit;
	}

	public void Menu_AnimMainPlay (int intWhichAnim) {
		animMenuHolder.clip = animClip_MenuArr [intWhichAnim];
		animMenuHolder.Play ();
	}

	public void AnimEvent_KnifeLastPosit () {
		// Remember the position of last knife played
		if (PlayerData.Instance != null) {
			StartCoroutine (KnifeLastPosit_Routine ());
//			knifeLineController.scrollRectKnivesHolder.horizontalNormalizedPosition = PlayerData.Instance.floatLastKnifePosit;
		}
	}

	public void AnimEvent_KnifeIdelReset () {
		animMainLogoHolder.Rewind ();
		animMainLogoHolder.Stop ();

		ParticleSystem.MainModule main = particleMenuStarsOnly.main;
		main.startLifetime = 1;
		main.loop = false;
	}

	public void AnimEvent_ChangeButtonsToGameOver () {
		menuButtonType = Menu_Script.MenuButtonsType.GameOver_Buttons;
		if (PlayerData.Instance != null) {
			PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.GameOver;
		}

		// Change buttons
		GameOver_AllSetup ();

		Menu_AnimMainPlay (4);
	}

	public void AnimEvent_ContiueResurrectionEnd () {
		StartCoroutine (MainMenu_ActivatorDelayer (2, false));
	} 

	public IEnumerator MainMenu_ActivatorDelayer ( float delay, bool isActive) {
		yield return new WaitForSeconds (delay);
		MainMenu_Activator (isActive);
	}

	public void MainMenu_Activator (bool isActive) {
		graphicRaycasterMainMenuHolder.enabled = isActive;
		canvasMainMenuHolder.enabled = isActive;
		objMainMenuHolder.SetActive (isActive);
	}

	public void MainMenu_OnlyGraphicRayActivator (bool isActive) {
		graphicRaycasterMainMenuHolder.enabled = isActive;
	}

	public void DailyGift_Activator (bool isActive) {
//		buttonDailyGiftHolder.enabled = isActive;
		objDailyGiftFlashHolder.SetActive (isActive);

		// Moved out of following if 
		GameController.Instance.isDailyGiftAvailable = isActive;

		if (isActive) {
			objDailyGiftParticlesParent.SetActive (true);
			animDailyGiftFlashHolder.Play ();
		} else {
			objDailyGiftParticlesParent.SetActive (false);
			animDailyGiftFlashHolder.Stop ();
		}
	}
}
