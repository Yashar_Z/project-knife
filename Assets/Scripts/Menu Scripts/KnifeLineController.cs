﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnifeLineController : MonoBehaviour {

	public RectTransform rectTransContentHolder;

	public float floatKnivesPositX_Default;

	public KnifeClass[] knifeClassArr;

	public ScrollRect scrollRectKnivesHolder;
	public Transform transScrollContent;

	private Transform transKnifeBuffer;
	private Vector3 v3_KnifePositBuffer;

	void Start () {
		ResizeContents ();
		StartCoroutine (SortKnifes ());
	}

	void CheckSavedKnives () {

	}

	public IEnumerator SortKnifes () {
		for (int i = 0; i < knifeClassArr.Length; i++) {
			
			transKnifeBuffer = knifeClassArr [i].transform;
			v3_KnifePositBuffer = transKnifeBuffer.localPosition;
			v3_KnifePositBuffer.x = floatKnivesPositX_Default + (knifeClassArr [i].intIndexOrder - 1) * 125;

			if (knifeClassArr [i].isLongForm) {
				v3_KnifePositBuffer.x += 6F;
				v3_KnifePositBuffer.y -= 6.5F;
			} 

			if (knifeClassArr [i].hasParticle) {
				transKnifeBuffer.GetChild (3).gameObject.SetActive (true);
//				knifeClassArr [i].particleKnifeBodyHolder.gameObject.SetActive (true);
//				knifeClassArr [i].particleKnifeBodyHolder.Play ();
			}

			transKnifeBuffer.localPosition = v3_KnifePositBuffer;


			if (PlayerData.Instance != null) {
				// Checks saved data
				if (PlayerData.Instance.hasKnifeArr [i]) {
					// Changes the type of knife to free in case AND removes the knife
					KnifeChangeToFree (i);
				}
			}

			// Enable locks in only cases that you don't
			if (knifeClassArr [i].knifeType != KnifeClass.KnifeType.Free) {
				knifeClassArr [i].objKnifeLockHolder.SetActive (true);
			}

			knifeClassArr [i].boxColl2D_Holder.enabled = true;

			yield return null;
		}
	}

//	public void KnifeLife_DragStart () {
//		scrollRectKnivesHolder
//	}

	public void KnifeLife_LastPlaceScroll (float floatPosit) {
		scrollRectKnivesHolder.horizontalNormalizedPosition = floatPosit;
	}

	public void KnifeLife_DragEnd () {
		StartCoroutine (BeforeAutoDrag ());
	}

	IEnumerator BeforeAutoDrag () {
		if (scrollRectKnivesHolder.velocity.x > 0) {
			while (scrollRectKnivesHolder.velocity.x > 420) {
				yield return null;
			}
			StartCoroutine (AutoDrag_Routine (true));
		} else {
			while (scrollRectKnivesHolder.velocity.x < -420) {
				yield return null;
			}
			StartCoroutine (AutoDrag_Routine (false));
		}
	}

	IEnumerator AutoDrag_Routine (bool isForward) {
		scrollRectKnivesHolder.inertia = false;

		float floatAutoDragTarget = transScrollContent.localPosition.x % 125;
		Vector3 v3_DragTargetBuffer = transScrollContent.localPosition;
		Vector3 v3_DragCurrentBuffer = transScrollContent.localPosition;

		if (isForward) {
			v3_DragTargetBuffer.x = v3_DragCurrentBuffer.x - floatAutoDragTarget;
		} else {
			v3_DragTargetBuffer.x = v3_DragCurrentBuffer.x - floatAutoDragTarget - 125;
		}

//		if (floatAutoDragTarget > -64) {
//			v3_DragTargetBuffer.x = v3_DragCurrentBuffer.x - floatAutoDragTarget;
//		} else {
//			v3_DragTargetBuffer.x = v3_DragCurrentBuffer.x - floatAutoDragTarget - 125;
//		}

		while (Mathf.Abs (v3_DragCurrentBuffer.x - v3_DragTargetBuffer.x) > 2) {
			v3_DragCurrentBuffer.x = Mathf.MoveTowards (v3_DragCurrentBuffer.x, v3_DragTargetBuffer.x, Time.deltaTime * 360);
			transScrollContent.localPosition = v3_DragCurrentBuffer;
			yield return null;
		}

		v3_DragCurrentBuffer.x = v3_DragTargetBuffer.x;
		transScrollContent.localPosition = v3_DragCurrentBuffer;

		scrollRectKnivesHolder.inertia = true;

		yield return null;
	}

	public void ResizeContents () {
		Vector2 v2Buffer = rectTransContentHolder.sizeDelta;
		v2Buffer.x = 500 + (knifeClassArr.Length - 1) * 125;
		rectTransContentHolder.sizeDelta = v2Buffer;
	}

//	public void KnifeChangeToFree (int index) {

	public void KnifeChangeToFree (int index) {
		knifeClassArr [index].knifeType = KnifeClass.KnifeType.Free;
		knifeClassArr [index].objKnifeLockHolder.SetActive (false);
	}

	public void KnifeBuyItselfAnimate (int index) {
		knifeClassArr [index].animKnifeHolder.Play ();
	}
}
