﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsScript : MonoBehaviour {

	public bool isCreditsOn;

	public bool isPlayerScrolling;

	public ScrollRect scrollRectCreditsHolder;

	public GameObject objSoundToggle_On;
	public GameObject objSoundToggle_Off;

	public GameObject objGooglePlayToggle_On;
	public GameObject objGooglePlayToggle_Off;

	[Header ("")]
	public GameObject objSettingsMainHolder;
	public GameObject objSettingsCreditsHolder;

	public Animation animSettingsClouds;

	private	float floatAutoScrollSpeed;
	private float floatVertNormalized;

	void OnEnable () {
		GooglePlayManager.OnGooglePlaySignIn += onGooglePlaySignIn;
		GooglePlayManager.OnGooglePlaySignOut += onGooglePlaySignOut;

		isPlayerScrolling = true;
		scrollRectCreditsHolder.horizontalNormalizedPosition = 1;

		animSettingsClouds [animSettingsClouds.clip.name].time = 0.4F;
		animSettingsClouds.Play ();

		// Setup values for start of settings
		SoundAndGooglePlaySetup ();
	}

	void OnDisable () {
		GooglePlayManager.OnGooglePlaySignIn -= onGooglePlaySignIn;

		animSettingsClouds.Stop ();
	}

	void SoundAndGooglePlaySetup () {
		if (PlayerData.Instance != null) {
			Debug.LogError ("PlayerData.Instance.isYesGooglePlay = " + PlayerData.Instance.isYesGooglePlay);

			objSoundToggle_On.SetActive (!PlayerData.Instance.isNoSound);
			objSoundToggle_Off.SetActive (PlayerData.Instance.isNoSound);

			objGooglePlayToggle_On.SetActive (PlayerData.Instance.isYesGooglePlay);
			objGooglePlayToggle_Off.SetActive (!PlayerData.Instance.isYesGooglePlay);

			SoundManager.Instance.SoundVol_Check ();
		}
	}
		
	void onGooglePlaySignIn () {
		PlayerData.Instance.isYesGooglePlay = true;

		SoundAndGooglePlaySetup ();
	}

	void onGooglePlaySignOut () {
		PlayerData.Instance.isYesGooglePlay = false;

		SoundAndGooglePlaySetup ();
	}

	public void Pressed_HomeButton () {
		if (isCreditsOn) {
			isCreditsOn = false;
			objSettingsMainHolder.SetActive (true);
			objSettingsCreditsHolder.SetActive (false);

			GameController.Instance.menuScript.SettingsHomeParticle_Controller (true);
			GameController.Instance.menuScript.particleSettings_HomeButton.Play ();
		} else {
			GameController.Instance.menuScript.SettingsHomeParticle_Controller (false);
			GameController.Instance.menuScript.Menus_SettingsToMain ();
		}

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Pressed_SoundButton () {
		if (PlayerData.Instance != null) {
			PlayerData.Instance.isNoSound = !PlayerData.Instance.isNoSound;
			SaveLoad.Save ();
			SoundAndGooglePlaySetup ();
		}

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Pressed_CupButton () {
		if (PlayerData.Instance != null) {

		}

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Pressed_GooglePlayButton () {
		if (PlayerData.Instance != null) {
			if (PlayerData.Instance.isYesGooglePlay) {
				GooglePlayManager.Instance.SignOut ();
			} else {
				GooglePlayManager.Instance.SignIn ();
			}

			SaveLoad.Save ();
			SoundAndGooglePlaySetup ();
		}

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Pressed_CreditsButton () {
//		StartCoroutine (GameController.Instance.menuScript.ToSettingsAndBack ());

		isPlayerScrolling = true;

		// Reset Credits Values
		scrollRectCreditsHolder.verticalNormalizedPosition = 1;
		floatVertNormalized = 10 * scrollRectCreditsHolder.verticalNormalizedPosition;

		Credits_StartScroll (0.25F);

		objSettingsMainHolder.SetActive (false);
		objSettingsCreditsHolder.SetActive (true);

		GameController.Instance.menuScript.Button_PressSound ();
	}

	void Credits_StartScroll (float delay) {
		isCreditsOn = true;

		StartCoroutine (CreditsScrollRoutine (delay));
	}

	IEnumerator CreditsScrollRoutine (float delay) {
		yield return new WaitForSeconds (delay);
		isPlayerScrolling = false;

		StartCoroutine (AutoScrollSpeedIncrease ());

		floatVertNormalized = 10 * scrollRectCreditsHolder.verticalNormalizedPosition;
		while (!isPlayerScrolling && (scrollRectCreditsHolder.verticalNormalizedPosition > 0)) {

			floatVertNormalized = Mathf.MoveTowards (floatVertNormalized, 0, Time.deltaTime * floatAutoScrollSpeed);
			scrollRectCreditsHolder.verticalNormalizedPosition = floatVertNormalized / 10;
			yield return null;
		}
	}

	IEnumerator AutoScrollSpeedIncrease () {
		floatAutoScrollSpeed = 0;
		while (!isPlayerScrolling && (floatAutoScrollSpeed < 2)) {
			floatAutoScrollSpeed = Mathf.MoveTowards (floatAutoScrollSpeed, 1F, Time.deltaTime);
			yield return null;
		}
		yield return null;
	}

	public void Pressed_ContactUsButton () {
		// TODO: Settings

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Pressed_MoreGamesButton () {
		// TODO: Settings

		GameController.Instance.menuScript.Button_PressSound ();
	}

	public void Credits_DragStart () {
		isPlayerScrolling = true;
	}

	public void Credits_DragEnd () {
		isPlayerScrolling = true;
		StartCoroutine (AfterDrag_AutoScroll ());
	}

	IEnumerator AfterDrag_AutoScroll () {
		yield return new WaitForSeconds (0.1F);
		Credits_StartScroll (0);
	}

	void Update () {
//		if (!isPlayerScrolling) {
//			Debug.LogWarning ("scrollRectCreditsHolder.horizontalNormalizedPosition = " + scrollRectCreditsHolder.verticalNormalizedPosition);
//			scrollRectCreditsHolder.verticalNormalizedPosition -= 0.01F;
//
//			if (scrollRectCreditsHolder.verticalNormalizedPosition <= 0) {
//				isPlayerScrolling = true;
//			}
//		}

		if (Input.GetKeyDown(KeyCode.Escape)) {

			//TODO: Pressing Back In Settings

//			Application.Quit(); 
		}
	}
}
