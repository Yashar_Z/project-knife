﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Continue_AnimsScript : MonoBehaviour {

	public void AnimEvent_ContinueCountdownEnd () {
		if (!GameController.Instance.menuScript.objContinueExplosionsParent.activeInHierarchy) {
			GameController.Instance.menuScript.Continue_To_GameOvers ();
		}
	}
}
