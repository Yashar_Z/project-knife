﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BazaarPlugin;

public class Loader : MonoBehaviour {

	void Start () {
		StartCoroutine (Start_Loader ());
	}

	IEnumerator Start_Loader () {
		// Wait before load
		yield return null;

		SaveLoad.Load ();

		SoundManager.Instance.SoundVol_Check ();

		// Wait before scenee
		yield return new WaitForSeconds (0.6F);

		PlayerData.Instance.gameMenuType = PlayerData.GameMenuType.Startup;
		PlayerData.Instance.SceneLoadAsynch ("GameScene");
	}
}
