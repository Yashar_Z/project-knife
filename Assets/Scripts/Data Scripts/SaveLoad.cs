﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class SaveLoad  {

    private static PersistentData persistentData = new PersistentData();

    public static void Save() {
		persistentData.player_Currency = PlayerData.Instance.player_Currency;
		persistentData.player_VidsWatched = PlayerData.Instance.player_VidsWatched;
		persistentData.player_Score_Highscore = PlayerData.Instance.player_Score_Highscore;
		persistentData.player_CheckpointTotal = PlayerData.Instance.player_CheckpointTotal;

		persistentData.player_VidsPerKnifeArr = PlayerData.Instance.player_VidsPerKnifeArr;
		persistentData.hasKnifeArr = PlayerData.Instance.hasKnifeArr;

		persistentData.isNoAds = PlayerData.Instance.isNoAds;
		persistentData.isNoSound = PlayerData.Instance.isNoSound;
		persistentData.isYesGooglePlay = PlayerData.Instance.isYesGooglePlay;

		persistentData.floatLastKnifePosit = PlayerData.Instance.floatLastKnifePosit;
		persistentData.timeLastDailyGift = PlayerData.Instance.timeLastDailyGift;

		persistentData.intGameVersion = PlayerData.Instance.intGameVersion;

        BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGamesKnife.gd");
		bf.Serialize (file, persistentData);
		file.Close ();

        Debug.Log("SAVED!");
	}

    public static void Load() {
        Debug.Log(Application.persistentDataPath);

        if (PlayerData.Instance != null) {
			if (File.Exists (Application.persistentDataPath + "/savedGamesKnife.gd")) {
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (Application.persistentDataPath + "/savedGamesKnife.gd", FileMode.Open);

				try {
					persistentData = (PersistentData)bf.Deserialize (file);
				} catch (System.Exception e) {
					Debug.LogError (e.Message);
				}

				file.Close ();

				PlayerData.Instance.player_Currency = persistentData.player_Currency;
				PlayerData.Instance.player_VidsWatched = persistentData.player_VidsWatched;
				PlayerData.Instance.player_Score_Highscore = persistentData.player_Score_Highscore;
				PlayerData.Instance.player_CheckpointTotal = persistentData.player_CheckpointTotal;

				PlayerData.Instance.player_VidsPerKnifeArr = persistentData.player_VidsPerKnifeArr;
				PlayerData.Instance.hasKnifeArr = persistentData.hasKnifeArr;

				PlayerData.Instance.isNoAds = persistentData.isNoAds;
				PlayerData.Instance.isNoSound = persistentData.isNoSound;
				PlayerData.Instance.isYesGooglePlay = persistentData.isYesGooglePlay;

				PlayerData.Instance.floatLastKnifePosit = persistentData.floatLastKnifePosit;
				PlayerData.Instance.timeLastDailyGift = persistentData.timeLastDailyGift;

//				PlayerData.Instance.intGameVersion = PlayerData.Instance.intGameVersion;
				PlayerData.Instance.intGameVersion = persistentData.intGameVersion;

				// Version check for first times for quest update
				if (IsNewer_VersionCheck ()) {
					Debug.LogError ("Newer version! " + PlayerData.Instance.intGameVersion);
				}

                //default value
//                if (string.IsNullOrEmpty(PlayerData_Main.Instance.PowUp1_StartCharge_Time))
//                    PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
//                if (string.IsNullOrEmpty(PlayerData_Main.Instance.PowUp2_StartCharge_Time))
//                    PlayerData_Main.Instance.PowUp2_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            }

            // Default values and create file
            else
            {
				PlayerData.Instance.player_Currency = 0;
				PlayerData.Instance.player_VidsWatched = 0;
				PlayerData.Instance.player_Score_Highscore = 0;
				PlayerData.Instance.player_CheckpointTotal = 0;

				PlayerData.Instance.isNoAds = false;
				PlayerData.Instance.timeLastDailyGift = System.DateTime.Now.AddDays (-1);

//				PlayerData.Instance.hasKnifeArr = PlayerData.Instance.hasKnifeArr;
//				PlayerData.Instance.intGameVersion = 0;


//                PlayerData_Main.Instance.PowUp1_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
//                PlayerData_Main.Instance.PowUp2_StartCharge_Time = System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
//
//				  PlayerData_Main.Instance.player_dateVidGiftLast = System.DateTime.Now.AddDays (-1);
            }
		}
        Debug.Log("LOADED!");
    }

	private static bool IsNewer_VersionCheck () {
		int versionBufferInt = System.Convert.ToInt32(System.Text.RegularExpressions.Regex.Replace(Application.version, "[^0-9]", "" ));
		if (PlayerData.Instance.intGameVersion < versionBufferInt) {
			PlayerData.Instance.intGameVersion = versionBufferInt;
			return true;
		} else {
			return false;
		}
	}
}
