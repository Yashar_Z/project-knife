﻿using System.Collections.Generic;

[System.Serializable]
public class PersistentData  {

	public int player_Currency;
	public int player_VidsWatched;
	public int player_Score_Highscore;
	public int player_CheckpointTotal;
	public int[] player_VidsPerKnifeArr;

	public bool[] hasKnifeArr;
	public bool isNoSound;
	public bool isNoAds;
	public bool isYesGooglePlay;

	public float floatLastKnifePosit;
	public System.DateTime timeLastDailyGift;

	public int intGameVersion;

//
//    public int player_WorldsUnlocked;
//	public int player_LevelsUnlocked;
//
//	public List<ItemWithAmount> player_ItemsAmount;
//
//	public int player_Mustache;
//	public int player_Stars;
//
//	public int player_PowUp1_Own;
//	public int player_PowUp2_Own;
//
//	public int player_PowUp1_Ready;
//	public int player_PowUp2_Ready;
//
//	public bool player_Sound_ON;
//	public bool player_Music_ON;
//
//	public LevelData_Class[] player_World1_LevelsDataArr;
//	public LevelData_Class[] player_World2_LevelsDataArr;
//	public LevelData_Class[] player_World3_LevelsDataArr;
//	public LevelData_Class[] player_World4_LevelsDataArr;
//	public LevelData_Class[] player_World5_LevelsDataArr;
//
//	public int[] worldsGoldStarsArr;
//
//	public bool TutSeen_BatteryCharge;
//	public bool TutSeen_ItemProgress;
//	public bool TutSeen_ItemRepeatItem;
//	public bool TutSeen_BatterySlot;
//	public bool TutSeen_EndlessUnlock;
//
//	public bool TutSeen_2ndCostume_Fat;
//	public bool TutSeen_2ndCostume_Thin;
//	public bool TutSeen_2ndCostume_Muscle;
//	public bool TutSeen_2ndCostume_Giant;
//
//	public bool Event_NewYear_SawPopup;
//	public bool Event_NewYear_Registered;
//
//	public bool Show_NewLevel_Allow;
//
//	public bool Show_NewWorld_Allow;
//	public bool Show_NewWorld_Remain;
//
//	public bool isVibratingPhone;
//
//    public string PowUp1_StartCharge_Time;
//    public string PowUp2_StartCharge_Time;
//
//	public Achievement[] Achivements;
//    public Achievement[] AchivementsArr;
//
//	public Quest[] Quests;
//	public Quest[] ActiveQuests;
//	public Quest[] QueueQuests;
//
//	public System.DateTime LastQuestGivenDate;
//
//	// Future Stuff
//	public bool[] boolFutureArr1;
//	public bool[] boolFutureArr2;
//	public int[] intFutureArr1;
//	public int[] intFutureArr2;
//
//	// Endless Stuff
//	public int player_XP_Curr;
//	public int player_Score_Record;
//	public int player_EndlessLevel;
//	public bool player_Google_PreviouslySignedIn;
//
//	public System.DateTime player_dateVidGiftLast;
}
