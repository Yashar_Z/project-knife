﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour {

	public static PlayerData Instance;

	public enum GameMenuType {
		Startup,
		MainMenu,
		InGame,
		Continue,
		GameOver,
		Other
	}

	public enum ShopType
	{
		Bazaar,
		Myket,
		VAS
	}

	public GameMenuType gameMenuType;
	public ShopType shopType;

	#region Saved Data
	public int player_Currency;
	public int player_VidsWatched;
	public int player_Score_Highscore;
	public int player_CheckpointTotal;
	public int[] player_VidsPerKnifeArr;

	public bool[] hasKnifeArr;
	public bool isNoSound;
	public bool isNoAds;
	public bool isYesGooglePlay;

	public float floatLastKnifePosit;
	public System.DateTime timeLastDailyGift;

	public int intGameVersion;
	#endregion

	public bool isAdVideoAvailable_Reward;
	public bool isAdVideoAvailable_Forced;

	public int intTimesPlayedThisSession;

	public Sprite spritePlayerKnifeHolder;

	void Awake () {
		Instance = this;
		DontDestroyOnLoad (this);
	}

	public void SceneLoad (string stringName) {
		SceneManager.LoadScene (stringName);
	}

	public void SceneLoadAsynch (string stringName) {
		SceneManager.LoadSceneAsync (stringName);
	}

	void OnApplicationQuit()
	{
		Debug.Log("Application ending after " + Time.realtimeSinceStartup + " seconds");
	}
}
