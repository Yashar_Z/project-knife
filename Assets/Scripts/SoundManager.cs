﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    private static SoundManager _instance = null;
    public static SoundManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public AudioSource ShrinkingCheese;
    public AudioSource CheckPoint;
    public AudioSource CutMamoli;
    public AudioSource CutSayareAbi;
    public AudioSource CutPanirMoraba;
    public AudioSource MargBaFelfel;
    public AudioSource MargBaSayare;
    public AudioSource MargBaKhoroj;

    public AudioSource Poof;

    public AudioSource EnterToCheese;
    public AudioSource Banana;
    public AudioSource Bread;

    public AudioSource GenericTap;

    public AudioSource MusicMenu;
    public AudioSource MusicInGame;

    private AudioSource audioSourceCurrent;

	void Start () {
		audioSourceCurrent = MusicMenu;
	}

	public void SoundVol_Check () {
		if (PlayerData.Instance != null) {
			if (!PlayerData.Instance.isNoSound) {
				AudioListener.volume = 1;
			} else {
				AudioListener.volume = 0;
			}
		}
	}

    public void BlendMusic(int newMusic)
    {

        StartCoroutine(MusicVolume_FadeToZero(() =>
        {
			if (newMusic == 0)
			{
				MusicInGame.Stop();
				audioSourceCurrent = MusicMenu;
			}
			else
			{
				MusicMenu.Stop();
				audioSourceCurrent = MusicInGame;
			}

            audioSourceCurrent.PlayDelayed(0.1f);
            StartCoroutine(MusicVolume_FadeToFull(() => { Debug.Log("music blend ended"); }));
        }
        ));
    }

    public IEnumerator MusicVolume_FadeToFull(Action onFinished)
    {
        while (audioSourceCurrent.volume < 1)
        {
            audioSourceCurrent.volume += 0.05F;
            yield return new WaitForSeconds(0.05F);
        }
        onFinished();
    }

    public IEnumerator MusicVolume_FadeToZero(Action onFinished)
    {
        while (audioSourceCurrent.volume > 0)
        {
            audioSourceCurrent.volume -= 0.05F;
            yield return new WaitForSeconds(0.05F);
        }
        onFinished();
    }
}
