﻿using BazaarPlugin;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using Firebase.Analytics;
using Firebase;

public class testSceneManager : MonoBehaviour {
    public Text messageText;

    //no need for foreced ad button in real game
    public Button forcedAdButton;
    public Button rewardAdButton;

    public Image Blocker;

    //implement in save/load file
    public static bool PlayerHavePurchasedNoAds = false;


    private void Awake()
    {
        Blocker.enabled = false;

        // from load files
        if (PlayerPrefs.GetInt("NoAdsPurchased",0) == 1)
        {
            PlayerHavePurchasedNoAds = true;
        }

        //        Tapligh_Script.Instance.OnTaplighVideoRewardTypeAvailable += onTaplighVideoRewardTypeAvailable;
        //        Tapligh_Script.Instance.OnTaplighVideoForcedTypeAvailable += onTaplighVideoForcedTypeAvailable;
        //        Tapligh_Script.Instance.OnTaplighRewardEligible += onTaplighRewardEligible;
        //        Tapligh_Script.Instance.OnTimeToDisableBlocker += onTimeToDisableBlocker;
        //        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful += onTaplighVerifiedSuccessful;

        // get from cafe bazaar 
        var key = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCZStpIlJutJd8zxuBJLmdxxp4BXDAgnSCJ0Nbkf8SUvf1KxD7v6hdaiiy+1swf6VThjWSxBOxr4O8U9RQaiXWLf5nWr/fxfkOmnCooTd+IDG2yZyQclE7CDAW4aMhDIAfXKbxOJ3nMKUwVKvwym7Fo4tFiBye/IPZ8PX476nw0vMpX9wys/9HnG1rWq7Pz/95twTLuOhob19O9yQqXVRgF+WxVwyIZxaR3CyxtFW8CAwEAAQ==";
        BazaarIAB.init(key);
        //BazaarIAB.enableLogging(false);
    }

    private void OnDisable()
    {
        Tapligh_Script.Instance.OnTaplighVideoRewardTypeAvailable -= onTaplighVideoRewardTypeAvailable;
        Tapligh_Script.Instance.OnTaplighVideoForcedTypeAvailable -= onTaplighVideoForcedTypeAvailable;
        Tapligh_Script.Instance.OnTaplighRewardEligible -= onTaplighRewardEligible;
//        Tapligh_Script.Instance.OnTimeToDisableBlocker -= onTimeToDisableBlocker;
        Tapligh_Script.Instance.OnTaplighVerifiedSuccessful -= onTaplighVerifiedSuccessful;
    }

    private void Start()
    {

        //// initiate google play games
        //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        //PlayGamesPlatform.InitializeInstance(config);
        //PlayGamesPlatform.DebugLogEnabled = true;
        //PlayGamesPlatform.Activate();

        //Social.localUser.Authenticate((bool success) =>
        //{
        //    if (success)
        //    {
        //        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
        //        Debug.Log("login success");
        //    }
        //    else
        //    {
        //        Debug.Log("login failed");
        //    }
        //});


        //Tapligh_Script.Instance.SetTestMode(false);
    }



    IEnumerator ShowTextForFiveSeconds(string text)
    {
        messageText.text = text;
        yield return new WaitForSeconds(5f);
        messageText.text = "";
    }

    private void onTaplighVideoForcedTypeAvailable()
    {
        forcedAdButton.interactable = true;
    }

    private void onTaplighVideoRewardTypeAvailable()
    {
        rewardAdButton.interactable = true;
    }

    public void Pressed_ForcedAdButton()
    {
        forcedAdButton.interactable = false;
        Tapligh_Script.Instance.ShowAdForcedType();
    }

    public void Pressed_RewardAdButton()
    {
        rewardAdButton.interactable = false;
        Tapligh_Script.Instance.ShowAdRewardType();
        StartCoroutine(ShowBlockerForSeconds(5f));
    }

    private void onTaplighRewardEligible()
    {
        //Tapligh_Script.Instance.VerifyToken();
    }
    private void onTaplighVerifiedSuccessful()
    {
        StartCoroutine(ShowTextForFiveSeconds("reward is ready"));
    }

    private void onTimeToDisableBlocker()
    {
        Blocker.enabled = false;
    }

    IEnumerator ShowBlockerForSeconds(float seconds)
    {
        Blocker.enabled = true;
        yield return new WaitForSeconds(seconds);
        Blocker.enabled = false;
    }

    // ========================== CafeBazar===========================

    string[] skus = { NO_ADS_ITEM_NAME, GOLDEN_ITEM_NAME,
                        ELEGANT_DAGGER_G_ITEM_NAME, DRAGON_G_ITEM_NAME,
                        DART_G_ITEM_NAME,CURVED_G_ITEM_NAME,ARABIC_SWORD_G_ITEM_NAME };

    public const string NO_ADS_ITEM_NAME = "noads";
    public const string GOLDEN_ITEM_NAME = "golden";
    public const string ELEGANT_DAGGER_G_ITEM_NAME = "elegant_dagger_g";
    public const string DRAGON_G_ITEM_NAME = "dragon_g";
    public const string DART_G_ITEM_NAME = "dart_g";
    public const string CURVED_G_ITEM_NAME = "curved_g";
    public const string ARABIC_SWORD_G_ITEM_NAME = "arabic_sword_g";


    public static void PurchaseNoAds()
    {
        BazaarIAB.purchaseProduct(NO_ADS_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenDagger()
    {
        BazaarIAB.purchaseProduct(GOLDEN_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenElegantDagger()
    {
        BazaarIAB.purchaseProduct(ELEGANT_DAGGER_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenDragonDagger()
    {
        BazaarIAB.purchaseProduct(DRAGON_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenDartDagger()
    {
        BazaarIAB.purchaseProduct(DART_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenCurvedDagger()
    {
        BazaarIAB.purchaseProduct(CURVED_G_ITEM_NAME, "hezarmoshtan");
    }
    public static void PurchaseGoldenArabicSword()
    {
        BazaarIAB.purchaseProduct(ARABIC_SWORD_G_ITEM_NAME,"hezarmoshtan");
    }

    public void Pressed_NoAds()
    {
        PurchaseNoAds();
    }
    public void Pressed_QueryPurchases()
    {
        BazaarIAB.queryPurchases();
    }
    public void Pressed_ConsumeForTest()
    {
        BazaarIAB.consumeProduct(NO_ADS_ITEM_NAME);
    }
    private void Update()
    {
        messageText.text = "";
        if (PlayerHavePurchasedNoAds)
        {
            messageText.text = " player have purchased no ads";
        }
    }

}
